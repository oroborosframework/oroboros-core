<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

//this will prevent any variable declarations from being used out of scope.
namespace oroboros\core\bin\bootload\subroutines;

/**
 * <Token Declaration Patch>
 * This patch adds any missing PHP token constants that are not declared
 * using their standard values, to prevent tokenization errors from all
 * versions of PHP between 5.4 and 7.2.
 */

$token_definitions = array(
    'T_FINALLY' => 351, //Added in PHP 5.5
    'T_YIELD' => 267, //Added in PHP 5.5
    'T_POW' => 304, //Added in PHP 5.6
    'T_POW_EQUAL' => 281, //Added in PHP 5.6
    'T_ELLIPSIS' => 391, //Added in PHP 5.6
    'T_YIELD_FROM' => 269, //Added in PHP 7.0
    'T_SPACESHIP' => 289, //Added in PHP 7.0
    'T_CHARACTER' => 315, //Removed in PHP 7.0
    'T_BAD_CHARACTER' => 316, //Removed in PHP 7.0
);

foreach ($token_definitions as $constant => $definition)
{
    if (!defined( $constant ) )
    {
        define($constant, $definition);
    }
}

unset($token_definitions);
