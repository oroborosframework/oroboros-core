<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

//this will prevent any variable declarations from being used out of scope.

namespace oroboros\core\bin\bootload;

/**
 * <Compatibility Routine>
 *
 * This routine is executed to determine if any compatibility patches
 * are required as per the current php version, and apply them if they
 * are neccessary. If the version of php is too low or too high to proceed,
 * this file will break the bootload routine by exiting the program with
 * an error.
 *
 * --------
 *
 * Routines represent a subset of execution that assumes control.
 * These are separated under a different naming convention, so
 * they are not accidentally included where it is no appropriate.
 * Anything that runs imperative commands (eg: executes logic in the
 * global scope as soon as the file is included) falls under the
 * classification of a routine.
 *
 * --------
 * 
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/
 * @category routines
 * @package oroboros/core
 * @subpackage bootload
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
//check if a compatible version of php is being used
if ( !( version_compare( PHP_VERSION, OROBOROS_PHP_MINIMUM_VERSION, '>=' ) ) )
{
    fwrite( STDERR,
        sprintf( 'Oroboros core version %s requires a minimum PHP version of [%s], but your version is [%s].'
            . PHP_EOL . ' This runtime will now terminate for stability purposes. Please upgrade your version of PHP '
            . 'to at least [%s], or remove any dependency on this software to continue beyond this point.',
            OROBOROS_VERSION, OROBOROS_PHP_MINIMUM_VERSION, PHP_VERSION,
            OROBOROS_PHP_MINIMUM_VERSION ) );
    die( 1 );
}