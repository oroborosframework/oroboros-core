<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\prototype\traits;

/**
 * <Oroboros Static Prototype Api Trait>
 * This trait provides the base accessor class with prototyping passthrough
 * methods compatible with the StaticControlApi declaration.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait PrototypeExtensionTrait
{
    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Prototype Class Check Method>
     * Checks if the given class has a prototypical instance registered.
     * @param string|object $class A class name or object to check for a
     *     prototypical instance of within the registered prototypers, or the
     *     specified prototyper if the second parameter is passed.
     * @param type $prototyper (optional) If supplied, will check only within
     *     the specified prototyper if that prototyper exists in the registry
     *     of prototyper instances.
     * @return bool
     */
    protected static function _prototypeHasClass( $class, $prototyper = null )
    {
        return self::_getPrototyper()->hasPrototype( $class, $prototyper );
    }

    /**
     * <Prototype Prototypical Check Method>
     * Reurns a boolean determination as to whether a given class name
     * or object is prototypically compatible.
     * @param type $class
     * @return bool
     */
    protected static function _prototypeIsPrototypical( $class )
    {
        return self::_getPrototyper()->checkIfPrototypical( $class );
    }

    /**
     * <Prototype Prototyper Setter Method>
     * Sets an externally supplied prototyper into the manager's prototyper registry.
     * @param string $id
     * @param \oroboros\core\interfaces\contract\libraries\prototype\PrototyperContract $prototyper
     * @param type $replace (optional) If true, will replace the current instance of the prototyper if it exists. Default false.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided id is not a string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided prototyper does not honor its contract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an attempt is made to set an internal default prototyper
     * @throws \oroboros\core\utilities\exception\RuntimeException If replace is false and the given id already exists
     */
    protected static function _prototypeSet( $id, $prototyper, $replace = false )
    {
        return self::_getPrototyper()->setPrototypical( $id, $object,
                $prototyper );
    }

    /**
     * <Prototype New Prototyper Instance Setter Method>
     * Creates a new prototyper instance using the template prototyper.
     * @param string $id The id for the new prototyper
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided id is not a string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an attempt is made to set an internal default prototyper
     * @throws \oroboros\core\utilities\exception\RuntimeException If replace is false and the given id already exists
     */
    protected static function _prototypeAdd( $id )
    {
        return self::_getPrototyper()->addPrototyper( $id );
    }

    /**
     * <Prototype Manager Prototyper Getter Method>
     * Returns a clone of a currently held prototyper if it exists,
     * or false if it does not exist.
     *
     * Returns a copy of the default prototyper if the id is not supplied.
     *
     * @param string $id (optional) The slug identifier of the prototyper to return
     * @return bool|\oroboros\core\interfaces\contract\libraries\prototype\PrototyperContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided id is not a string
     */
    protected static function _prototypeGet( $id = null )
    {
        if ( is_null( $id ) )
        {
            $id = '__default';
        }
        return self::_getPrototyper()->getPrototyper( $id );
    }

    /**
     * <Prototype Prototyper Check Method>
     * Returns a boolean determination as to whether a given id is
     * associated with a currently held prototyper.
     * @param string $id
     * @return bool
     */
    protected static function _prototypeHas( $id )
    {
        return self::_getPrototyper()->hasPrototyper( $id );
    }

    /**
     * <Prototype Prototyper List Method>
     * Returns a list of the current slug identifiers for
     * all currently held prototypers.
     * @return array
     */
    protected static function _prototypeList()
    {
        return self::_getPrototyper()->listPrototypers();
    }

    /**
     * <Prototype Manager Prototyper Reset Method>
     * Resets a prototyper by the slug identifier of the instance currently
     * held by the prototype manager.
     * @param type $id The slug identifier of the prototyper to reset
     * @return void
     */
    protected static function _prototypeResetPrototyper( $prototyper )
    {
        return self::_getPrototyper()->resetPrototyper( $prototyper );
    }

    /**
     * <Prototype Prototyper Unsetter Method>
     * Deletes a current prototyper instance if it exists.
     * @param string $id
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided id is not a string
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an attempt is made to delete the default prototyper
     */
    protected static function _prototypeDelete( $id )
    {
        return self::_getPrototyper()->deletePrototyper( $id );
    }

    /**
     * <Prototype Prototyical Instance Getter Method>
     * Returns a prototypical clone of a given prototypical id,
     * or false if not found.
     * @param string $id The slug identifier of the prototypical instance
     *     to retrieve.
     * @param string $prototyper (optional) If supplied, will check the
     *     given prototyper for the specified instance. If not supplied,
     *     will check the default prototyper instance.
     * @return bool|\oroboros\core\interfaces\contract\patterns\creational\PrototyicalContract
     */
    protected static function _prototypeGetInstance( $id, $prototyper = null )
    {
        return self::_getPrototyper()->getPrototypical( $id, $prototyper );
    }

    /**
     * <Prototype Prototypical Instance Check Method>
     * Checks if a given slug identifier has an instance stored in the
     * given prototyper instance.
     * @param string $id The slug identifier of the
     *     prototyical instance to check.
     * @param string $prototyper (optional) If supplied, will check within
     *     the given prototyper. If not supplied, will check within the
     *     default prototyper.
     * @return bool|string Returns the class name of the given id if it exists,
     *     otherwise returns false.
     */
    protected static function _prototypeHasInstance( $id, $prototyper = null )
    {
        return self::_getPrototyper()->hasPrototypical( $id, $prototyper );
    }

    /**
     * <Prototype Prototypical List Method>
     * Returns a list of all prototypical instances currently held in all
     * prototypers currently registered. If the optional prototyper parameter
     * is passed, the check will occur only within the given prototyper,
     * and return an associative array of the slug identifiers as keys and
     * the class names of the prototypical instances as values.
     *
     * If the prototyper parameter is not supplied, all prototypical instances
     * will be returned in an associative array, where the key is the slug
     * identifier of the prototyper, and the value is an associative array
     * with keys corresponding to the slug identifiers of the prototypical
     * instances, and values corresponding to their class names.
     *
     * If a prototyper is specified that does not exist, returns false.
     *
     * @param string $prototyper (optional)
     * @return array|bool
     */
    protected static function _prototypeListInstances( $prototyper = null )
    {
        return self::_getPrototyper()->listPrototypical( $prototyper );
    }

    /**
     * <Prototype Prototypical Instance Unsetter Method>
     * Unsets a given prototypical instance from the specified prototyper.
     * If the optional prototyper parameter is supplied, it will unset the
     * given id from that prototyper. If not supplied, it will unset it from
     * the default prototyper.
     *
     * @param string $id The slug identifier of the prototypical
     *     instance to remove.
     * @param string $prototyper (optional) If supplied, will unset the
     *     prototypical instance from the specified prototyper. If not supplied,
     *     will unset it from the default prototyper.
     * @return void
     */
    protected static function _prototypeDeleteInstance( $id, $prototyper = null )
    {
        return self::_getPrototyper()->deletePrototypical( $id, $prototyper );
    }

    /**
     * <Prototype Reset Method>
     * Resets the prototype manager to its default state,
     * which will also reset or remove all of its redundant
     * prototypers. The default prototyper will be restored
     * to an empty state.
     * @return void
     */
    protected static function _prototypeResetAll()
    {
        return self::_getPrototyper()->reset();
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Supplies a reference to the internal prototype manager instance.
     * @return \oroboros\core\interfaces\contract\libraries\prototype\PrototypeManagerContract
     * @throws \oroboros\core\utilities\exception\RuntimeException
     * @internal
     */
    private static function &_getPrototyper()
    {
        $prototyper = self::_staticBaselineGetDependency( 'prototyper' );
        if ( is_null( $prototyper ) )
        {
            throw new \oroboros\core\utilities\exception\RuntimeException(
            sprintf( 'Error encountered at [%s]. Prototyper instance of [%s] '
                . 'must be set as a static baseline dependency to use '
                . 'this trait.', __METHOD__,
                '\\oroboros\\prototype\\interfaces\\contract\\PrototyperContract' ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE_EXTENSION_FAILURE );
        }
        return $prototyper;
    }

}
