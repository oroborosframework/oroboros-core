<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\bootstrap\traits;

/**
 * <BootstrapTrait Trait>
 * Satisfies the BootstrapContract requirements.
 * Using this trait and implementing that contract interface should give
 * you a fully functional bootstrap object that honors it's api on any class
 * that does not collide with it's declared methods, with no additional work.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 * 
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage bootstrap
 * @requires \oroboros\environment\interfaces\enumerated\Environment
 * @satisfies \oroboros\core\interfaces\contract\libraries\bootstrap\BootstrapContract
 * @satisfies \oroboros\core\interfaces\contract\utilities\UtilityContract
 * @satisfies \oroboros\core\interfaces\contract\BaseContract
 * @default \oroboros\core\bootstrap\Bootstrap
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
trait BootstrapTrait
{

    /**
     * Here we are extending the BaselineTrait,
     * to inherit it's system recognition methods.
     */
    use \oroboros\core\traits\core\BaselineTrait;

/**
     * This will be our local value store.
     * This will also allow us to separate which internals
     * we wish to expose from those we do not want to be
     * outwardly affected.
     */
    use \oroboros\core\traits\patterns\behavioral\RegistryTrait;

/**
     * This will allow us to obtain the factories to automatically construct
     * the appropriate router and controllers we need in the current scope,
     * with respect to any overrides that have been defined.
     */
//    use \oroboros\core\traits\patterns\creational\factory\FactoryFactory;
    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\libraries\bootstrap\BootstrapContract
     *
     * @execution Default Execution Plan (minimal)
     * //this represents generic usage in a bootload routine
     * $bootstrap = new \path\to\your\class\with\this\trait\YourBootstrapClass();
     * $bootstrap->initialize();
     * try {
     *     $bootstrap->execute();
     * }
     * catch (\oroboros\core\interfaces\contract\libraries\bootstrap\BootstrapExceptionContract $e) {
     *     $bootstrap->error($e);
     * }
     * $bootstrap->shutdown();
     * exit();
     *
     * @execution Default Execution Plan (commented)
     * //this represents generic usage in a bootload routine
     *
     * //Create an instance
     * //The default concrete for this is \oroboros\core\bootstrap\Bootstrap
     * $bootstrap = new \path\to\your\class\with\this\trait\YourBootstrapClass();
     *
     * //Initialize the instance
     * $bootstrap->initialize();
     *
     * //this object will throw a BootstrapException if it fails to execute.
     * //We should catch this, and call the error method if it fires.
     * try {
     *     $bootstrap->execute();
     * }
     *
     * //We will catch the error using the
     * //contract interface for bootstrap exceptions
     * //This will allow us to also handle any overrides,
     * //or integration with any existing exception that implements it.
     * catch (\oroboros\core\interfaces\contract\libraries\bootstrap\BootstrapExceptionContract $e) {
     *
     *     //BootstrapExceptions return meaningful results that can be used to
     *     //accurately create a valid error execution plan.
     *     //All you need to do here is pass it into the bootstrap error method.
     *     //It already contains any relevant error codes and message output
     *     //appropriate to the defined scope.
     *     $bootstrap->error($e);
     * }
     *
     * //Our execution has completed.
     * //We may optionally run the clean shutdown process.
     * //You may also let PHP's native garbage collector
     * //do the rest if you aren't doing anything else
     * //after this point.
     *
     * //Tell the bootstrap object to release any objects it has
     * //stored in memory, which will fire the destructors for all
     * //objects anywhere in it's stack.
     * $bootstrap->shutdown();
     *
     * //We don't need this object any longer.
     * unset($bootstrap);
     *
     * //And we're done.
     * exit();
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Bootstrap Reset>
     * Resets the Bootstrap object to its state immediately after execution
     * fires successfully, so it is initialized, but does not have any
     * remnant data that was obtained through any action that occurred
     * after initialization. The purpose of this method is to allow a
     * degree of external control over the execution plan, and allow it
     * to be called repeatedly if needed, without the weight of
     * reinitialization.
     *
     * @param array $params (optional) If params are passed, they will replace the values known under the same key that were obtained during initialization. If not passed, they will be ignored.
     * @param array $flags (optional) If params are passed, they will replace the flags that were passed or obtained during initialization. If not passed, they will be ignored.
     * @return void
     */
    public function reset( $params = null, $flags = null )
    {

    }

    /**
     * <Bootstrap Launch>
     * Launches the application.
     * If the bootstrap has not already initialized fully, it will do so at
     * this point using it's default values (bootstraps should be capable of
     * autonomous operation, but not assume it).
     *
     * At this point it will obtain it's routes, check for a match,
     * load the appropriate controller if a match is found,
     * load the default (or supplied) error controller if no match is found,
     * initialize it's selected controller and pass any flags into it that
     * were given to the bootstrap, and execute the controller route.
     */
    public function launch()
    {

    }

    /**
     * <Bootstrap Shutdown>
     * This method terminates the bootstrap object, and causes it to release
     * all objects that it has instantiated along with their own chid objects.
     * This should clear it's entire stack and memory footprint, and leave the
     * bootstrap in an empty, uninitialized state. This SHALL NOT terminate
     * the object, but will leave it to be either reinitialized or unset as
     * determined by it's controlling logic.
     */
    public function shutdown()
    {

    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

}
