<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\adapter\abstracts;

/**
 * <Oroboros Abstract Adapter>
 * Within the Oroboros class structure, an adapter is a class
 * that wraps a 3rd party library, external asset,
 * or any non-PHP construct that needs to be interacted with.
 * Adapters are registered into the system as satisfying
 * requirements for a specific exteral asset or package,
 * and will be passed directly by the adapter factory by keyword.
 *
 * Adapters inherently rely on the ControlApi trait to generate
 * their Api dynamically. This trait allows the class to control
 * specific prefixed protected methods publicly, and keep an
 * indexed registration of them by prefix keyword. This allows you to easily scrape any connected class's public methods and create a passthru for them that is externally documented using their doc-block comments similar to how apigen produces code coverage documentation.
 * @satisfies CLASS_TYPE = '::adapter::'
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
abstract class AbstractAdapter
    extends \oroboros\core\abstracts\AbstractBase
    implements \oroboros\adapter\interfaces\contract\AdapterContract
{

    use \oroboros\adapter\traits\AdapterTrait
    {
        \oroboros\adapter\traits\AdapterTrait::_getException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\adapter\traits\AdapterTrait::_getExceptionCode insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\adapter\traits\AdapterTrait::_getExceptionCodeIdentifiers insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\adapter\traits\AdapterTrait::_getExceptionIdentifier insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\adapter\traits\AdapterTrait::_getExceptionIdentifiers insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\adapter\traits\AdapterTrait::_getExceptionMessage insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\adapter\traits\AdapterTrait::_recastException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\adapter\traits\AdapterTrait::_throwException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\adapter\traits\AdapterTrait::_validate insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\adapter\traits\AdapterTrait::_validationModes insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\adapter\traits\AdapterTrait::_baselineInternalsCheckValid insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\adapter\traits\AdapterTrait::_baselineInternalsMergeArrayDefaults insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\adapter\traits\AdapterTrait::_baselineInternalsThrowInvalidException insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\adapter\traits\AdapterTrait::_baselineInternalsValidateValues insteadof \oroboros\core\traits\core\StaticBaselineTrait;
        \oroboros\adapter\traits\AdapterTrait::_baselineInternalsCheckConfiguration insteadof \oroboros\core\traits\core\StaticBaselineTrait;
    }
    use \oroboros\core\traits\core\StaticBaselineTrait;

    const OROBOROS_CLASS_TYPE = \oroboros\core\interfaces\enumerated\type\ClassType::CLASS_TYPE_ADAPTER;
    const OROBOROS_CLASS_SCOPE = \oroboros\core\interfaces\enumerated\scope\ClassScope::CLASS_SCOPE_ADAPTER_ABSTRACT;
    const OROBOROS_CLASS_CONTEXT = \oroboros\core\interfaces\enumerated\context\ClassContext::CLASS_CONTEXT_ADAPTER;
    const OROBOROS_ADAPTER = true;

}
