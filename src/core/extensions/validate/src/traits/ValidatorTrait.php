<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\validate\traits;

/**
 * <Validator Utility Trait>
 * Provides methods for extensive validation of parameters.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage utilities
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\validate\interfaces\contract\ValidatorContract
 */
trait ValidatorTrait
{

    use \oroboros\core\traits\core\BaselineTrait;
    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\validate\interfaces\contract\ValidatorContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Validation Method>
     * @param string $mode A canonicalized validation mode.
     * @param mixed $evaluate The parameter to evaluate
     * @param mixed $valid The parameter(s) to consider valid
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for calling
     *     objects to raise their own.
     * @return bool
     */
    public static function validate( $mode, $evaluate, $valid = null,
        $throw_exception = false )
    {
        $class = get_called_class();
        $methods = $class::methods();
        unset( $methods[array_search( 'validate', $methods )] );
        $method = '_validate' . str_replace( ' ', null,
                ucwords( str_replace( '-', ' ', $mode ) ) );
        if ( !in_array( $mode, $methods ) )
        {
            throw self::_getException( 'bad-method-call-exception',
                'php-method-failure',
                array(
                'method' => $mode,
                'expected' => $methods,
                'context' => sprintf( 'Command resolves to [%s], which does not exist.',
                    get_called_class() . '::' . $method ) ) );
        }
        return $class::$method( $evaluate, $valid, $throw_exception );
    }

    /**
     * <Validation Method Keyword Getter>
     * Returns an array of the validation methods available,
     * which can be used as the first parameter of the validate() method.
     * @return array
     */
    public static function methods()
    {
        $class = get_called_class();
        $methods = get_class_methods( $class );
        foreach ( $methods as
            $key =>
            $name )
        {
            if ( strpos( $name, '_validate' ) === false )
            {
                unset( $methods[$key] );
                continue;
            }
            $methods[$key] = trim( strtolower( preg_replace( '/(?<!^)([A-Z])/',
                        '-\\1', substr( $name, 9 ) ) ), '-' );
        }
        return $methods;
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * Performs a regular expression match against the given subject,
     * and returns whether the match is valid. Fails safely if a subject
     * is given that cannot be used as a regular expression parameter.
     *
     * If an object with __toString is given, it will cast the object
     * to a string before matching.
     *
     * If an array or \Iterable is given, it will return whether all
     * keys match or not.
     *
     * If the given expression is invalid, an exception will be raised
     * automatically.
     *
     * @param type $subject
     * @param type $expression
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for calling
     *     objects to raise their own.
     * @return bool
     */
    protected static function _validateRegex( $subject, $expression,
        $throw_exception = false )
    {
        $class = get_called_class();
        $valid = preg_match( $expression, $subject )
            ? true
            : false;
        $expected_type = 'regex match of ' . $expression;
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     * Validates if a given subject is any of the provided values.
     * @param mixed $subject
     * @param array $whitelist
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for
     *     calling objects to raise their own.
     * @return bool
     */
    protected static function _validateAnyOf( $subject, array $whitelist,
        $throw_exception = false )
    {
        $class = get_called_class();
        $valid = in_array( $subject, $whitelist );
        $expected_type = 'value in ' . implode( ', ', $whitelist );
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param type $subject
     * @param type $blacklist
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for
     *     calling objects to raise their own.
     * @return bool
     */
    protected static function _validateNoneOf( $subject, array $blacklist,
        $throw_exception = false )
    {
        $class = get_called_class();
        $valid = !in_array( $subject, $blacklist );
        $expected_type = 'value not in ' . implode( ', ', $blacklist );
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param type $subject
     * @param type $type
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for
     *     calling objects to raise their own.
     * @return bool
     */
    protected static function _validateTypeOf( $subject, $type,
        $throw_exception = false )
    {
        $class = get_called_class();
        $valid = (gettype( $subject ) === $type);
        $expected_type = $type;
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param type $subject
     * @param string $expected
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for
     *     calling objects to raise their own.
     * @return bool
     */
    protected static function _validateInstanceOf( $subject, $expected,
        $throw_exception = false )
    {
        $class = get_called_class();
        $subject = (is_object( $subject )
            ? get_class( $subject )
            : $subject );
        $expected = (is_object( $expected )
            ? get_class( $expected )
            : $expected );
        $valid = ( ( is_string( $subject ) && ( class_exists( $subject ) || interface_exists( $subject ) ) ) )
            && ( is_string( $expected ) && ( class_exists( $expected ) || interface_exists( $expected ) ) )
            && ( ( $subject === $expected ) || is_subclass_of( $subject,
                $expected ) );
        $expected_type = 'instanceof ' . $class::_validatorGetRecievedType( $expected );
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param type $subject
     * @param string $expected
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for
     *     calling objects to raise their own.
     * @return bool
     */
    protected static function _validateChildOf( $subject, $expected,
        $throw_exception = false )
    {
        $class = get_called_class();
        $subject = (is_object( $subject )
            ? get_class( $subject )
            : $subject );
        $expected = (is_object( $expected )
            ? get_class( $expected )
            : $expected );
        $valid = is_subclass_of( $subject, $expected );
        $expected_type = 'extension of ' . $class::_validatorGetRecievedType( $expected );
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param type $subject
     * @param string $expected
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for
     *     calling objects to raise their own.
     * @return bool
     */
    protected static function _validateParentOf( $subject, $expected,
        $throw_exception = false )
    {
        $class = get_called_class();
        $subject = (is_object( $subject )
            ? get_class( $subject )
            : $subject );
        $expected = (is_object( $expected )
            ? get_class( $expected )
            : $expected );
        $valid = is_subclass_of( $expected, $subject );
        $expected_type = 'class extending ' . $class::_validatorGetRecievedType( $subject );
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param type $subject
     * @param mixed $unused (optional) no-op, just a placeholder for
     *     the public validate method
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for
     *     calling objects to raise their own.
     * @return bool
     */
    protected static function _validateClass( $subject, $unused = null,
        $throw_exception = false )
    {
        $class = get_called_class();
        $valid = false;
        if ( is_object( $subject ) )
        {
            $valid = true;
        } elseif ( is_string( $subject ) )
        {
            $valid = class_exists( $subject, true );
        }
        $expected_type = 'valid  existing or autoloadable class';
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param type $subject
     * @param mixed $unused (optional) no-op, just a placeholder for
     *     the public validate method
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for
     *     calling objects to raise their own.
     * @return bool
     */
    protected static function _validateTrait( $subject, $unused = null,
        $throw_exception = false )
    {
        $class = get_called_class();
        $valid = false;
        if ( is_string( $subject ) )
        {
            $valid = trait_exists( $subject, true );
        }
        $expected_type = 'valid  existing or autoloadable trait';
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param type $subject
     * @param mixed $unused (optional) no-op, just a placeholder for
     *     the public validate method
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for
     *     calling objects to raise their own.
     * @return bool
     */
    protected static function _validateInterface( $subject, $unused = null,
        $throw_exception = false )
    {
        $class = get_called_class();
        $valid = false;
        if ( is_string( $subject ) )
        {
            $valid = interface_exists( $subject, true );
        }
        $expected_type = 'valid existing or autoloadable interface';
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param type $subject
     * @param type $readable
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for
     *     calling objects to raise their own.
     * @return bool
     */
    protected static function _validateFile( $subject, $readable = true,
        $throw_exception = false )
    {
        $class = get_called_class();
        $valid = file_exists( $subject );
        $expected_type = 'existing file';
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param type $subject
     * @param type $readable
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for
     *     calling objects to raise their own.
     * @return bool
     */
    protected static function _validateDirectory( $subject, $readable = true,
        $throw_exception = false )
    {
        $class = get_called_class();
        $valid = is_dir( $subject );
        $expected_type = 'existing directory';
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param type $path
     * @param mixed $unused (optional) no-op, just a placeholder for
     *     the public validate method
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for calling
     *     objects to raise their own.
     * @return bool
     */
    protected static function _validateWritable( $path, $unused = null,
        $throw_exception = false )
    {
        $class = get_called_class();
        $valid = is_writable( $path );
        $expected_type = 'writable file or directory';
        $received_type = $class::_validatorGetRecievedType( $path );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param type $path
     * @param mixed $unused (optional) no-op, just a placeholder for the
     *     public validate method
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for calling
     *     objects to raise their own.
     * @return bool
     */
    protected static function _validateExecutable( $path, $unused = null,
        $throw_exception = false )
    {
        $class = get_called_class();
        $valid = is_executable( $path );
        $expected_type = 'executable file';
        $received_type = $class::_validatorGetRecievedType( $path );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param mixed $subject
     * @param mixed $range
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for calling
     *     objects to raise their own.
     * @return bool
     */
    protected static function _validateEquals( $subject, $expected,
        $throw_exception = false )
    {
        $class = get_called_class();
        $valid = $subject == $expected;
        $expected_type = $class::_validatorGetRecievedType( $expected );
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param mixed $subject
     * @param mixed $range
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for calling
     *     objects to raise their own.
     * @return bool
     */
    protected static function _validateExact( $subject, $expected,
        $throw_exception = false )
    {
        $class = get_called_class();
        $valid = $subject === $expected;
        $expected_type = $class::_validatorGetRecievedType( $expected );
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param type $subject
     * @param type $range
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for calling
     *     objects to raise their own.
     * @return bool
     */
    protected static function _validateBetween( $subject, array $range,
        $throw_exception = false )
    {
        $class = get_called_class();
        ksort( $range );
        $expected_type = 'value between ' . implode( ' and ', $range );
        $floor = array_shift( $range );
        $ceil = array_pop( $range );
        $valid = $subject >= $floor && $subject <= $ceil;
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param type $subject
     * @param type $object
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for calling
     *     objects to raise their own.
     * @return bool
     */
    protected static function _validatePropertyExists( $subject, $object,
        $throw_exception = false )
    {
        $class = get_called_class();
        try
        {
            $reflector = new \ReflectionClass( $object );
            $valid = $reflector->hasProperty( $subject );
        } catch ( \ReflectionException $e )
        {
            $valid = false;
        }
        $expected_type = 'property of ' . ( is_object( $object )
            ? get_class( $object )
            : $object );
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param type $subject
     * @param type $constant
     * @param type $throw_exception (optional) If this parameter is true,
     *    an InvalidArgumentException will automatically be raised to designate
     *     an invalid parameter, preventing the need for calling objects to
     *     raise their own.
     * @return bool
     */
    protected static function _validateConstantExists( $subject, $object = null,
        $throw_exception = false )
    {
        $class = get_called_class();
        $const = (string) $subject;
        if ( !is_null( $object ) && ( is_object( $object ) || interface_exists( $object ) || class_exists( $object ) ) )
        {
            $const = get_class( $object ) . '::' . (string) $subject;
            $valid = defined( $const );
        }
        $expected_type = (!is_null( $object ) && ( is_object( $object ) || interface_exists( $object ) ||
            class_exists( $object ) )
            ? 'class constant of ' . $class::_validatorGetRecievedType( $object )
            : 'defined constant' );
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param type $subject
     * @param type $method
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for calling
     *     objects to raise their own.
     * @return bool
     */
    protected static function _validateMethodExists( $subject, $method,
        $throw_exception = false )
    {
        $class = get_called_class();
        $valid = method_exists( $subject, $method );
        $expected_type = 'object with given method';
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param string|scalar|object $subject Must be castable to a string.
     *     If an object, must have the __toString method
     * @param mixed $unused (optional) no-op, just a placeholder for the
     *     public validate method
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for calling
     *     objects to raise their own.
     * @return bool
     */
    protected static function _validateStringable( $subject, $unused = null,
        $throw_exception = false )
    {
        $class = get_called_class();
        $valid = is_string( $subject ) || is_scalar( $subject ) || ( is_object( $subject ) &&
            method_exists( $subject, '__toString' ));
        $expected_type = 'string, scalar, or object with __toString';
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param scalar $subject
     * @param mixed $unused (optional) no-op, just a placeholder for the
     *     public validate method
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for calling
     *     objects to raise their own.
     * @return bool
     */
    protected static function _validateScalar( $subject, $unused = null,
        $throw_exception = false )
    {
        $class = get_called_class();
        $valid = is_scalar( $subject );
        $expected_type = 'scalar';
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param type $subject
     * @param type $object
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for calling
     *     objects to raise their own.
     * @return bool
     */
    protected static function _validateCallable( $subject, $object = null,
        $throw_exception = false )
    {
        $class = get_called_class();
        $valid = is_callable( $subject );
        $expected_type = 'callable';
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     *
     * @param type $subject
     * @param type $allow_float
     * @param type $throw_exception (optional) If this parameter is true,
     *     an InvalidArgumentException will automatically be raised to
     *     designate an invalid parameter, preventing the need for calling
     *     objects to raise their own.
     * @return bool
     */
    protected static function _validateNumber( $subject, $allow_float = true,
        $throw_exception = false )
    {
        $class = get_called_class();
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$allow_float )
        {
            $expected_type = 'int or numeric string';
            $valid = ( is_int( $subject ) || (string) intval( $subject ) === $subject );
        } else
        {
            $expected_type = 'int, float, or numeric string';
            $valid = ( is_int( $subject )
                || is_float( $subject ) || (string) intval( $subject ) === $subject );
        }
        $received_type = $class::_validatorGetRecievedType( $subject );
        if ( !$valid && $throw_exception )
        {
            throw self::_getException( 'invalid-argument-exception',
                'data-validation-invalid-format',
                array(
                'provided' => $received_type,
                'expected' => $expected_type ) );
        }
        return $valid;
    }

    /**
     * Gets the type designation of the provided subject
     * @param type $subject
     * @return type
     */
    private static function _validatorGetRecievedType( $subject, $depth = 3 )
    {
        if ( $subject === false )
        {
            return 'boolean: false';
        } elseif ( $subject === true )
        {
            return 'boolean: true';
        } elseif ( $subject === null )
        {
            return 'null';
        } elseif ( is_array( $subject ) )
        {
            $msg = 'array: [ ';
            foreach ( $subject as
                $key =>
                $value )
            {
                $submsg = ( $depth <= 0
                    ? '...'
                    : self::_validatorGetRecievedType( $value, $depth - 1 ));
                $msg .= $key . ': ' . $submsg;
            }
            $msg .= ' ] ';
            return $msg;
        }
        $received = ( is_object( $subject )
            ? 'object: ' . get_class( $subject )
            : gettype( $subject ) . (!is_resource( $subject )
            ? ': ' . (string) $subject
            : 'resource' ) );
        return $received;
    }

}
