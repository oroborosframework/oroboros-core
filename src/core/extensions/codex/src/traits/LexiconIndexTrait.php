<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\traits;

/**
 * <Lexicon Index Trait>
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\codex\interfaces\contract\LexiconIndexContract
 */
trait LexiconIndexTrait
{

    use \oroboros\core\traits\core\BaselineTrait;
    /**
     * Represents the time that the LexiconIndex was last updated.
     * @var string
     */
    private $_lexicon_index_last_update;

    /**
     * Represents the identifying slug key of the LexiconIndex
     * @var string
     */
    private $_lexicon_index_key;

    /**
     * Represents the set of LexiconEntry
     * objects represented by the LexiconIndex
     * @var \oroboros\collection\Collection
     */
    private $_lexicon_index_entries = array();

    /**
     * Represents whether the LexiconIndex has been correctly initialized.
     * @var bool
     */
    private $_lexicon_index_initialized = false;

    /**
     * Represents the object used to package an export or parse an import
     * @var \oroboros\codex\interfaces\contract\LexiconApiContract
     */
    private $_lexicon_index_api;

    /**
     * Indicates the api sourcing method that should be used when
     * generating an export, so that it can be restored automatically
     * in another instance.
     * @var string
     */
    private $_lexicon_index_api_type = 'api';

    /**
     * Indicates the tags associated with the LexiconIndex
     * that compile into the package. These tags cannot be
     * changed if the api is locked.
     *
     * These are persistent, searchable criteria that allow
     * the Lexicon to quickly identify correlation between
     * multiple LexiconIndexes.
     *
     * @var array
     */
    private $_lexicon_index_api_tags = array();

    /**
     * Represents valid api tags. Other api tags not in this set,
     * or that do not conform to the requirements of this set will
     * be rejected. All api tags are required for valid object creation.
     * @var array
     */
    private static $_lexicon_index_api_tags_valid = array(
        'type' => 'string', //Generalized keyword about what the package covers (eg autoloading)
        'api' => 'string', //slug identifier for the package in the lexicon
        'package' => 'string', //Package name. Use the composer install name for simplicity (eg oroboros/core)
        'properties' => 'array', //An array of classes provided by the api. Default empty array.
        'dependencies' => 'array', //An array of dependencies by lexicon api (the "api" key). Default empty array.
        'subpackages' => 'array', //An array of additional integrated packages supplied within the package as part of its source, which may or may not be dependencies. Dependencies not listed as subpackages are expected to be externally provided to use the package. Default empty array.
    );

    /**
     * Default null values for the required api tags.
     * Any api tags not present in this array MUST be
     * supplied to create a LexiconIndex
     * @var array
     */
    private static $_lexicon_index_api_tags_defaults = array(
        'properties' => array(),
        'dependencies' => array(),
        'subpackages' => array(),
    );

    /**
     * Indicates the tags associated with the LexiconIndex.
     * These are persistent, searchable criteria that allow
     * the Lexicon to quickly identify correlation between
     * multiple LexiconIndexes.
     *
     * Meta tags are generated when the LexiconInterface is created,
     * and impart information specific to the individual installation
     * that does not carry across versions of the LexiconIndex from
     * one installation to another. These impart information like the
     * absolute disk path, current git/svn/mercurial checkout, localized
     * signature, environment variables used, configuration files, or
     * other system specific data. These can be changed when the api
     * is locked, so that the api can be correctly configured.
     *
     * @var array
     */
    private $_lexicon_index_meta_tags = array();

    /**
     * Represents valid meta tags. Other meta tags not in this set,
     * or that do not conform to the requirements of this set will
     * be rejected. All meta tags are optional for valid object creation,
     * but must conform to their schema if provided. Default nulled values
     * will be assigned if they are not provided.
     * @var array
     */
    private static $_lexicon_index_meta_tags_valid = array(
        'title' => 'string', //human readable package name
        'description' => 'string', //human readable package description
        'copyright' => 'string', //copyright attribution
        'version' => 'string', //package release version
        'composer' => 'string', //composer require name (eg oroboros/core)
        'composer-repo' => 'string', //composer repository for private package distribution
        'website' => 'string', //package website
        'documentation' => 'string', //package documentation link
        'license' => 'string', //license type (eg: MIT, Apache, etc)
        'author-name' => 'string', //Author name
        'author-email' => 'string', //Author email
        'author-website' => 'string', //Author website
        'repository' => 'string', //Repository link (eg https://github.com/packagename.git)
        'repository-type' => array(
            'git', //indicates the project uses git version control
            'svn', //indicates the project uses subversion version control
            'hg' //indicates the project uses mercurial version control
        ),
        'root-directory' => 'directory', //Root directory of the package in the local installation. Typically automatically determined.
        'environment-variables' => 'array', //Associative array of environment variables used by the package
        'configuration' => 'file', //Optional config file
        'bootload' => 'file', //Bootload file for bootstrapping the package
        'autoload-schema' => array(
            'psr0', //indicates that the package uses Psr0 autoloading
            'psr4', //indicates that the package uses Psr4 autoloading
            'schema', //indicates that the package uses a schema autoloading file
            'classmap', //indicates that the package uses a classmap file
            false //indicates that the project does not autoload, or provides its own autoload mechanism
        ),
        'autoload-root' => 'directory', //The root directory for autoloading to search from, if an autoloading schema is specified.
        'scope' => 'string', //Specific keyword about what the package covers within its type (eg psr4)
        'signature' => 'string', //Provided for locking the LexiconIndex. Should be the a bcrypt hash
        'map' => 'file', //Optional map file, which is a json file that provides more verbose information. Relative to "root-directory". If a map file resolves correctly, any/all keys for loading the LexiconIndex can be supplied there except "map" and "root-directory"
        'category' => 'string', //Contextual category keyword
        'subcategory' => 'string', //Contextual subcategory keyword
        'parent-package' => 'string', //If the package depends on a root package, this indicates what that package is
        'provides-namespace' => 'boolean', //If the package supplies a namespace, this should be that namespace (eg vendor\\packagename)
        'namespace' => 'string', //The namespace used by the package. Usually will match "provides-namespace".
        'parent-namespace' => 'string', //The namespace of the parent package, if there is one.
        'api-type' => array(
            'api', //Indicates that the api reflects an ApiInterface
            'package', //Indicates that the api reflects a json package file
            'subpackage', //Indicates that the api is a sub-package in a package json file
            'composer' //Indicates that the api is designated by a composer.json file
        ),
    );

    /**
     * Default null values for the optional meta tags.
     * Any meta tags not present in this array will be omitted.
     * Meta tags present in this array will default to these values
     * if not supplied.
     * @var array
     */
    private static $_lexicon_index_meta_tags_defaults = array(
        'title' => 'Untitled Package',
        'description' => 'No description provided.',
        'environment-variables' => array(),
        'root-directory' => false,
        'configuration' => false,
        'bootload' => false,
        'autoload-schema' => false,
        'scope' => false,
        'map' => false,
        'parent-package' => false,
        'provides-namespace' => false,
        'namespace' => '\\', //Defaults to the global namespace
        'parent-namespace' => false,
        'api-type' => 'package' //Defaults to a package json file, if exported
    );

    /**
     * Represents the internal Lexicon instance, for self registration
     * @var \oroboros\codex\Lexicon
     */
    private static $_lexicon_index_lexicon;

    /**
     * Indicates the tags associated with the LexiconIndex.
     * These are persistent, searchable criteria that allow
     * the Lexicon to quickly identify correlation between
     * multiple LexiconIndexes.
     *
     * User tags are arbitrary key > value designations assigned arbitrarily,
     * and can be updated even if the object is locked.
     *
     * @var array
     */
    private $_lexicon_index_user_tags = array();

    /**
     * Designates if the api is locked. If so, it will be read only,
     * and will not accept updates, additions, or deletions.
     * @var bool
     */
    private $_lexicon_index_api_locked = false;

    /**
     * Designates the public hash to resolve the
     * signature against if the LexiconIndex is locked.
     * The private signature is not stored internally, and must
     * be externally recorded to unlock the LexiconIndex
     * from read-only status.
     * @var bool
     */
    private $_lexicon_index_api_lock_resolver = null;

    /**
     * Represents the name of the package the LexiconIndex represents.
     * @var string
     */
    private $_lexicon_index_package;

    /**
     * Represents the name of the parent package, if one exists.
     * If there is no parent package, the value will be false.
     * @var string|bool
     */
    private $_lexicon_index_parent_package = false;

    /**
     * Represents subpackages of the LexiconIndex, which have their
     * own index but are distributed under the same package.
     * @var array
     */
    private $_lexicon_index_subpackages = array();

    /**
     * Represents dependencies of the LexiconIndex, which must be present
     * for its LexiconEntries to be in a usable state.
     * @var array
     */
    private $_lexicon_index_dependencies = array();

    /**
     * Represents the LexiconArchive instance wrapping this LexiconEntry
     * @var \oroboros\codex\interfaces\contract\LexiconArchiveContract
     */
    private $_lexicon_index_archive;

    /**
     * Represents the valid api sourcing methods that can be used
     * to autoload a new LexiconIndex automatically.
     * @var array
     */
    private static $_lexicon_index_api_types_valid = array(
        'api', //Indicates that the api reflects an ApiInterface
        'package', //Indicates that the api reflects a json package file
        'subpackage', //Indicates that the api is a sub-package in a package json file
        'composer' //Indicates that the api is designated by a composer.json file
    );

    /**
     * Provides exception support for standardized messages.
     * @var \oroboros\core\utilities\exception\ExceptionUtility
     */
    private static $_lexicon_index_exception_utility;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\codex\LexiconIndexContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Lexicon Index Unserialize Method>
     * This will restore a LexiconIndex from a serialized state,
     * and allow it to rebuild its index of LexiconEntry objects.
     * @param string $name The slug name that the LexiconIndex will be referred to by
     * @param array|\oroboros\collection\interfaces\contract\CollectionContract $entries (optional) The index may be constructed with a predefined set of LexiconEntries, or they may be defined later.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid parameter is passed, or if the LexiconIndex name is already taken and would cause an ambiguous reference
     */
    public function __construct( $name, $entries = array(), $flags = array() )
    {
        $this->_lexiconIndexInitialize( $name, $entries );
    }

    /**
     * <Lexicon Index Cache Save Method>
     * Saves the LexiconIndex to a disk cache if
     * possible in the current environment.
     * @param string $path The absolute filepath of the save location.
     * @return bool Returns true if the save attempt was successful, false otherwise.
     */
    public function save( $path )
    {
        return $this->_lexiconIndexArchiveSet( $path );
    }

    /**
     * <Lexicon Index Cache Load Versions Method>
     * Loads all existing versions of an existing LexiconIndex from disk,
     * if any entries currently exist. If any do, it will return a collection
     * of the cached LexiconIndexes. If not, it will return false,
     * indicating that no previous versions have been saved.
     * @param string $key
     * @param string $path
     * @return bool|\oroboros\collection\interfaces\contract\CollectionContract
     */
    public static function versions( $key, $path )
    {
        return self::_lexiconIndexArchiveRestore( $key, $path, true );
    }

    /**
     * <Lexicon Index Cache Load Method>
     * Loads an existing LexiconIndex from disk, if an entry currently exists.
     * If one does, it will return the cached LexiconIndex. If not,
     * it will return false, indicating that full instantiation is necessary.
     * @param string $key
     * @param string $path
     * @return bool|\oroboros\codex\interfaces\contract\LexiconIndexContract
     */
    public static function load( $key, $path )
    {
        return self::_lexiconIndexArchiveRestore( $key, $path );
    }

    /**
     * Validates a set of data to see if a valid LexiconIndex can be created from it.
     * @param array $data the data to evaluate
     * @param bool $partial (optional) if true, will validate a partial data set, if false, will evaluate for a full data set. Default false.
     * @param bool $return_errors (optional) If true, will return an array of the problematic supplied keys instead of a boolean value on failure, but will still return true on success. Default false.
     * @return bool|array If the third parameter is true, returns an array of errors on error, otherwise returns true on success and false on failure.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided parameter is not an array
     */
    public static function validate( $data, $partial = false,
        $return_errors = false )
    {
        if ( !is_array( $data ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                'array',
                gettype( $data ) ) );
        }
        return self::_lexiconIndexValidateData( $data, $partial, $return_errors );
    }

    /**
     * Returns an array of expected and optional keys,
     * and what format they are required to be to pass
     * validation of a proposed data set for creating
     * a new LexiconIndex.
     * @return array
     */
    public static function showValid()
    {
        return self::_lexiconIndexGetValidationParams();
    }

    /**
     * <Lexicon Index Last Update Getter>
     * Returns the timestamp that the LexiconIndex was last updated,
     * or null if it has not been recorded.
     * @return string|null
     */
    public function getLastUpdate()
    {
        return $this->_lexicon_index_last_update;
    }

    /**
     * <Lexicon Index Last Update Getter>
     * Returns the identifying key of the LexiconIndex.
     * @return string
     */
    public function getKey()
    {
        return $this->_lexicon_index_key;
    }

    /**
     * <Lexicon Index Serialize Method>
     * This will instruct PHP to only serialize
     * parameters that are unique to the entry itself.
     * @return string
     */
    public function serialize()
    {
        $entries = array();
        foreach ( $this->_lexicon_index_entries as
            $key =>
            $value )
        {
            $entries[$key] = serialize( clone $value );
        }
        return serialize( array(
            'key' => $this->_lexicon_index_key,
            'api' => $this->_lexicon_index_api,
            'api-type' => $this->_lexicon_index_api_type,
            'locked' => $this->_lexicon_index_api_locked,
            'lock-resolver' => $this->_lexicon_index_api_lock_resolver,
            'api-tags' => $this->_lexicon_index_api_tags,
            'meta-tags' => $this->_lexicon_index_meta_tags,
            'user-tags' => $this->_lexicon_index_user_tags,
            'package' => $this->_lexicon_index_package,
            'parent-package' => $this->_lexicon_index_parent_package,
            'subpackages' => $this->_lexicon_index_subpackages,
            'dependencies' => $this->_lexicon_index_dependencies,
            'updated' => $this->_lexicon_index_last_update,
            'entries' => $entries,
            ) );
    }

    /**
     * <Lexicon Index Unserialize Method>
     * Provides a means to restore a saved LexiconIndex from a lightweight
     * string to full functionality. Only the details required to restore the
     * the Lexicon to it's original unique state are reflected, and all others
     * are lazy loaded on request.
     * @param string $serialized
     * @return void
     */
    public function unserialize( $serialized )
    {
        $this->_baselineGetObjectFingerprint();
        $unserialized = unserialize( $serialized );
        $entries = array();
        foreach ( $unserialized['entries'] as
            $key =>
            $entry )
        {
            $entries[$key] = unserialize( $entry );
        }
        $this->_lexiconIndexValidateEntries( $entries );
        $this->_lexicon_index_key = $unserialized['key'];
        $this->_lexicon_index_api = $unserialized['api'];
        $this->_lexicon_index_api_type = $unserialized['api-type'];
        $this->_lexicon_index_api_locked = $unserialized['locked'];
        $this->_lexicon_index_api_lock_resolver = $unserialized['lock-resolver'];
        $this->_lexicon_index_api_tags = $unserialized['api-tags'];
        $this->_lexicon_index_meta_tags = $unserialized['meta-tags'];
        $this->_lexicon_index_user_tags = $unserialized['user-tags'];
        $this->_lexicon_index_package = $unserialized['package'];
        $this->_lexicon_index_parent_package = $unserialized['parent-package'];
        $this->_lexicon_index_subpackages = $unserialized['subpackages'];
        $this->_lexicon_index_dependencies = $unserialized['dependencies'];
        $this->_lexicon_index_last_update = $unserialized['updated'];
        $this->_lexicon_index_entries = new \oroboros\collection\Collection( $entries );
        $this->_lexicon_index_initialized = true;
    }

    /**
     * <Lexicon Entry List Method>
     * Provides a list of the keys associated with all classes, traits, interfaces,
     * or functions in its collection of assets.
     * @return array
     */
    public function listEntries()
    {
        return array_keys( $this->_lexicon_index_entries->toArray() );
    }

    /**
     * <Lexicon Entry Check Method>
     * Checks if the index has a specific class, trait, interface,
     * or function in its collection of assets.
     * @param string $name the fully qualified name (including namespace), or slug keyword associated with the LexiconEntry
     * @return bool
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the given parameter is not a string
     */
    public function checkEntry( $name )
    {
        if ( !is_string( $name ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                'string',
                gettype( $name ) ) );
        }
        return $this->_lexicon_index_entries->has( $name );
    }

    /**
     * <Lexicon Entry Getter Method>
     * Fetches an instance of the LexiconEntry associated with the given name from the LexiconIndex
     * @param string $name the fully qualified name (including namespace), or slug keyword associated with the LexiconEntry
     * @return \oroboros\codex\interfaces\contract\LexiconEntryContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid LexiconEntry is passed or the key is not a string
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex if an unknown key is passed
     */
    public function pullEntry( $name )
    {
        return $this->_lexiconIndexGetEntry( $name );
    }

    /**
     * <Lexicon Entry Setter Method>
     * Adds a new resource to the index.
     * @param string $name the fully qualified name (including namespace), or slug keyword associated with the LexiconEntry
     * @param \oroboros\codex\interfaces\contract\LexiconEntryContract $entry The LexiconEntry instance representing the given asset
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid LexiconEntry is passed or the key is not a string
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function pushEntry( $name, $entry )
    {
        $this->_lexiconIndexAddEntry( $name, $entry );
        $this->_lexiconIndexUpdateTimestamp();
    }

    /**
     * <Package Getter Method>
     * Gets the name of the package, subpackage, category, or subcategory represented by the LexiconIndex.
     * @return string|bool Returns false if no package name is set, otherwise returns the package name
     */
    public function getPackage()
    {
        return $this->_lexiconIndexGetPackage();
    }

    /**
     * <Package Setter Method>
     * Sets a name for the package, subpackage, category, or subcategory represented by the LexiconIndex
     * @param string $package the slug name of the package, subpackage, category, or subcategory
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid package name is passed
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function setPackage( $package )
    {
        $this->_lexiconIndexSetPackage( $package );
        $this->_lexiconIndexUpdateTimestamp();
    }

    /**
     * <Api Getter Method>
     * Fetches the name of the api associated with the LexiconIndex.
     * This will either be the namespace of an ApiInterface, or the
     * disk location of a package json or composer.json file.
     *
     * This method will return false if no api is associated
     * with the LexiconIndex.
     *
     * @return string|bool Either the namespace of the ApiInterface or the disk location of the source file, or false if no api is associated
     */
    public function getApi()
    {
        return $this->_lexiconIndexGetApi();
    }

    /**
     * <Api Setter Method>
     * Sets an api source for the LexiconIndex. The source can be synchronized
     * with the Api, and if the Api is not locked, it can be automatically
     * updated also.
     * @param string $api Either the namespace of the ApiInterface or the disk location of the source file
     * @param string $type api|package|subpackage|composer
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the provided api is not a string, or is not a valid api type
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status, or the given api type is not yet implemented
     */
    public function setApi( $api, $type = 'api' )
    {
        return $this->_lexiconIndexSetApi( $api, $type );
        $this->_lexiconIndexUpdateTimestamp();
    }

    /**
     * <Api Type Getter Method>
     * Fetches the type of api associated with the LexiconIndex.
     *
     * This method will return false if no api is associated
     * with the LexiconIndex.
     *
     * @return string|bool api|package|subpackage|composer, or false if no api is associated
     */
    public function getApiType()
    {
        return $this->_lexicon_index_api_type;
    }

    /**
     * <Api Lock Method>
     * Sets the lock status of the Lexicon Index to true, putting it into a read-only state.
     * The Lexicon Index and all further archives of it will be updated to be read-only until it is unlocked.
     * This can also be written into exports, making all further copies of the Lexicon Entry
     * loaded elsewhere also locked in a read-only state.
     *
     * This method will return a signature key for the Lexicon Index that can
     * be used to unlock it. This is done using a bcrypt hashing algorithm,
     * so no discernable details of the signature are stored in exports or
     * archives. The original developer can unlock any copy of the
     * Lexicon Entry with the provided key.
     *
     * @param string $signature (optional) An existing key may be provided to allow for a developer to lock multiple Apis under the same key. If this is not provided, a unique key will be generated for each api.
     * @return string This method will return a signature that can be used to unlock the Lexicon entry. If this is not retained, it will be permanently locked. unless it is purged and entirely rebuilt.
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException Not yet implemented
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function lockApi( $signature = null )
    {
        $this->_lexiconIndexCheckLock( __METHOD__ );
        throw new \oroboros\core\utilities\exception\codex\LexiconIndexException(
        sprintf( 'Error encountered at [%s]. This functionality is not yet implemented.',
            __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE
        );
        $this->_lexiconIndexUpdateTimestamp();
    }

    /**
     * <Api Unlock Method>
     * Sets the lock status of the Lexicon Index to false, allowing the
     * Lexicon Index to be edited further. This can only be done with the
     * provided key that was given when it was originally locked.
     *
     * @param string $signature (optional) An existing key may be provided to allow for a developer to lock multiple Apis under the same key. If this is not provided, a unique key will be generated for each api.
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException Not yet implemented
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function unlockApi( $signature )
    {
        $this->_lexiconIndexCheckLock( __METHOD__ );
        throw new \oroboros\core\utilities\exception\codex\LexiconIndexException(
        sprintf( 'Error encountered at [%s]. This functionality is not yet implemented.',
            __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE
        );
        $this->_lexiconIndexUpdateTimestamp();
    }

    /**
     * <Api Import Method>
     * Automatically generates a fully populated LexiconIndex to
     * represent a package or subpackage. Supports ApiInterfaces,
     * Oroboros package json files, and composer.json files.
     * @param string $source Either the namespace of the ApiInterface or the disk location of the source file if it is a composer.json or package json file
     * @param string $type api|package|subpackage|composer
     * @return \oroboros\codex\interfaces\contract\LexiconIndexContract
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException If an unimplemented method is called
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the api could not be parsed correctly.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid parameter is passed
     */
    public static function import( $source, $type = 'api' )
    {
        return self::_lexiconIndexImportApi( $source, $type );
    }

    /**
     * <Lexicon Index Export Method>
     * Exports the Lexicon Index into a production format,
     * which can be either an ApiInterface, package json file, or composer.json.
     *
     * Subpackages will export a package as if they were a primary package.
     * This is to enable quickly parting out subpackages into individual
     * deliverables as needed. If you would like the subpackage to be reflected
     * in the api, export from the parent package instead.
     *
     * @param array $options To be determined
     * @return string The namespace of the generated ApiInterface, or the disk location of the package json or composer.json
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException Not yet implemented
     */
    public function export( $options = array() )
    {
        return $this->_lexiconIndexExport( $options );
    }

    /**
     * <Lexicon Merge Method>
     * This method will merge another given LexiconIndex with the current object,
     * and return a new LexiconIndex that contains the full collection of both combined.
     * In the event of conflicting entries, the newer entry will be trusted.
     *
     * This method treats both current LexiconIndex objects as immutable,
     * and will not modify either, but instead will return a new
     * third LexiconIndex reflecting the changes.
     *
     * @param string $new_key the identifying key to issue to the new LexiconIndex
     * @param \oroboros\codex\interfaces\contract\LexiconIndexContract $index
     * @return \oroboros\codex\interfaces\contract\LexiconIndexContract
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException Not yet implemented
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function merge( $new_key, $index )
    {
        return $this->_lexiconIndexCreateMergeIndex( $new_key, $index );
    }

    /**
     * <Lexicon Index Subpackage Setter Method>
     * Sets another LexiconIndex as a subpackage of the current LexiconIndex.
     * The current LexiconIndex will register itself as a dependency of the
     * subpackage in the process.
     * @param \oroboros\codex\interfaces\contract\LexiconIndexContract $index
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the supplied index is not a LexiconIndex
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function setSubpackage( $index )
    {
        $this->_lexiconIndexCheckLock( __METHOD__ );
        $this->_lexiconIndexSetSubpackage( $index );
        $this->_lexiconIndexUpdateTimestamp();
    }

    /**
     * <Lexicon Index Subpackage Remove Method>
     * Removes a subpackage of the current LexiconIndex.
     * The current LexiconIndex will register itself as a dependency of the
     * subpackage in the process.
     *
     * @param \oroboros\codex\interfaces\contract\LexiconIndexContract $index
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException Not yet implemented
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function removeSubpackage( $index )
    {
        $this->_lexiconIndexCheckLock( __METHOD__ );
        $this->_lexiconIndexRemoveSubpackage( $index );
        $this->_lexiconIndexUpdateTimestamp();
    }

    /**
     * <Lexicon Index Dependency Setter Method>
     * Adds another LexiconIndex as a dependency of the current one.
     * @param \oroboros\codex\interfaces\contract\LexiconIndexContract $index
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid LexiconEntry is passed
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function setDependency( $index )
    {
        $this->_lexiconIndexCheckLock( __METHOD__ );
        $this->_lexiconIndexSetDependency( $index );
        $this->_lexiconIndexUpdateTimestamp();
    }

    /**
     * <Lexicon Index Dependency Delete Method>
     * Deletes a dependency of the current one.
     * @param string $index The identifying package of the dependency LexiconIndex, or the LexiconIndex itself to extract the package from
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException If the dependency is indirect, and cannot be removed without also removing the direct dependency (or chain of dependency leading to the specified dependent)
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function removeDependency( $key )
    {
        $this->_lexiconIndexCheckLock( __METHOD__ );
        $this->_lexiconIndexRemoveDependency( $key );
        $this->_lexiconIndexUpdateTimestamp();
    }

    /**
     * <Lexicon Index Dependency Check Method>
     * Returns a boolean determination as to whether the LexiconIndex
     * has dependencies.
     * @return bool
     */
    public function hasDependencies()
    {
        return ( empty( $this->_lexicon_index_dependencies )
            ? false
            : true );
    }

    /**
     * <Lexicon Index Dependent Check Method>
     * Returns a boolean determination as to whether the LexiconIndex
     * depends on a specific dependency.
     * @param string $key The identifying package of the dependency to check.
     * @return bool
     */
    public function hasDependency( $key )
    {
        if ( !$this->hasDependencies() )
        {
            return false;
        }
        if ( array_key_exists( $key, $this->_lexicon_index_dependencies ) )
        {
            return true;
        }
        $has = false;
        foreach ( $this->_lexicon_index_dependencies as
            $dependency )
        {
            if ( $dependency->hasDependency( $key ) )
            {
                $has = true;
                break;
            }
        }
        return $has;
    }

    /**
     * <Lexicon Index Dependency Getter Method>
     * Returns a collection of the existing dependencies of the LexiconIndex.
     * @return \oroboros\collection\interfaces\contract\CollectionContract
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException Not yet implemented
     */
    public function listDependencies()
    {
        return new \oroboros\collection\Collection( $this->_lexicon_index_dependencies );
    }

    /**
     * <Lexicon Index Api Tag Setter Method>
     * Sets a searchable api tag on the LexiconIndex.
     * These most often correspond to DocBlock parameters that
     * should be included in the Api or classes within it.
     * @param string $tag
     * @param string $value
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the tag already exists
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function apiTag( $tag, $value )
    {
        $this->_lexiconIndexCheckLock( __METHOD__ );
        if ( is_string( $tag ) && array_key_exists( $tag,
                $this->_lexicon_index_api_tags ) )
        {
            self::$_lexicon_index_exception_utility->throwException(
                'lexicon-index',
                sprintf( 'Error encountered at [%s]. Specified tag [%s] cannot be created '
                    . 'because it already exists. Use the updateTag() method to alter it.',
                    __METHOD__, $tag ), 'ambiguous-reference' );
        }
        $this->_lexiconIndexSetApiTag( $tag, $value );
        $this->_lexiconIndexUpdateTimestamp();
    }

    /**
     * <Lexicon Index Tag Setter Method>
     * Sets a searchable meta tag on the LexiconIndex.
     * These most often correspond to DocBlock parameters that
     * should be included in the Api or classes within it.
     * @param string $tag
     * @param string $value
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the tag already exists
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function metaTag( $tag, $value )
    {
        $this->_lexiconIndexCheckLock( __METHOD__ );
        if ( is_string( $tag ) && array_key_exists( $tag,
                $this->_lexicon_index_api_tags ) )
        {
            self::$_lexicon_index_exception_utility->throwException( 'lexicon-index',
                sprintf( 'Error encountered at [%s]. Specified tag [%s] cannot be created '
                    . 'because it already exists. Use the updateTag() method to alter it.',
                    __METHOD__, $tag ), 'ambiguous-reference' );
        }
        $this->_lexiconIndexSetMetaTag( $tag, $value );
        $this->_lexiconIndexUpdateTimestamp();
    }

    /**
     * <Lexicon Index User Tag Setter Method>
     * Sets a searchable tag on the LexiconIndex.
     * These most often correspond to DocBlock parameters that
     * should be included in the Api or classes within it.
     * @param string $tag
     * @param string $value
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the tag already exists
     */
    public function tag( $tag, $value )
    {
        if ( is_string( $tag ) && array_key_exists( $tag,
                $this->_lexicon_index_api_tags ) )
        {
            self::$_lexicon_index_exception_utility->throwException( 'lexicon-index',
                sprintf( 'Error encountered at [%s]. Specified tag [%s] cannot be created '
                    . 'because it already exists. Use the updateTag() method to alter it.',
                    __METHOD__, $tag ), 'ambiguous-reference' );
        }
        $this->_lexiconIndexSetUserTag( $tag, $value );
        $this->_lexiconIndexUpdateTimestamp();
    }

    /**
     * <Lexicon Index Api Tag Update Method>
     * Alters the value of an existing api tag.
     * @param string $tag
     * @param string $value
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the tag does not exist
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function updateApiTag( $tag, $value )
    {
        $this->_lexiconIndexCheckLock( __METHOD__ );
        if ( is_string( $tag ) && !array_key_exists( $tag,
                $this->_lexicon_index_api_tags ) )
        {
            self::$_lexicon_index_exception_utility->throwException( 'lexicon-exception',
                sprintf( 'Error encountered at [%s]. Specified tag [%s] cannot be updated '
                    . 'because it does not exist. Use the tag() method to create it.',
                    __METHOD__, $tag ), 'error-core' );
        }
        $this->_lexiconIndexSetApiTag( $tag, $value );
        $this->_lexiconIndexUpdateTimestamp();
    }

    /**
     * <Lexicon Index Meta Tag Update Method>
     * Alters the value of an existing meta tag.
     * @param string $tag
     * @param string $value
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the tag does not exist
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function updateMetaTag( $tag, $value )
    {
        $this->_lexiconIndexCheckLock( __METHOD__ );
        if ( is_string( $tag ) && !array_key_exists( $tag,
                $this->_lexicon_index_api_tags ) )
        {
            self::$_lexicon_index_exception_utility->throwException( 'lexicon-exception',
                sprintf( 'Error encountered at [%s]. Specified tag [%s] cannot be updated '
                    . 'because it does not exist. Use the tag() method to create it.',
                    __METHOD__, $tag ), 'error-core' );
        }
        $this->_lexiconIndexSetTag( $tag, $value );
        $this->_lexiconIndexUpdateTimestamp();
    }

    /**
     * <Lexicon Index User Tag Update Method>
     * Alters the value of an existing user tag.
     * @param string $tag
     * @param string $value
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the tag does not exist
     */
    public function updateTag( $tag, $value )
    {
        if ( is_string( $tag ) && !array_key_exists( $tag,
                $this->_lexicon_index_api_tags ) )
        {
            self::$_lexicon_index_exception_utility->throwException( 'lexicon-exception',
                sprintf( 'Error encountered at [%s]. Specified tag [%s] cannot be updated '
                    . 'because it does not exist. Use the tag() method to create it.',
                    __METHOD__, $tag ), 'error-core' );
        }
        $this->_lexiconIndexSetUserTag( $tag, $value );
        $this->_lexiconIndexUpdateTimestamp();
    }

    /**
     * <Lexicon Index Api Tag Delete Method>
     * Deletes an existing api tag from the Lexicon Index.
     * @param type $tag
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function deleteApiTag( $tag )
    {
        $this->_lexiconIndexCheckLock( __METHOD__ );
        $this->_lexiconIndexDeleteApiTag( $tag );
        $this->_lexiconIndexUpdateTimestamp();
    }

    /**
     * <Lexicon Index Meta Tag Delete Method>
     * Deletes an existing meta tag from the Lexicon Index.
     * @param type $tag
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function deleteMetaTag( $tag )
    {
        $this->_lexiconIndexCheckLock( __METHOD__ );
        $this->_lexiconIndexDeleteMetaTag( $tag );
        $this->_lexiconIndexUpdateTimestamp();
    }

    /**
     * <Lexicon Index Tag Delete Method>
     * Deletes an existing user tag from the Lexicon Index.
     * @param type $tag
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    public function deleteTag( $tag )
    {
        $this->_lexiconIndexDeleteUserTag( $tag );
        $this->_lexiconIndexUpdateTimestamp();
    }

    /**
     * <Lexicon Index Api Tag Getter Method>
     * Gets a collection of the existing tags on the LexiconIndex.
     * @param type $tag
     * @return \oroboros\collection\interfaces\contract\CollectionContract
     */
    public function getApiTags()
    {
        return new \oroboros\collection\Collection( $this->_lexicon_index_api_tags );
    }

    /**
     * <Lexicon Index Valid Api Tag Getter Method>
     * Gets a collection of the valid api tag keys.
     * @return array
     */
    public static function getValidApiTags()
    {
        return array_keys( self::$_lexicon_index_api_tags_valid );
    }

    /**
     * <Lexicon Index Default Api Tag Getter Method>
     * Gets the default api tags, for values not supplied.
     * @return array
     */
    public static function getDefaultApiTags()
    {
        return self::$_lexicon_index_api_tags_defaults;
    }

    /**
     * <Lexicon Index Meta Tag Getter Method>
     * Gets a collection of the existing tags on the LexiconIndex.
     * @param type $tag
     * @return \oroboros\collection\interfaces\contract\CollectionContract
     */
    public function getMetaTags()
    {
        return new \oroboros\collection\Collection( $this->_lexicon_index_meta_tags );
    }

    /**
     * <Lexicon Index Valid Meta Tag Getter Method>
     * Gets a collection of the valid meta tag keys.
     * @return array
     */
    public static function getValidMetaTags()
    {
        return array_keys( self::$_lexicon_index_meta_tags_valid );
    }

    /**
     * <Lexicon Index Default Meta Tag Getter Method>
     * Gets the default meta tags, for values not supplied.
     * @return array
     */
    public static function getDefaultMetaTags()
    {
        return self::$_lexicon_index_meta_tags_defaults;
    }

    /**
     * <Lexicon Index Tag Getter Method>
     * Gets a collection of the existing tags on the LexiconIndex.
     * @param type $tag
     * @return \oroboros\collection\interfaces\contract\CollectionContract
     */
    public function getTags()
    {
        return new \oroboros\collection\Collection( $this->_lexicon_index_user_tags );
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Lexicon Index Initialization Method>
     * Initializes the LexiconIndex.
     * @param string $key
     * @param array|\oroboros\collection\interfaces\contract\CollectionContract $entries
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     */
    protected function _lexiconIndexInitialize( $name, $entries = array() )
    {
        if ( is_null( self::$_lexicon_index_lexicon ) )
        {
            self::$_lexicon_index_lexicon = new \oroboros\codex\Lexicon();
        }
        if ( is_null( self::$_lexicon_index_exception_utility ) )
        {
            self::$_lexicon_index_exception_utility = new \oroboros\core\utilities\exception\ExceptionUtility();
        }
        if ( !is_array( $entries ) && (!is_object( $entries ) && ($entries instanceof \oroboros\collection\interfaces\contract\CollectionContract ) ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                'array|\\oroboros\\collection\\interfaces\\contract\\CollectionContract',
                gettype( $entries ) ) );
        }
        $this->_baselineInitialize();
        if ( !is_string( $name ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters', array(
                'string',
                gettype( $name ) ) );
        }
        $this->_lexiconIndexValidateName( $name );
        $this->_lexiconIndexValidateEntries( $entries );
        $this->_lexicon_index_key = $name;
        if ( is_array( $entries ) )
        {
            $entries = new \oroboros\collection\Collection( $entries );
        }
        $this->_lexicon_index_key = $name;
        $this->_lexicon_index_entries = $entries;
        $this->_lexicon_index_initialized = true;
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Updates the LexiconIndex last update timestamp.
     * @return void
     */
    private function _lexiconIndexUpdateTimestamp()
    {
        $time = gmdate( 'Y-m-d H:i:s' );
        $this->_lexicon_index_last_update = $time;
    }

    /**
     * Validates the name provided at instantiation, to insure that it is valid
     * @param string $key
     */
    private function _lexiconIndexValidateName( $name )
    {
        //no-op for now
    }

    /**
     * Validates provided LexiconEntry objects to insure
     * they honor their contract interface.
     * @param type $entries
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If one or more invalid entries are detected. The message will relay which key(s) from the provided array were incorrect.
     */
    private function _lexiconIndexValidateEntries( $entries )
    {
        $invalid = array();
        foreach ( $entries as
            $key =>
            $entry )
        {
            if ( !($entry instanceof \oroboros\codex\interfaces\contract\LexiconEntryContract) )
            {
                $invalid[] = $key;
            }
        }
        if ( !empty( $invalid ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( 'Error encountered at [%s]. The following supplied keys are not valid LexiconEntry instances [%s].',
                __METHOD__, implode( ', ', $invalid ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
    }

    /**
     *
     * @param \oroboros\core\inerfaces\contract\libraries\codex\LexiconEntryContract $entry
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid LexiconEntry is passed or the key is not a string
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    private function _lexiconIndexAddEntry( $key, $entry )
    {
        if ( !is_string( $key ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                'string',
                gettype( $key ) ) );
        }
        if ( !( $entry instanceof \oroboros\core\inerfaces\contract\libraries\codex\LexiconEntryContract ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( 'Error encountered at [%s]. The supplied [%s] is not a valid instance of [%s].',
                __METHOD__, '$entry',
                '\\oroboros\\codex\\interfaces\\contract\\LexiconEntryContract' ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        $this->_lexiconIndexCheckLock( __METHOD__ );
        $this->_lexicon_index_entries[$key] = $entry;
    }

    /**
     *
     * @param \oroboros\core\inerfaces\contract\libraries\codex\LexiconEntryContract $entry
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid LexiconEntry is passed or the key is not a string
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    private function _lexiconIndexGetEntry( $key )
    {
        if ( !is_string( $key ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                'string',
                gettype( $key ) ) );
        }
        if ( !$this->_lexicon_index_entries->has( $key ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( 'Error encountered at [%s]. The supplied [%s] is not a LexiconEntry that exists in [%s].',
                __METHOD__, $key, get_class( $this ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        return $this->_lexicon_index_entries[$key];
    }

    /**
     * Throws an exception to block destructive changes if the
     * LexiconEntry is in read-only status. This method is called
     * by all methods that make changes to the LexiconIndex.
     * @param string $method The name of the method that called this method, typically passed as __METHOD__
     * @return void
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    private function _lexiconIndexCheckLock( $method )
    {
        if ( $this->_lexicon_index_api_locked )
        {
            throw new \oroboros\core\utilities\exception\codex\LexiconIndexException(
            sprintf( 'Error encountered at [%s]. This Api is locked in a read-only state, and cannot be modified.',
                $method ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_SECURITY_LOCKED_OBJECT
            );
        }
    }

    /**
     * Sets a name for the package, if the LexiconIndex is not in a read-only state
     * @param string $package
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid package name is passed
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    private function _lexiconIndexSetPackage( $package )
    {
        if ( !is_string( $package ) && $package !== false )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                'string',
                gettype( $package ) ) );
        }
        $this->_lexiconIndexCheckLock( __METHOD__ );
        $this->_lexicon_index_package = $package;
    }

    /**
     * Returns the package name, or false if one has not been set.
     * @return string
     */
    private function _lexiconIndexGetPackage()
    {
        return $this->_lexicon_index_package;
    }

    /**
     * Returns the api associated with the LexiconIndex,
     * which will either be the namespace of an ApiInterface,
     * the file path of a package json or composer.json file,
     * or false if there is no api associated with the LexiconIndex.
     * @return string
     */
    private function _lexiconIndexGetApi()
    {
        return $this->_lexicon_index_api;
    }

    /**
     * Sets the api associated with the LexiconIndex,
     * which will either be the namespace of an ApiInterface,
     * the file path of a package json or composer.json file,
     * or false if there is no api associated with the LexiconIndex.
     * @param string $api The source of the api, which is either a fully qualified ApiInterface instance, or the disk location of a settings json or composer.json file
     * @param string $type api|package|subpackage|composer
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the provided api is not a string
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException if the LexiconIndex is in read-only status
     */
    private function _lexiconIndexSetApi( $api, $type )
    {
        if ( !is_string( $api ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                'string',
                gettype( $api ) ) );
        }
        $this->_lexiconIndexCheckLock( __METHOD__ );
        switch ( $type )
        {
            case 'api':
                $this->_lexiconIndexSetApiInterface( $api );
                break;
            case 'package':
                $this->_lexiconIndexSetApiPackage( $api );
                break;
            case 'subpackage':
                $this->_lexiconIndexSetApiSubpackage( $api );
                break;
            case 'composer':
                $this->_lexiconIndexSetApiComposer( $api );
                break;
            default:
                throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                    'logic-bad-parameters',
                    array(
                    implode( '|', self::$_lexicon_index_api_types_valid ),
                    gettype( $api ) ) );
                break;
        }
        $this->_lexicon_index_api_type = $type;
    }

    /**
     * Sets the api as an ApiInterface, if the given parameter is valid.
     * @param string $api
     */
    private function _lexiconIndexSetApiInterface( $api )
    {
        $msg = false;
        $msg_template = 'Error encountered at [%s]. %s The given parameter MUST be an interface that extends [%s].';
        $code = \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE;
        try
        {
            //Try to load a reflector of the api,
            //which will resolve if it is an interface
            $check = new \oroboros\core\utilities\reflection\Reflection( $api );
            if ( !method_exists( $check->getReflector(), 'isInterface' ) || !$check->isInterface() )
            {
                throw new \Exception( $api . ' is not an interface.' );
            }
            if ( !is_subclass_of( $api,
                    '\\oroboros\\core\\interfaces\\api\\BaseApi' ) )
            {
                throw new \Exception( $api . ' is not a valid ApiInterface.' );
            }
            $this->_lexicon_index_api = $api;
        } catch ( \oroboros\core\utilities\exception\InvalidArgumentException $e )
        {
            //Not even a php construct at all
            $msg = sprintf( $msg_template, __METHOD__,
                'The given parameter is not a valid PHP construct.',
                '\\oroboros\\core\\interfaces\\api\\BaseApi' );
        } catch ( \Exception $e )
        {
            //Valid object, but either not an interface or not an api interface
            $msg = sprintf( $msg_template, __METHOD__, $e->getMessage(),
                '\\oroboros\\core\\interfaces\\api\\BaseApi' );
        }
        if ( $msg )
        {
            throw new \oroboros\core\utilities\exception\codex\LexiconIndexException( $msg,
            $code );
        }
    }

    /**
     * Sets the api as a package json file, if the given parameter is valid.
     * @param string $api
     */
    private function _lexiconIndexSetApiPackage( $api )
    {
        throw new \oroboros\core\utilities\exception\codex\LexiconIndexException(
        sprintf( 'Error encountered at [%s]. This functionality is not yet implemented.',
            __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE
        );
    }

    /**
     * Sets the api as a package json subpackage, if the given parameter is valid.
     * @param string $api
     */
    private function _lexiconIndexSetApiSubpackage( $api )
    {
        throw new \oroboros\core\utilities\exception\codex\LexiconIndexException(
        sprintf( 'Error encountered at [%s]. This functionality is not yet implemented.',
            __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE
        );
    }

    /**
     * Sets the api as a composer.json, if the given parameter is valid.
     * @param string $api
     */
    private function _lexiconIndexSetApiComposer( $api )
    {
        throw new \oroboros\core\utilities\exception\codex\LexiconIndexException(
        sprintf( 'Error encountered at [%s]. This functionality is not yet implemented.',
            __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE
        );
    }

    /**
     * Sets another LexiconIndex as a direct dependency if this LexiconIndex
     * @param \oroboros\codex\interfaces\contract\LexiconIndexContract $index
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     */
    private function _lexiconIndexSetDependency( $index )
    {
        if ( !( is_object( $index ) && ( $index instanceof \oroboros\codex\interfaces\contract\LexiconIndexContract ) ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                '\\oroboros\\codex\\interfaces\\contract\\LexiconIndexContract',
                is_object( $index )
                    ? get_class( $index )
                    : gettype( $index ) ) );
        }
        $this->_lexicon_index_dependencies[$index->getPackage()] = $index;
    }

    private function _lexiconIndexRemoveDependency( $key )
    {
        if ( $key instanceof \oroboros\codex\interfaces\contract\LexiconIndexContract )
        {
            $key = $key->getPackage();
        }
        if ( array_key_exists( $key, $this->_lexicon_index_dependencies ) )
        {
            unset( $this->_lexicon_index_dependencies[$key] );
            return;
        }
        if ( !$this->hasDependency( $key ) )
        {
            return;
        }
        throw new \oroboros\core\utilities\exception\codex\LexiconIndexException(
        sprintf( 'Error encountered at [%s]. Specified dependent [%s] cannot be removed because it is an indirect dependency.',
            __METHOD__, $key ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE
        );
    }

    /**
     * Adds another LexiconIndex as a subpackage of the current LexiconIndex
     * @param \oroboros\codex\interfaces\contract\LexiconIndexContract $index The subpackage to add
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the supplied index is not a LexiconIndex
     */
    private function _lexiconIndexSetSubpackage( $index )
    {
        if ( !( is_object( $index ) && ( $index instanceof \oroboros\codex\interfaces\contract\LexiconIndexContract ) ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                '\\oroboros\\codex\\interfaces\\contract\\LexiconIndexContract',
                is_object( $index )
                    ? get_class( $index )
                    : gettype( $index ) ) );
        }
        $this->_lexicon_index_subpackages[$index->getPackage()] = $index;
    }

    /**
     * Removes a subpackage of the current LexiconIndex
     * @param \oroboros\codex\interfaces\contract\LexiconIndexContract $index The subpackage to remove
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the supplied index is not a LexiconIndex
     */
    private function _lexiconIndexRemoveSubpackage( $index )
    {
        if ( !( is_object( $index ) && ( $index instanceof \oroboros\codex\interfaces\contract\LexiconIndexContract ) ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                '\\oroboros\\codex\\interfaces\\contract\\LexiconIndexContract',
                is_object( $index )
                    ? get_class( $index )
                    : gettype( $index ) ) );
        }
        if ( array_key_exists( $index->getPackage(),
                $this->_lexicon_index_subpackages ) )
        {
            unset( $this->_lexicon_index_subpackages[$index->getPackage()] );
        }
    }

    private function _lexiconIndexCreateMergeIndex( $new_key, $index )
    {
        if ( !is_string( $new_key ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                'string',
                gettype( $new_key ) ) );
        }
        if ( !( is_object( $index ) && ( $index instanceof \oroboros\codex\interfaces\contract\LexiconIndexContract ) ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                '\\oroboros\\codex\\interfaces\\contract\\LexiconIndexContract',
                is_object( $index )
                    ? get_class( $index )
                    : gettype( $index ) ) );
        }
        $new_data = unserialize( $index->serialize() );
        $old_data = unserialize( $this->serialize() );
        $data = array_replace_recursive( $old_data, $new_data );
//        dd( $data, $old_data, $new_data );
    }

    /**
     *
     * @param type $source
     * @param type $api
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException If an unimplemented method is called
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid parameter is passed
     */
    private static function _lexiconIndexImportApi( $source, $api )
    {
        switch ( $api )
        {
            case 'api':
                return self::_lexiconIndexParseApiInterface( $source );
                break;
            case 'package':
                return self::_lexiconIndexParseApiPackage( $source );
                break;
            case 'subpackage':
                return self::_lexiconIndexParseApiSubackage( $source );
                break;
            case 'composer':
                return self::_lexiconIndexParseApiComposer( $source );
                break;
            default:
                throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                    'logic-bad-parameters',
                    array(
                    implode( '|', self::$_lexicon_index_api_types_valid ),
                    gettype( $api ) ) );
                break;
        }
    }

    /**
     * Returns a new LexiconIndex populated from the parameters set in an ApiInterface.
     * @todo ~0.2.6/0.2.7 Integrate developer key signing into package generation
     * @param string $source The fully qualified name of the ApiInterface
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided interface is not a string
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException If the provided ApiInterface is not valid
     */
    private static function _lexiconIndexParseApiInterface( $source )
    {
        if ( !is_string( $source ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                'string',
                gettype( $source ) ) );
        }
        if ( !is_subclass_of( $source,
                '\\oroboros\\core\\interfaces\\api\\BaseApi' ) )
        {
            throw new \oroboros\core\utilities\exception\codex\LexiconIndexException(
            sprintf( 'Error encountered at [%s]. The provided [%s] is not a valid instance of [%s].',
                __METHOD__, $source,
                '\\oroboros\\core\\interfaces\\api\\BaseApi' ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_SECURITY_LOCKED_OBJECT
            );
        }
        $enumerator = new \oroboros\enum\InterfaceEnumerator( $source );
        $name = $enumerator['API_CODEX'];
        $properties = array(
            'classes' => $enumerator->filterKey( 'CLASS', true )->toArray(),
            'traits' => $enumerator->filterKey( 'TRAIT', true )->toArray(),
            'interfaces' => $enumerator->filterKey( 'INTERFACE', true )->toArray(),
            'contracts' => $enumerator->filterKey( 'CONTRACT', true )->toArray(),
            'enumerators' => $enumerator->filterKey( 'ENUMERATOR', true )->toArray(),
            'functions' => $enumerator->filterKey( 'FUNCTION', true )->toArray()
        );
        $invalid = array();
        $props = array();
        foreach ( $properties as
            $type =>
            $set )
        {
            foreach ( $set as
                $property_name =>
                $property )
            {
                try
                {
                    $prop = new \oroboros\codex\LexiconEntry( $property );
                    $props[str_replace( '_', '-', strtolower( $property_name ) )]
                        = $prop;
                } catch ( \Exception $e )
                {
                    $invalid[$type][] = $property_name;
                }
            }
        }
        $index_class = get_called_class();
        $lex_index = new $index_class( $name, $props, true );
        $lex_index->setApi( $source );
        $lex_index->setPackage( $enumerator->get( 'API_PACKAGE' ) );
        $lex_index->tag( 'type',
            $enumerator->has( 'API_TYPE' )
                ? $enumerator->get( 'API_TYPE' )
                : false  );
        $lex_index->tag( 'scope',
            $enumerator->has( 'API_SCOPE' )
                ? $enumerator->get( 'API_SCOPE' )
                : false  );
        $lex_index->tag( 'category',
            $enumerator->has( 'API_CATEGORY' )
                ? $enumerator->get( 'API_CATEGORY' )
                : false  );
        $lex_index->tag( 'subcategory',
            $enumerator->has( 'API_SUBCATEGORY' )
                ? $enumerator->get( 'API_SUBCATEGORY' )
                : false  );
        $lex_index->tag( 'namespace',
            $enumerator->has( 'API_PROVIDES_NAMESPACE' )
                ? $enumerator->get( 'API_PROVIDES_NAMESPACE' )
                : false  );
        $lex_index->tag( 'parent-namespace',
            $enumerator->has( 'API_PARENT_NAMESPACE' )
                ? $enumerator->get( 'API_PARENT_NAMESPACE' )
                : false  );
        $lex_index->tag( 'map',
            $enumerator->has( 'API_PACKAGE_MAP' )
                ? $enumerator->get( 'API_PACKAGE_MAP' )
                : false  );
        $lex_index->tag( 'parent-package',
            $enumerator->has( 'API_PARENT_PACKAGE' )
                ? $enumerator->get( 'API_PARENT_PACKAGE' )
                : false  );
//        $lex_index->lockApi(); //This will be implemented when key signing is integrated in 0.2.6 or 0.2.7
        return $lex_index;
    }

    /**
     * Returns a new LexiconIndex populated from the parameters set in a package json file.
     * @todo implement api generation via package json
     * @param string $source The fully qualified name of the ApiInterface
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided interface is not a string
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException If the provided ApiInterface is not valid
     */
    private static function _lexiconIndexParseApiPackage( $source )
    {
        throw new \oroboros\core\utilities\exception\codex\LexiconIndexException(
        sprintf( 'Error encountered at [%s]. This functionality is not yet implemented.',
            __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE
        );
    }

    /**
     * Returns a new LexiconIndex populated from the parameters set in a package json file.
     * @todo implement api subpackage generation via package json
     * @param string $source The fully qualified name of the ApiInterface
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided interface is not a string
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException If the provided ApiInterface is not valid
     */
    private static function _lexiconIndexParseApiSubackage( $source )
    {
        throw new \oroboros\core\utilities\exception\codex\LexiconIndexException(
        sprintf( 'Error encountered at [%s]. This functionality is not yet implemented.',
            __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE
        );
    }

    /**
     * Returns a new LexiconIndex populated from the parameters set in a composer.json file.
     * @todo implement api generation from a composer.json
     * @param string $source The fully qualified name of the ApiInterface
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the provided interface is not a string
     * @throws \oroboros\core\utilities\exception\codex\LexiconIndexException If the provided ApiInterface is not valid
     */
    private static function _lexiconIndexParseApiComposer( $source )
    {
        throw new \oroboros\core\utilities\exception\codex\LexiconIndexException(
        sprintf( 'Error encountered at [%s]. This functionality is not yet implemented.',
            __METHOD__ ),
        \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE
        );
    }

    /**
     * Sets an api tag on the LexiconIndex
     * @param string $tag
     * @param string|bool $value
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the tag or value parameters are not strings
     */
    private function _lexiconIndexSetApiTag( $tag, $value )
    {
        if ( $value === 1 )
        {
            $value = true;
        } elseif ( $value === 0 )
        {
            $value = false;
        }
        if ( !is_string( $tag ) || !( is_string( $value ) || is_bool( $value ) ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                'string',
                !is_string( $tag )
                    ? gettype( $tag )
                    : gettype( $value ) ) );
        }
        if ( !array_key_exists( $tag, self::$_lexicon_index_api_tags_valid ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_PHP_KEY_NOT_FOUND_MESSAGE,
                $tag, __METHOD__ ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_KEY_NOT_FOUND
            );
        }
        $this->_lexicon_index_api_tags[$tag] = $value;
    }

    /**
     * Sets a meta tag on the LexiconIndex
     * @param string $tag
     * @param string|bool $value
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the tag or value parameters are not strings
     */
    private function _lexiconIndexSetMetaTag( $tag, $value )
    {
        if ( $value === 1 )
        {
            $value = true;
        } elseif ( $value === 0 )
        {
            $value = false;
        }
        if ( !is_string( $tag ) || !( is_string( $value ) || is_bool( $value ) ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                'string',
                !is_string( $tag )
                    ? gettype( $tag )
                    : gettype( $value ) ) );
        }
        if ( !array_key_exists( $tag, self::$_lexicon_index_meta_tags_valid ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_PHP_KEY_NOT_FOUND_MESSAGE,
                $tag, __METHOD__ ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_KEY_NOT_FOUND
            );
        }
        $this->_lexicon_index_meta_tags[$tag] = $value;
    }

    /**
     * Sets a user tag on the LexiconIndex
     * @param string $tag
     * @param string|bool $value
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the tag or value parameters are not strings
     */
    private function _lexiconIndexSetUserTag( $tag, $value )
    {
        if ( $value === 1 )
        {
            $value = true;
        } elseif ( $value === 0 )
        {
            $value = false;
        }
        if ( !is_string( $tag ) || !( is_string( $value ) || is_bool( $value ) ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                'string',
                !is_string( $tag )
                    ? gettype( $tag )
                    : gettype( $value ) ) );
        }
        $this->_lexicon_index_user_tags[$tag] = $value;
    }

    /**
     * Deletes an api tag from the LexiconIndex
     * @param string $tag
     * @param string $value
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the tag or value parameters are not strings
     */
    private function _lexiconIndexDeleteApiTag( $tag )
    {
        if ( !is_string( $tag ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                'string',
                gettype( $tag ) ) );
        }
        if ( array_key_exists( $tag, $this->_lexicon_index_api_tags ) )
        {
            unset( $this->_lexicon_index_api_tags[$tag] );
        }
    }

    /**
     * Deletes a meta tag from the LexiconIndex
     * @param string $tag
     * @param string $value
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the tag or value parameters are not strings
     */
    private function _lexiconIndexDeleteMetaTag( $tag )
    {
        if ( !is_string( $tag ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                'string',
                gettype( $tag ) ) );
        }
        if ( array_key_exists( $tag, $this->_lexicon_index_meta_tags ) )
        {
            unset( $this->_lexicon_index_meta_tags[$tag] );
        }
    }

    /**
     * Deletes a user-generated tag from the LexiconIndex
     * @param string $tag
     * @param string $value
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If the tag or value parameters are not strings
     */
    private function _lexiconIndexDeleteUserTag( $tag )
    {
        if ( !is_string( $tag ) )
        {
            throw self::$_lexicon_index_exception_utility->getException( 'invalid-argument',
                'logic-bad-parameters',
                array(
                'string',
                gettype( $tag ) ) );
        }
        if ( array_key_exists( $tag, $this->_lexicon_index_user_tags ) )
        {
            unset( $this->_lexicon_index_user_tags[$tag] );
        }
    }

    /**
     * <Lexicon Index Archive Getter Method>
     * Creates an archive instance to represent save and load
     * functionality for this LexiconIndex.
     * @param string $path A path to a disk location where the archives will be saved.
     * @return \oroboros\codex\interfaces\contract\LexiconArchiveContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If any invalid parameters are passed that prevent loading the LexiconArchive
     */
    private function &_lexiconIndexGetArchiveInstance( $path )
    {
        if ( is_null( $this->_lexicon_index_archive ) )
        {
            try
            {
                $this->_lexicon_index_archive = new \oroboros\codex\LexiconArchive( $this,
                    $this->_lexicon_index_key, $path );
            } catch ( \Exception $e )
            {
                throw self::$_lexicon_index_exception_utility->throwException( 'invalid-argument',
                    $e->getMessage(), 'bad-parameters', $e );
            }
        }
        return $this->_lexicon_index_archive;
    }

    /**
     * Saves the LexiconIndex to disk for quick retrieval later.
     * Returns the fully qualified path to the save file.
     * @param string $path the disk save location
     * @return string
     */
    private function _lexiconIndexArchiveSet( $path )
    {
        $archive = $this->_lexiconIndexGetArchiveInstance( $path );
        return $archive->save();
    }

    /**
     * Attempts to restore an existing LexiconIndex from disk if one exists.
     * @param string $key the identifying key of the LexiconIndex to restore
     * @param string $path the save path to check for archives
     * @param bool $versions If true, returns all versions of the archive in a collection. If false, returns only the most recent. Default false.
     * @return bool|\oroboros\codex\interfaces\contract\LexiconIndexContract|\oroboros\collection\interfaces\contract\Collection
     */
    private static function _lexiconIndexArchiveRestore( $key, $path,
        $versions = false )
    {
        try
        {
            $archive = \oroboros\codex\LexiconArchive::load( $key,
                    $path, $versions );
        }
        //This is the only expected exception. All other exceptions should bubble up normally.
        catch ( \oroboros\core\utilities\exception\codex\LexiconArchiveException $e )
        {
            if ( $e->getCode() === \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_KEY_NOT_FOUND )
            {
                //This just means that the LexiconIndex had not already been saved.
                return false;
            }
            //All other cases are legitimate errors, and need to be rethrown.
            throw $e;
        }
        return $archive;
    }

    /**
     * Checks a set of valid parameters, and returns
     * a designation as to whether they are valid.
     * @param array $data the data to evaluate
     * @param bool $partial (optional) if true, will validate a partial data set, if false, will evaluate for a full data set. Default false.
     * @param bool $return_errors (optional) If true, will return an array of the problematic supplied keys instead of a boolean value on failure, but will still return true on success. Default false.
     * @return bool|array If the third parameter is true, returns an array of errors on error, otherwise returns true on success and false on failure.
     */
    private static function _lexiconIndexValidateData( $data, $partial = false,
        $return_errors = false )
    {
        if ( !is_array( $data ) )
        {
            return false;
        }
        $missing = array();
        $invalid = array();
        $data = array_replace_recursive( self::$_lexicon_index_api_tags_defaults,
            self::$_lexicon_index_meta_tags_defaults, $data );
        $valid = self::_lexiconIndexGetValidationParams();
        //Check required keys
        foreach ( $valid['required'] as
            $req_key =>
            $type )
        {
            if ( !array_key_exists( $req_key, $data ) && $partial )
            {
                continue;
            }
            if ( !array_key_exists( $req_key, $data ) )
            {
                $missing['required'][] = $req_key;
            } elseif ( !( gettype( $data[$req_key] === $type )
                || ($type === 'file' && file_exists( $data[$req_key] ) )
                ) )
            {
                $invalid[] = $req_key;
            }
        }
        //Check optional keys
        foreach ( $valid['optional'] as
            $opt_key =>
            $type )
        {
            if ( !array_key_exists( $opt_key, $data ) )
            {
                continue;
            }
            if ( $data[$opt_key] !== false && !( gettype( $data[$opt_key] ) === $type
                || ( $type === 'file' && file_exists( $data[$opt_key] ) )
                || ( $type === 'directory' && is_dir( $data[$opt_key] ) )
                || ( is_array( $type ) && in_array( $data[$opt_key], $type ) )
                ) )
            {
                $invalid[] = $opt_key;
            }
        }
        //Check properties
        if ( !$partial || ($partial && array_key_exists( 'properties', $data )) )
        {
            foreach ( $data['properties'] as
                $key =>
                $property )
            {
                try
                {
                    $check = new \ReflectionClass( $property );
                    if ( $check->isSubclassOf( '\\oroboros\\codex\\interfaces\\contract\\LexiconEntryContract' ) )
                    {
                        $invalid['properties'][] = $property;
                    }
                } catch ( \ReflectionException $e )
                {
                    $invalid['properties'][] = $property;
                }
            }
        }
        if ( empty( $invalid ) && empty( $missing ) )
        {
            return true;
        }
        if ( $return_errors )
        {
            $result = array();
            if ( !empty( $missing ) )
            {
                $result['missing'] = $missing;
            }
            if ( !empty( $invalid ) )
            {
                $result['invalid'] = $invalid;
            }
            return $result;
        }
        return false;
    }

    /**
     * Returns an array of the parameters used for
     * validation of the LexiconIndex data provided.
     * @return array
     */
    private static function _lexiconIndexGetValidationParams()
    {
        return array(
            'required' => self::$_lexicon_index_api_tags_valid,
            'optional' => self::$_lexicon_index_meta_tags_valid,
        );
    }

    private function _lexiconIndexExport( $options = array() )
    {
        $export = array();
        $root_directory = $this->_lexicon_index_meta_tags['root-directory'];
        foreach ( array_merge( $this->_lexicon_index_api_tags,
            $this->_lexicon_index_meta_tags ) as
            $tag =>
            $value )
        {
            $export['api'][$tag] = $value;
        }
        unset( $export['api']['root-directory'] );
        foreach ( array(
        'map',
        'configuration',
        'bootload',
        'autoload-root' ) as
            $relative_key )
        {
            if ( array_key_exists( $relative_key, $export['api'] )
                && $root_directory
                && $export['api'][$relative_key]
                && strpos( $export['api'][$relative_key], $root_directory ) !== false )
            {
                $export['api'][$relative_key] = str_replace( $root_directory,
                    null, $export['api'][$relative_key] );
            }
        }
        foreach ( $this->_lexicon_index_dependencies as
            $key =>
            $dependency )
        {
            $export['dependencies'][$key] = $dependency->getPackage();
        }
        foreach ( $this->_lexicon_index_subpackages as
            $key =>
            $subpackage )
        {
            $export['subpackages'][$key] = $subpackage->getPackage();
        }
        foreach ( $this->_lexicon_index_entries as
            $key =>
            $entry )
        {
            $export['properties'][$key] = $entry->getReflector()->getReflector()->name;
        }
        $api = array_merge( $this->_lexicon_index_api_tags,
            $this->_lexicon_index_meta_tags );
        if ( !array_key_exists( 'format', $options ) )
        {
            return $export;
        }
        return $this->_lexiconIndexFormatExportData( $export,
                $options['format'], $options );
    }

    private function _lexiconIndexFormatExportData( $data, $format, $options )
    {
        switch ( $format )
        {
            case 'api':
                $this->_lexiconIndexCheckLock( __METHOD__ );
                throw new \oroboros\core\utilities\exception\codex\LexiconIndexException(
                sprintf( 'Error encountered at [%s]. This functionality is not yet implemented.',
                    __METHOD__ ),
                \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE
                );
                break;
            case 'package':
                $parser = new \oroboros\parse\Json( $data );
                return $parser->cast( JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES );
                break;
            case 'subpackage':
                $this->_lexiconIndexCheckLock( __METHOD__ );
                throw new \oroboros\core\utilities\exception\codex\LexiconIndexException(
                sprintf( 'Error encountered at [%s]. This functionality is not yet implemented.',
                    __METHOD__ ),
                \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE
                );
                break;
            case 'composer':
                $this->_lexiconIndexCheckLock( __METHOD__ );
                throw new \oroboros\core\utilities\exception\codex\LexiconIndexException(
                sprintf( 'Error encountered at [%s]. This functionality is not yet implemented.',
                    __METHOD__ ),
                \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE
                );
                break;
            default:
                throw new \oroboros\core\utilities\exception\InvalidArgumentException(
                sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_PHP_KEY_NOT_FOUND_MESSAGE,
                    $format, __METHOD__ ),
                \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_PHP_KEY_NOT_FOUND
                );
                break;
        }
    }

}
