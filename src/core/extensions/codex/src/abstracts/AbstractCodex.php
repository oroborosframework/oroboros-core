<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\abstracts;

/**
 * <Abstract Codex>
 * Provides a set of methods for indexing class types and system assets,
 * or querying that information at runtime.
 * @author Brian Dayhoff <mopsyd@me.com>
 */
abstract class AbstractCodex
    extends \oroboros\core\abstracts\AbstractBase
    implements \oroboros\codex\interfaces\contract\CodexContract
{

    use \oroboros\codex\traits\CodexTrait;

    const OROBOROS_CLASS_TYPE = \oroboros\core\interfaces\enumerated\type\LibraryClassTypes::CLASS_TYPE_LIBRARY_CODEX;
    const OROBOROS_CLASS_SCOPE = \oroboros\core\interfaces\enumerated\scope\LibraryClassScopes::CLASS_SCOPE_LIBRARY_CODEX_ABSTRACT;
    const OROBOROS_CLASS_CONTEXT = \oroboros\core\interfaces\enumerated\context\ClassContext::CLASS_CONTEXT_LIBRARY;
    const OROBOROS_LIBRARY = true;
    const OROBOROS_API = '\\oroboros\\codex\\interfaces\\api\\CodexApi';

}
