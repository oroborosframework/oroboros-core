<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2018, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\interfaces\contract\generators;

/**
 * <Codex Generator Contract Interface>
 * Designates that a core utility is a codex generator, and enforces
 * the set of methods for codex generation of entries, archives, indexes,
 * and configs.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @category codex
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.5
 * @since 0.2.5
 */
interface CodexGeneratorContract
extends \oroboros\core\interfaces\contract\utilities\core\ContextGeneratorContract
{

    /**
     * <Codex Generator Initialization Method>
     * Fires the context generator initialization.
     * @param mixed $params Any parameters that should be passed into initialization.
     * @param array $dependencies Any dependencies to inject into initialization.
     * @param array $flags
     * @return $this
     *
     * @throws \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     * This interface is attached to all exceptions that arise from this trait. If you want
     * to handle everything in one catch block, use this for your catch statement.
     *
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     * If provided parameters are not valid
     *
     * @throws \oroboros\core\utilities\exception\RuntimeException
     * If initialization has already occurred and reinitialization is disabled.
     *
     * @throws \oroboros\core\utilities\exception\LogicException
     * If the class is misconfigured. This is a serious error that
     * requires developer intervention on the part of whoever wrote
     * the override that caused the error, because it means the class
     * is entirely unusable.
     */
    public function initialize( $params = null, $dependencies = null,
        $flags = null );

    /**
     * <Codex Getter Method>
     * Gets the codex index that is associated with the context generator.
     * @param type $element
     * @return type
     */
    public function getContext();

    /**
     * <Codex Defaults Getter Method>
     * Returns the default entries for the codex index associated with
     * the context generator.
     * @return array
     */
    public function getDefaults();

    /**
     * <Codex Generator Parse Method>
     * Parses a given subject that should match the declared subject type
     * for the generator, and returns a codex entry for it.
     * @param mixed $subject
     * @return \oroboros\codex\interfaces\contract\CodexEntryContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given entry is not valid.
     */
    public function parse( $subject );

    /**
     * <Codex Generator Batch Parse Method>
     * Bulk parses a set of subjects that are expressed either
     * in an array or a collection.
     * @param array|\oroboros\collection\interfaces\contract\CollectionContract $subject
     * @return \oroboros\codex\interfaces\contract\CodexEntryContract
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given subject parameter is not an array or collection.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If any of the given entries are not valid.
     */
    public function batch( $subjects );

    /**
     * <Codex Generator Valid Subject Check Method>
     * Preflights a subject candidate and returns whether or not it is valid.
     * @return bool
     */
    public function isValid( $subject );

    /**
     * <Codex Generator Valid Type Getter Method>
     * Returns the valid parameter type that the generator can parse.
     * This will be a valid php variable type, "class", "trait", "interface",
     * "file", "directory", or a class or interface name.
     * @return string
     */
    public function getValidType();
}
