<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\interfaces\contract;

/**
 * <Lexicon Entry Contract Interface>
 * Enforces a set of methods for providing information about a
 * specific class, interface, function, or trait.
 *
 * Lexicon entries represent a set of detailed information about one
 * specific construct. All of the information in the lexicon entry
 * is lazy loaded, and persists for the duration of runtime after
 * its first load to minimize memory overhead. The lexicon entry
 * acts as a pointer to this information, and will perform the logic
 * to retrieve it upon first request, and simply return the same
 * result again if multiple requests occur for the same information.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface LexiconEntryContract
extends \oroboros\core\interfaces\contract\libraries\LibraryContract,
 \Serializable
{


    /**
     * <Lexicon Entry Constructor>
     * Instantiates the lexicon entry.
     *
     * Will accept a string or an object.
     *
     * If the parameter is a string, it MUST be the name
     * of a valid class, interface, trait, or function that
     * has already compiled. It MUST NOT attempt to find
     * uncompiled constructs. The user is responsible for insuring that the
     * construct requested exists before instantiating a new lexicon entry.
     *
     * This method SHOULD be implemented in such a way where a reflector is
     * NOT generated until a query for information requires it. It SHOULD only
     * validate that the property provided is valid, and store the property name
     * and type for later use.
     *
     * @param string|object $property a valid class, interface, trait, or function that has been compiled prior to initialization
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the provided property is not valid
     */
    public function __construct( $property );

    /**
     * <Lexicon Entry Serialize Method>
     * This will instruct PHP to only serialize
     * parameters that are unique to the entry itself.
     * @return array
     */
    public function serialize();

    /**
     * <Lexicon Entry Unserialize Method>
     * Restores a serialized Lexicon Entry
     * to it's proper state.
     */
    public function unserialize( $data );

    /**
     * <Vendor Getter>
     * This will return the vendor level namespace.
     * If none exists, it will return false.
     * @return string|bool
     */
    public function getVendor();

    /**
     * <Reflection Getter>
     * This will return a reflection object of the subject.
     * @return \oroboros\core\utilities\reflection\Reflection
     */
    public function getReflector();

    /**
     * <DocBlock Getter Method>
     * This will return the docblock comment of the construct,
     * by default as a collection. If you would rather have the string value,
     * pass true in as the parameter.
     *
     * @param bool $as_text
     * @return string|\oroboros\collection\interfaces\contract\CollectionContract
     */
    public function getDocBlock( $as_text = false );

    /**
     * <Api Getter>
     * This will return the declared api interface as
     * an enumerated set if one is declared.
     *
     * If one is not declared, it will check if any
     * interface exists on the object that extends
     * \oroboros\core\interfaces\api\DefaultApi
     * and return an enumerated set of that if one does exist.
     * @return \oroboros\enum\InterfaceEnumerator
     */
    public function getApi();

    /**
     * <Lexicon Entry Method Getter>
     * Lazy loads the entry methods if they are not
     * already loaded and returns a collection of them.
     *
     * The methods returned by this will be packed within the collection
     * as an array containing an evaluation of the input and output
     * parameters, if they can be determined.
     *
     * @return \oroboros\collection\Collection|null
     */
    public function getMethods();

    /**
     * <Lexicon Entry Constant Getter>
     * Lazy loads the entry methods if they are not
     * already loaded and returns a collection of them.
     *
     * The constants returned will be a simple key value store
     * contained within a collection.
     *
     * @return \oroboros\collection\Collection|null
     */
    public function getConstants();

    /**
     * <Lexicon Entry Property Getter>
     * Lazy loads the entry properties if they are not
     * already loaded and returns a collection of them.
     *
     * The properties returned will be an array wrapped in a collection,
     * designating the name as the key, and an array of details,
     * including the doc comment and type, if either are present
     * and can be determined. Keys will still exist but will be false
     * if they cannot be determined.
     *
     * @return \oroboros\collection\Collection|null
     */
    public function getProperties();

    /**
     * <Lexicon Entry Interfaces Getter>
     * Lazy loads the entry interfaces if they are not
     * already loaded and returns a collection of them.
     *
     * The interfaces returned will be lexicon
     * entries wrapped in a collection.
     *
     * @return \oroboros\collection\Collection|null
     */
    public function getInterfaces();

    /**
     * <Lexicon Entry Interfaces Getter>
     * Lazy loads the entry traits if they are not
     * already loaded and returns a collection of them.
     *
     * The traits returned will be lexicon
     * entries wrapped in a collection.
     *
     * @param bool $include_indirect If true, traits used by indirect inheritance will also be collected. Default false.
     * @return \oroboros\collection\Collection|null
     */
    public function getTraits( $include_indirect = false );

    /**
     * <Lexicon Entry Interfaces Getter>
     * Lazy loads the entry parent if they are not
     * already loaded and returns a collection of them.
     *
     * The return entry will be a lexicon entry. If there is no parent,
     * the lexicon entry will always be for stdClass (the PHP base class)
     *
     * @return \oroboros\codex\LexiconEntry|null
     */
    public function getParent();

    /**
     * <Lexicon Entry Package Getter>
     * Lazy loads the entry package if they are not
     * already loaded and returns a collection of them.
     *
     * Packages will be checked first by ApiInterface, then by oroboros.json
     * for the root namespace using psr4 standards, then for a composer.json
     * at the root namespace using psr4 standards, then via composer itself
     * it is available. If none of these options turn up a parseable package,
     * then the function will return false. This approach follows the specific
     * to the general, only returning false if all available options to
     * determine a package have been exhausted.
     *
     * The return entry will be a lexicon index. If there is no package,
     * the return value will be false
     *
     * @todo allow overloading of package detection scripts
     * @return bool|\oroboros\codex\LexiconIndex
     */
    public function getPackage();

    /**
     * <Lexicon File Location Getter>
     * Returns an SplFileInfo object wrapping the file name.
     * To get the fully qualified filepath, just cast this
     * to a string.
     * @return \SplFileInfo
     */
    public function getFile();

    /**
     * <Lexicon Directory Location Getter>
     * Returns the string name of the directory containing the lexicon entry,
     * or a DirectoryIterator object representing the directory.
     * @param $iterator if true, will return a DirectoryIterator instead of the name, otherwise returns the string name of the directory. Default false.
     * @return \DirectoryIterator|string
     */
    public function getDirectory( $iterator = false );

    /**
     * <Lexicon Namespace Check Method>
     * Returns a boolean determination as to whether
     * the entry exists in a target namespace.
     * @return bool
     */
    public function inNamespace( $namespace );

    /**
     * <Lexicon Package Check Method>
     * Returns a boolean determination as to whether
     * the entry exists in a given package.
     * @return bool
     */
    public function inPackage( $package );

    /**
     * <Lexicon Instance Check Method>
     * Checks if the given lexicon entry is an instance of the given instance,
     * or if it directly uses it if the given instance is a trait.
     * All invalid parameters will return false, as will traits that
     * are indirectly inherited.
     * @param string|object $instance
     * @return bool
     */
    public function isInstanceOf( $instance );

    /**
     * <Lexicon Compile Check Method>
     * Checks if the lexicon entry is currently compiled or not.
     * This method will not fire autoloading or attempt to compile
     * it if it is not already compiled.
     * @return bool
     */
    public function isCompiled();

    /**
     * <Lexicon Psr0 Standard Check Method>
     * Returns a boolean determination as to whether
     * the entry is a Psr0 autoload compliant construct.
     * @return bool
     */
    public function isPsr0();

    /**
     * <Lexicon Psr4 Standard Check Method>
     * Returns a boolean determination as to whether
     * the entry is a Psr4 autoload compliant construct.
     * @return bool
     */
    public function isPsr4();

    /**
     * <Lexicon Closure Check Method>
     * Returns a boolean determination as to whether
     * the entry is a Closure.
     * @return bool
     */
    public function isClosure();

    /**
     * <Lexicon Entry Callable Check Method>
     * This will check if the lexicon entry is directly callable like a function.
     * This will return true if the entry is a function, a closure,
     * or an invokable class, and will return false if it is a
     * class method, non-invokable class, interface,
     * or trait. This method can be used to designate if the
     * given property can be directly called like a function
     * without any further consideration.
     * @return bool
     */
    public function isCallable();

    /**
     * <Lexicon Interface Check Method>
     * Returns a boolean determination as to whether
     * the entry is an interface.
     * @return bool
     */
    public function isInterface();

    /**
     * <Lexicon Trait Check Method>
     * Returns a boolean determination as to whether
     * the entry is a trait.
     * @return bool
     */
    public function isTrait();

    /**
     * <Lexicon Interface Usage Check Method>
     * Returns a boolean determination as to whether the entry has
     * implemented a given interface either directly or indirectly.
     * Passing true as the second parameter will cause the method to
     * only return true if the interface is directly implemented by the
     * exact instance, and will return false if indirectly implemented
     * via inheritance. Otherwise the method will return true if the
     * interface exists anywhere in the chain of inheritance.
     *
     * This will always return false if an invalid parameter is given.
     *
     * @param string $interface
     * @return bool
     */
    public function hasInterface( $interface, $direct = false );

    /**
     * <Lexicon Trait Usage Check Method>
     * Returns a boolean determination as to whether the entry has
     * used a given trait, either directly or indirectly. Passing true as
     * the second parameter will cause the method to only return true if
     * the trait is directly used by this exact instance, and will return
     * false on inherited usage. Otherwise it will return true if the trait
     * exists anywhere along the chain of inheritance.
     *
     * This will always return false if an invalid parameter is given.
     *
     * @param string $trait A valid trait name
     * @param bool $direct (optional) If provided as true, will return true only if the trait is used directly by this specific instance, and will return false on inherited usage.
     * @return bool
     */
    public function hasTrait( $trait, $direct = false );

    /**
     * <Lexicon Parent Usage Check Method>
     * Returns a boolean determination as to whether the entry has
     * extended a given trait, interface, or class, either directly
     * or indirectly. This method will only return true if the type of
     * the entry is the same as the type of the given instance
     * (eg: trait uses another trait, interface extends another interface,
     * class extends another class). It will return [false] if the entry
     * represents a class and an interface or trait are given, if it is a
     * trait and a non-trait is given, or if it is an interface and a
     * non-interface is given.
     *
     * If the second parameter is passed as true, the method will only return
     * true if the entry is a direct descendent AND is the same type.
     * Otherwise it will check if the entry extends the construct
     * indirectly, but only along inheritance of the same type.
     *
     * This will always return false if an invalid parameter is given.
     *
     * @param object|string $parent
     * @param bool $direct
     * @return bool
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid parameter is passed
     */
    public function hasParent( $parent, $direct = false );

}
