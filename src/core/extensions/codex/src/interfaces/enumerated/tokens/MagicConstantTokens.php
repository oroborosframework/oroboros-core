<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\interfaces\enumerated\tokens;

/**
 * <Magic Constant Token Enumerated Api Interface>
 * This enumerated interface provides more human readable descriptions
 * for PHP bitwise tokens, to ease the readability of error reporting
 * and logging of error messages.
 *
 * Magic constant tokens are pre-declared constants that provide contextual
 * information based on where they are used.
 *
 * This is used by the Lexicon Tokenizer also for validating PHP tokens
 * when auto-parsing packages from the command line api.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.5
 * @since 0.2.5
 */
interface MagicConstantTokens
extends \oroboros\codex\interfaces\enumerated\CodexBase
{
    //Magic Constants
    const TOKEN_MAGIC_CONSTANT_DIRECTORY = T_DIR;
    const TOKEN_MAGIC_CONSTANT_CLASS = T_CLASS_C;
    const TOKEN_MAGIC_CONSTANT_FILE = T_FILE;
    const TOKEN_MAGIC_CONSTANT_FUNCTION = T_FUNC_C;
    const TOKEN_MAGIC_CONSTANT_LINE_NUMBER = T_LINE;
    const TOKEN_MAGIC_CONSTANT_METHOD = T_METHOD_C;
    const TOKEN_MAGIC_CONSTANT_TRAIT = T_TRAIT_C;
    const TOKEN_MAGIC_CONSTANT_NAMESPACE = T_NS_C;
    const TOKEN_MAGIC_CONSTANT_DIRECTORY_NAME = 'T_DIR';
    const TOKEN_MAGIC_CONSTANT_CLASS_NAME = 'T_CLASS_C';
    const TOKEN_MAGIC_CONSTANT_FILE_NAME = 'T_FILE';
    const TOKEN_MAGIC_CONSTANT_FUNCTION_NAME = 'T_FUNC_C';
    const TOKEN_MAGIC_CONSTANT_LINE_NUMBER_NAME = 'T_LINE';
    const TOKEN_MAGIC_CONSTANT_METHOD_NAME = 'T_METHOD_C';
    const TOKEN_MAGIC_CONSTANT_TRAIT_NAME = 'T_TRAIT_C';
    const TOKEN_MAGIC_CONSTANT_NAMESPACE_NAME = 'T_NS_C';

}
