<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\codex\interfaces\enumerated\tokens;

/**
 * <Assignment Token Enumerated Api Interface>
 * This enumerated interface provides more human readable descriptions
 * for PHP assignment tokens, to ease the readability of error reporting
 * and logging of error messages.
 *
 * Assignment tokens are language constructs that modify a variable.
 *
 * This is used by the Lexicon Tokenizer also for validating PHP tokens
 * when auto-parsing packages from the command line api.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/codex
 * @subpackage codex
 * @version 0.2.5
 * @since 0.2.5
 */
interface AssignmentTokens
extends \oroboros\codex\interfaces\enumerated\CodexBase
{

    //Assignment Operators
    const TOKEN_OPERATOR_ASSIGNMENT_AND_EQUAL = T_AND_EQUAL;
    const TOKEN_OPERATOR_ASSIGNMENT_CONCAT_EQUAL = T_CONCAT_EQUAL;
    const TOKEN_OPERATOR_ASSIGNMENT_DIVISOR_EQUAL = T_DIV_EQUAL;
    const TOKEN_OPERATOR_ASSIGNMENT_XOR_EQUAL = T_XOR_EQUAL;
    const TOKEN_OPERATOR_ASSIGNMENT_OR_EQUAL = T_OR_EQUAL;
    const TOKEN_OPERATOR_ASSIGNMENT_PLUS_EQUAL = T_PLUS_EQUAL;
    const TOKEN_OPERATOR_ASSIGNMENT_ARITHMETIC_POWEROF_EQUAL = T_POW_EQUAL; //Not available below PHP 5.6
    const TOKEN_OPERATOR_ASSIGNMENT_ARITHMETIC_POWEROF = T_POW; //Not available below PHP 5.6
    const TOKEN_OPERATOR_ASSIGNMENT_BITWISE_SHIFT_LEFT_EQUAL = T_SL_EQUAL;
    const TOKEN_OPERATOR_ASSIGNMENT_BITWISE_SHIFT_RIGHT_EQUAL = T_SR_EQUAL;
    const TOKEN_OPERATOR_ASSIGNMENT_DECREMENT = T_DEC;
    const TOKEN_OPERATOR_ASSIGNMENT_INCREMENT = T_INC;
    const TOKEN_OPERATOR_ASSIGNMENT_MINUS_EQUAL = T_MINUS_EQUAL;
    const TOKEN_OPERATOR_ASSIGNMENT_MODULUS_EQUAL = T_MOD_EQUAL;
    const TOKEN_OPERATOR_ASSIGNMENT_MULTIPLY_EQUAL = T_MUL_EQUAL;
    const TOKEN_OPERATOR_ASSIGNMENT_AND_EQUAL_NAME = 'T_AND_EQUAL';
    const TOKEN_OPERATOR_ASSIGNMENT_CONCAT_EQUAL_NAME = 'T_CONCAT_EQUAL';
    const TOKEN_OPERATOR_ASSIGNMENT_DIVISOR_EQUAL_NAME = 'T_DIV_EQUAL';
    const TOKEN_OPERATOR_ASSIGNMENT_XOR_EQUAL_NAME = 'T_XOR_EQUAL';
    const TOKEN_OPERATOR_ASSIGNMENT_OR_EQUAL_NAME = 'T_OR_EQUAL';
    const TOKEN_OPERATOR_ASSIGNMENT_PLUS_EQUAL_NAME = 'T_PLUS_EQUAL';
    const TOKEN_OPERATOR_ASSIGNMENT_ARITHMETIC_POWEROF_EQUAL_NAME = 'T_POW_EQUAL'; //Not available below PHP 5.6
    const TOKEN_OPERATOR_ASSIGNMENT_ARITHMETIC_POWEROF_NAME = 'T_POW'; //Not available below PHP 5.6
    const TOKEN_OPERATOR_ASSIGNMENT_BITWISE_SHIFT_LEFT_EQUAL_NAME = 'T_SL_EQUAL';
    const TOKEN_OPERATOR_ASSIGNMENT_BITWISE_SHIFT_RIGHT_EQUAL_NAME = 'T_SR_EQUAL';
    const TOKEN_OPERATOR_ASSIGNMENT_DECREMENT_NAME = 'T_DEC';
    const TOKEN_OPERATOR_ASSIGNMENT_INCREMENT_NAME = 'T_INC';
    const TOKEN_OPERATOR_ASSIGNMENT_MINUS_EQUAL_NAME = 'T_MINUS_EQUAL';
    const TOKEN_OPERATOR_ASSIGNMENT_MODULUS_EQUAL_NAME = 'T_MOD_EQUAL';
    const TOKEN_OPERATOR_ASSIGNMENT_MULTIPLY_EQUAL_NAME = 'T_MUL_EQUAL';

}
