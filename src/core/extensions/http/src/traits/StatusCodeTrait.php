<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\http\traits;

/**
 * <Header Trait>
 * Provides a simple means for representing a status code as an object.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage utilities
 * @version 0.2.4
 * @since 0.2.4-alpha
 */
trait StatusCodeTrait
{

    /**
     * Represents the status code value.
     * @var int
     */
    private $_status_code = 200;

    /**
     * Represents the status code message.
     * @var string
     */
    private $_status_code_title;

    /**
     * Represents the possible valid status codes.
     * @var \oroboros\core\enum\http\StatusCodes
     */
    private static $_status_codes_valid;

    /**
     * Represents the validation enumerator used to
     * check whether a status code is valid or not.
     * @var string
     */
    private static $_status_codes_enumerator = '\\oroboros\\enum\\http\\StatusCodes';

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies ...
     *
     * @execution Default Execution Plan (minimal)
     *
     * @execution Default Execution Plan (commented)
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Status Code Constructor>
     * Instantiates an object representing a valid http status code, as defined in
     * \oroboros\http\interfaces\enumerated\StatusCodes
     * @param int $code (optional) Sets the status code that the object represents.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid status code is passed.
     */
    public function __construct( $code = 200 )
    {
        $this->_initializeStatusCode( $code );
    }

    /**
     * Returns the status code message when the object is cast to a string.
     * @return string
     */
    public function __toString()
    {
        return $this->_status_code_title;
    }

    /**
     * Returns the status code
     * @return int
     */
    public function getCode()
    {
        return $this->_status_code;
    }

    /**
     * Returns the current status code title.
     * @return string
     */
    public function getMessage()
    {
        return $this->_status_code_title;
    }

    /**
     * Sets a custom status code title.
     * If this method is not called, the default value will be used.
     * @param mixed $message Must be either a string, scalar, or any value that can be cast to a non-empty string (eg: object that has method __toString)
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if a value is passed that cannot resolve to a string that is not empty
     */
    public function setMessage( $message )
    {
        $this->_setCustomStatusCodeMessage( $message );
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Status Code Initialization>
     * Initializes the status code object.
     * @param int $code
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the provided code is not valid.
     */
    protected function _initializeStatusCode( $code, $message = null )
    {
        $this->_getValidStatusCodes();
        $this->_checkStatusCode( $code );
        $enum = new self::$_status_codes_enumerator();
        $this->_status_code = (int) $code;
        if ( is_null( $message ) )
        {
            $const = get_class( $enum ) . '::STATUS_' . $this->_status_code . '_TITLE';
            $message = constant( $const );
        }
        $this->_setCustomStatusCodeMessage( $message );
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Initializes the list of valid status codes for the current class,
     * if it has not already occurred.
     * @return void
     */
    private function _getValidStatusCodes()
    {
        if ( is_null( self::$_status_codes_valid ) )
        {
            $enum = new self::$_status_codes_enumerator();
            self::$_status_codes_valid = $enum->valid();
        }
    }

    /**
     * Checks if a status code is valid.
     * Will attempt to cast to int if it is scalar and not an integer.
     * @param int $code
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the status code is not valid.
     * @return void
     */
    private function _checkStatusCode( $code )
    {
        if ( !is_scalar( $code ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'int', gettype( $code ) ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
        $check = (int) $code;
        if ( !in_array( $code, self::$_status_codes_valid ) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( 'Provided value [%s] is not a valid HTTP status code.',
                $code ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS
            );
        }
    }

    /**
     * Sets a custom status code message. Any scalar value may be passed or
     * any object implementing __toString. The result will be cast to a
     * string if it is not already.
     * @param scalar|object $message Must be a scalar value or an object with __toString
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     * @return void
     */
    private function _setCustomStatusCodeMessage( $message )
    {
        $error = false;
        $error_message = null;
        $code = \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_BAD_PARAMETERS;
        if ( !is_scalar( $message ) && !(is_object( $message ) && method_exists( $message,
                '__toString' )) )
        {
            $error = true;
            $error_message = sprintf(
                \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, 'scalar value, or stringable object',
                gettype( $message ) );
        } elseif ( (string) $message === '' )
        {
            $error = true;
            $error_message = sprintf(
                \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_MISSING_PARAMETERS_MESSAGE,
                __METHOD__,
                '$message cannot evaluate to null or an empty string'
            );
            $code = \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_LOGIC_MISSING_PARAMETERS;
        }
        if ( $error )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException( $error_message,
            $code );
        }
        $this->_status_code_title = (string) $message;
    }

}
