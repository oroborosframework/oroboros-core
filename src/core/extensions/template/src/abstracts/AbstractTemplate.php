<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\template\abstracts;

/**
 * <Oroboros Abstract Template>
 * @todo Finish porting this to the new logic. This will remain commented out
 * until integration effort occurs to prevent skewing code coverage.
 * It is not currently used.
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
abstract class AbstractTemplate
    extends \oroboros\core\abstracts\libraries\AbstractLibrary
{

    use \oroboros\template\traits\TemplateTrait;

    const OROBOROS_CLASS_TYPE = \oroboros\core\interfaces\enumerated\type\LibraryClassTypes::CLASS_TYPE_LIBRARY_TEMPLATE;
    const OROBOROS_CLASS_SCOPE = \oroboros\core\interfaces\enumerated\scope\LibraryClassScopes::CLASS_SCOPE_LIBRARY_TEMPLATE_ABSTRACT;
    const OROBOROS_API = '\\oroboros\\template\\interfaces\\api\\TemplateApi';

//    private $_allowed_loader_types = array(
//        "template",
//        "module"
//    );
//    private $_details = array();
//    private $_title = 'Untitled Page';
//    private $_heading = 'Untitled Page';
//    private $_author = 'Unknown Author';
//    private $_description = 'No Description Provided';
//    private $_content = array();
//    private $_favicon = array();
//    private $_meta = array();
//    private $_scripts = array();
//    private $_css = array();
//    private $_fonts = array();
//    private $_javascript_metadata = array();
//    private $_resources = array();
//    private $_document = NULL;
//    private $_parts = array();
//    private $_pages = array();
//    private $_forms = array();
//    private $_path = __DIR__;
//    private $_data = array();
//    private $_lang = 'en';
//    private $_pagename = 'unknown';
//
//    public function __construct( $params = null, $flags = null ) {
//        $this->_setAllowedLoaderTypes($this->_allowed_loader_types);
//        $this->queue($params);
//        parent::__construct($params, $flags);
//    }
//
//    public function initialize( $params = null, $flags = null ) {
//        parent::initialize($params, $flags);
//        $this->queue($params);
//        $this->_initializeLoader();
//    }
//
//    public function render( $params = null, $flags = null ) {
//        print $this->_document($params, $flags);
//    }
//
//    public function reset( $params = null, $flags = null ) {
//        $this->resetLang();
//        $this->resetAuthor();
//        $this->resetDescription();
//        $this->resetMeta();
//        $this->resetCss();
//        $this->resetScripts();
//        $this->resetFavicon();
//        $this->resetFonts();
//        $this->resetPagename();
//        $this->resetTitle();
//        $this->resetHeading();
//        $this->resetContent();
//        $this->resetMetadata();
//        $this->queue($params, $flags);
//    }
//
//    public function resource($type) {
//        $result = FALSE;
//        $path = $this->_path . 'resources' . DIRECTORY_SEPARATOR . $type . DIRECTORY_SEPARATOR;
//        if (is_readable($path)) {
//            $result = \oroboros\core\localize_url($path);
//        }
//        return $result;
//    }
//
//    public function form($form, array $params = array(), array $flags = array()) {
//        return $this->_form($form, $params, $flags);
//    }
//
//    public function part($part, array $params = array(), array $flags = array()) {
//        return $this->_serve($part, $params, $flags);
//    }
//
//    public function page($name, array $params = array(), array $flags = array()) {
//        return $this->_page($name, $params, $flags);
//    }
//
//    public function queue( $params = null, $flags = null ) {
//        if (array_key_exists('author', $params)) {
//            $this->setAuthor($params['author']);
//        }
//        if (array_key_exists('description', $params)) {
//            $this->setDescription($description);
//        }
//        if (array_key_exists('title', $params)) {
//            $this->setTitle($params['title']);
//        }
//        if (array_key_exists('language', $params)) {
//            $this->setLang($params['language']);
//        }
//        if (array_key_exists('meta', $params)) {
//            foreach ($params['meta'] as $name => $value) {
//                $this->setMeta($name, $meta);
//            }
//        }
//        if (array_key_exists('pagename', $params)) {
//            $this->setPagename($params['pagename']);
//        }
//        if (array_key_exists('favicon', $params)) {
//            foreach ($params['favicon'] as $name => $favicon) {
//                $this->setFavicon($name, $favicon);
//            }
//        }
//        if (array_key_exists('fonts', $params)) {
//            foreach ($params['fonts'] as $name => $font) {
//                $this->setFont($name, $font);
//            }
//        }
//        if (array_key_exists('data', $params)) {
//            foreach ($params['data'] as $name => $data) {
//                $this->setData($name, $data);
//            }
//        }
//        if (array_key_exists('scripts', $params)) {
//            foreach ($params['scripts'] as $name => $script) {
//                $this->setScript($name, $script);
//            }
//        }
//        if (array_key_exists('css', $params)) {
//            foreach ($params['css'] as $name => $stylesheet) {
//                $this->setCss($name, $stylesheet);
//            }
//        }
//        if (array_key_exists('heading', $params)) {
//            $this->setHeading($params['heading']);
//        }
//        if (array_key_exists('content', $params)) {
//            foreach ($params['content'] as $name => $content) {
//                $this->setContent($name, $content);
//            }
//        }
//        if (array_key_exists('metadata', $params)) {
//            foreach ($params['metadata'] as $name => $metadata) {
//                $this->setMetadata($name, $metadata);
//            }
//        }
//    }
//
//    public function setAuthor($author) {
//        $this->_author = $author;
//    }
//
//    public function resetAuthor() {
//        $this->_author = 'Unknown Author';
//    }
//
//    public function author() {
//        return $this->_author;
//    }
//
//    public function setDescription($description) {
//        $this->_description = $description;
//    }
//
//    public function resetDescription() {
//        $this->_description = 'No Description Provided';
//    }
//
//    public function description() {
//        return $this->_description;
//    }
//
//    public function setTitle($title) {
//        $this->_title = $title;
//    }
//
//    public function resetTitle() {
//        $this->_title = 'Untitled Page';
//    }
//
//    public function title() {
//        return $this->_title;
//    }
//
//    public function setLang($lang) {
//        $this->_lang = $lang;
//    }
//
//    public function resetLang() {
//        $this->_lang = 'en';
//    }
//
//    public function lang() {
//        return $this->_lang;
//    }
//
//    public function setMeta($name, $meta) {
//        $this->_meta[$name] = $meta;
//    }
//
//    public function resetMeta() {
//        $this->_meta = array();
//    }
//
//    public function meta() {
//        $markup = PHP_EOL;
//        foreach ($this->_meta as $key => $value) {
//            $markup .= $this->_tag('meta', array(
//                $key => $value
//            ));
//        }
//        return $markup;
//    }
//
//    public function setPagename($id) {
//        $this->_pagename = $id;
//    }
//
//    public function resetPagename() {
//        $this->_pagename = 'unknown';
//    }
//
//    public function pagename() {
//        return $this->_pagename;
//    }
//
//    public function setFavicon($name, $favicon) {
//        $this->_favicon[$name] = $favicon;
//    }
//
//    public function resetFavicon() {
//        $this->_favicon = array();
//    }
//
//    public function favicon() {
//
//    }
//
//    public function setFont($name, $font) {
//        $this->_fonts[$name] = $font;
//    }
//
//    public function resetFonts() {
//        $this->_fonts = array();
//    }
//
//    public function fonts() {
//
//    }
//
//    public function setScript($name, $script) {
//        $this->_scripts[$name] = $script;
//    }
//
//    public function resetScripts() {
//        $this->_scripts = array();
//    }
//
//    public function scripts() {
//        $markup = PHP_EOL;
//        foreach ($this->_scripts as $priority => $script) {
//            foreach ($script as $key => $details) {
//                $tmp = array(
//                    'type' => 'application/javascript',
//                    'src' => $details->src
//                );
//                if (isset($details->crossorigin)) {
//                    $tmp['crossorigin'] = $details->crossorigin;
//                }
//                if (isset($details->integrity)) {
//                    $tmp['integrity'] = $details->integrity;
//                }
//                $markup .= $this->_tag('script', $tmp);
//            }
//        }
//        return $markup;
//    }
//
//    public function setCss($name, $stylesheet) {
//        $this->_css[$name] = $stylesheet;
//    }
//
//    public function resetCss() {
//        $this->_css = array();
//    }
//
//    public function css() {
//        $markup = PHP_EOL;
//        foreach ($this->_css as $priority => $css) {
//            foreach ($css as $key => $details) {
//                $tmp = array(
//                    'rel' => 'stylesheet',
//                    'type' => 'text/css',
//                    'href' => $details->href
//                );
//                if (isset($details->crossorigin)) {
//                    $tmp['crossorigin'] = $details->crossorigin;
//                }
//                if (isset($details->integrity)) {
//                    $tmp['integrity'] = $details->integrity;
//                }
//                $markup .= $this->_tag('link', $tmp, array(self::FLAG_SHORT_TAG));
//            }
//        }
//        return $markup;
//    }
//
//    public function setHeading($heading) {
//        $this->_heading = $heading;
//    }
//
//    public function resetHeading() {
//        $this->_heading = 'Untitled Page';
//    }
//
//    public function heading() {
//        return $this->_heading;
//    }
//
//    public function setMetadata($name, $metadata) {
//        $this->_javascript_metadata[$name] = $metadata;
//    }
//
//    public function resetMetadata() {
//        $this->_javascript_metadata = array();
//    }
//
//    public function metadata() {
//        $result = json_encode($this->_javascript_metadata, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
//        return $result;
//    }
//
//    public function setContent($name, $content) {
//        $this->_content[$name] = $content;
//    }
//
//    public function resetContent() {
//        $this->_content = array();
//    }
//
//    public function content($key) {
//        if (!isset($this->_content[$key])) {
//            return FALSE;
//        }
//        return $this->_content[$key];
//    }
//
//    protected function _registerMap($map) {
//        try {
//            $schema = json_decode(file_get_contents($map));
//            $this->_details = $schema->meta;
//            $this->_meta = get_object_vars($schema->profile->meta);
//            $this->_resources = $schema->resources;
//            $this->_document = $schema->profile->document;
//            $this->_parts = $schema->profile->parts;
//            $this->_pages = $schema->profile->pages;
//            $this->_forms = $schema->profile->forms;
//            $reflector = new \ReflectionClass(get_class($this));
//            $fn = $reflector->getFileName();
//            $this->_path = dirname($fn) . DIRECTORY_SEPARATOR;
//        } catch (\Exception $e) {
//            throw new \oroboros\core\utilities\exception\Exception("Invalid template schema map: [" . $map . "]", self::ERROR_VIEW);
//        }
//    }
//
//    protected function _registerData($data) {
//        $this->_data = $data;
//    }
//
//    protected function _data($key) {
//        if (isset($this->_data[$key])) {
//            return $this->_data[$key];
//        }
//        return FALSE;
//    }
//
//    protected function _document() {
//        ob_start();
//        $errors = $this->_checkErrors();
//        $post_data = $this->_checkPostData();
//        require $this->_path . DIRECTORY_SEPARATOR
//                . $this->_document;
//        return ob_get_clean();
//    }
//
//    protected function _serve($part, array $data = array(), array $flags = array()) {
//        if (isset($this->_parts->{$part})) {
//            ob_start();
//            $errors = $this->_checkErrors();
//            $post_data = $this->_checkPostData();
//            include $this->_path . DIRECTORY_SEPARATOR
//                    . 'template' . DIRECTORY_SEPARATOR
//                    . 'parts' . DIRECTORY_SEPARATOR
//                    . $this->_parts->{$part};
//            return ob_get_clean();
//        }
//        return FALSE;
//    }
//
//    protected function _page($name, array $data = array(), array $flags = array()) {
//        if (isset($this->_pages->{$name})) {
//            ob_start();
//            $errors = $this->_checkErrors();
//            $post_data = $this->_checkPostData();
//            include $this->_path . DIRECTORY_SEPARATOR
//                    . 'template' . DIRECTORY_SEPARATOR
//                    . 'pages' . DIRECTORY_SEPARATOR
//                    . $this->_pages->{$name};
//            return ob_get_clean();
//        }
//        return FALSE;
//    }
//
//    protected function _form($name, array $data = array(), array $flags = array()) {
//        if (isset($this->_forms->{$name})) {
//            ob_start();
//            $errors = $this->_checkErrors();
//            $post_data = $this->_checkPostData();
//            include $this->_path . DIRECTORY_SEPARATOR
//                    . 'template' . DIRECTORY_SEPARATOR
//                    . 'forms' . DIRECTORY_SEPARATOR
//                    . $this->_forms->{$name};
//            return ob_get_clean();
//        }
//        return FALSE;
//    }
//
//    protected function _parts() {
//        return array_keys(get_object_vars($this->_parts));
//    }
//
//    protected function _pages() {
//        return array_keys(get_object_vars($this->_pages));
//    }
//
//    private function _checkErrors() {
//        return (($this->_checkParam('errors'))
//                    ? $this->_getParam('errors')
//                    : FALSE);
//    }
//
//    private function _checkPostData() {
//        return (($this->_checkParam('post_data') && !empty($this->_getParam('post_data')))
//                    ? $this->_getParam('post_data')
//                    : FALSE);
//    }
}
