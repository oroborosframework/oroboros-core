<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\environment\interfaces\enumerated;

/**
 * <Cgi Environment Enumerated Api Interface>
 *
 * This enumerated api interface defines the cgi runtime environment.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage environment
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface CgiEnvironment
extends EnvironmentBase
{
    /**
     * Defines if the current request is cli based.
     * This value cannot be overridden.
     * Doing so will cause the system to reference HTTP resources
     * that are not there, and cause errors during cli sessions.
     */
    const IS_CLI = OROBOROS_IS_CLI;

    /**
     * Defines if shell is enabled or not.
     * This value can be overridden.
     * define('OROBOROS_SHELL_ENABLED', false);
     *
     * Unless overridden, this value will be true if shell_exec is available,
     * and false otherwise.
     */
    const SHELL_ENABLED = OROBOROS_SHELL_ENABLED;

    /**
     * Defines the username of the server for determining file permissions, etc.
     * This value can be overridden.
     * define('OROBOROS_SHELL_USER', 'username');
     *
     * Unless overridden, this value will be false if OROBOROS_SHELL_ENABLED is false,
     * and otherwise will call the shell command `whoami` to determine it's value
     */
    const SHELL_USER = OROBOROS_SHELL_USER;
}
