<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\environment\interfaces\enumerated;

/**
 * <Core Environment Enumerated Api Interface>
 *
 * This enumerated api interface defines information about
 * the oroboros core release package being utilized, so that
 * determinations about compatibility of extensions, modules,
 * components, and overrides can be determined, and what
 * features the platform offers that are currently available.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage environment
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface CoreEnvironment
extends EnvironmentBase
{
    /**
     * Defines the current release version of Oroboros core.
     */
    const OROBOROS_VERSION = OROBOROS_VERSION;

    /**
     * Defines the universal vendor namespace of oroboros.
     */
    const OROBOROS_VENDOR_NAMESPACE = 'oroboros';

    /**
     * Defines the root directory for Oroboros core.
     */
    const ROOT_DIRECTORY = OROBOROS_ROOT_DIRECTORY;

    /**
     * Defines the root directory for Oroboros core.
     */
    const ROOT_NAMESPACE = 'oroboros\\core';

    /**
     * Defines the temp directory for Oroboros core.
     * This may be overridden by using the
     * following prior to bootstrapping Oroboros core:
     * define('OROBOROS_TEMP_DIRECTORY', '/path/to/tmp/');
     */
    const TEMP_DIRECTORY = OROBOROS_TEMP_DIRECTORY;

    /**
     * Defines the current error log.
     * This value can be overridden.
     * define('OROBOROS_ERROR_LOG', '/path/to/logfile.log'); //or false
     *
     * Unless overridden, this value will check for the server error log value,
     * and use that if it is writeable. If that is not writeable, it will be false.
     *
     * A false value disables logging.
     * If you would like to log elsewhere,
     * override the constant with the file to log into.
     */
    const ERROR_LOG = OROBOROS_ERROR_LOG;

    /**
     * Defines the templates directory.
     * This value cannot be overridden.
     * @todo make this overrideable
     */
    const TEMPLATE_DIRECTORY = OROBOROS_TEMPLATES;

    /**
     * Defines the themes directory.
     * This value cannot be overridden.
     * @todo make this overrideable
     */
    const THEME_DIRECTORY = OROBOROS_THEMES;

    /**
     * Defines the tests directory.
     * This value cannot be overridden.
     * @todo make this overrideable
     */
    const TEST_DIRECTORY = OROBOROS_TESTS;

    /**
     * Defines the temp directory.
     * This value cannot be overridden.
     * @todo make this overrideable
     */
    const LOCAL_TMP_DIRECTORY = OROBOROS_TMP;

    /**
     * Defines the vendor directory.
     * This value cannot be overridden.
     * @todo make this overrideable
     */
    const VENDOR_DIRECTORY = OROBOROS_VENDOR;

    /**
     * Defines the modules directory.
     * This value cannot be overridden.
     * @todo make this overrideable
     */
    const MODULE_DIRECTORY = OROBOROS_MODULES;

    /**
     * Defines the cache directory.
     * This value cannot be overridden.
     * @todo make this overrideable
     */
    const CACHE_DIRECTORY = OROBOROS_CACHE;

    /**
     * Defines the config directory.
     * This value cannot be overridden.
     * @todo make this overrideable
     */
    const CONFIG_DIRECTORY = OROBOROS_CONFIG;

    /**
     * Defines the dependencies directory.
     * This value cannot be overridden.
     * @todo make this overrideable
     */
    const DEPENDENCY_DIRECTORY = OROBOROS_DEPENDENCIES;

    /**
     * Defines the docs directory.
     * This value cannot be overridden.
     * @todo make this overrideable
     */
    const DOCUMENTATION_DIRECTORY = OROBOROS_DOCS;

    /**
     * Defines the services directory.
     * This value cannot be overridden.
     * @todo make this overrideable
     */
    const SERVICE_DIRECTORY = OROBOROS_SERVICES;

    /**
     * Defines the bin directory.
     * This value cannot be overridden.
     */
    const BIN_DIRECTORY = OROBOROS_BIN;

    /**
     * Defines the source directory.
     * This value cannot be overridden.
     */
    const SRC_DIRECTORY = OROBOROS_SRC;

    /**
     * Defines the library directory.
     * This value cannot be overridden.
     */
    const LIB_DIRECTORY = OROBOROS_LIB;

    /**
     * Defines the routines directory.
     * This value cannot be overridden.
     */
    const ROUTINE_DIRECTORY = OROBOROS_ROUTINES;
}
