<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\math\traits;

/**
 * <Math Trait>
 * Provides utilities for common mathematical calculations.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage math
 * @version 0.2.2
 * @since 0.2.2-alpha
 * @satisfies \oroboros\core\interfaces\contract\utilities\math\MathContract
 */
trait MathTrait
{

    use \oroboros\core\traits\utilities\core\NumericUtilityTrait;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\utilities\math\MathContract
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Square Integer>
     * Returns an integer squared.
     * @param type $int
     * @return int
     */
    public static function square($int) {
        return $int * $int;
    }

    /**
     * <Cube Integer>
     * Returns an integer cubed.
     * @param type $int
     * @return $int
     */
    public static function cube($int) {
        return $int * $int * $int;
    }

    /**
     * <To the Power of...>
     * Returns an integer [$int] to the power of [$power].
     * @param int $int
     * @param int $power
     * @return int
     */
    public static function powerof($int, $power) {
        $calc = $int;
        for ($i = 0; $i < $power; $i++) {
            $calc = $calc * $int;
        }
        return $calc;
    }

    /**
     * <Median Integer>
     * Returns the median value of the supplied array.
     * @param array $values An array of integers.
     * @return int
     */
    public static function median(array $values) {
        sort($values);
        $count = count($values); //total numbers in array
        $middleval = floor(($count - 1) / 2); // find the middle value, or the lowest middle value
        if ($count % 2) { // odd number, middle is the median
            $median = $values[$middleval];
        } else { // even number, calculate avg of 2 medians
            $low = $values[$middleval];
            $high = $values[$middleval + 1];
            $median = (($low + $high) / 2);
        }
        return $median;
    }

    /**
     * <Mean Integer>
     * Returns the mean (average) value of the supplied array.
     * @param array $values An array of integers.
     * @return int
     */
    public static function mean(array $values) {
        $count = count($values); //total numbers in array
        foreach ($values as $value) {
            $total = $total + $value; // total value of array numbers
        }
        $average = ($total / $count); // get average value
        return $average;
    }

    /**
     * <Mode Integer>
     * Returns the mode value of the supplied array.
     * @param array $values An array of integers.
     * @return int
     */
    public static function mode(array $values) {
        $count = array();
        foreach ($values as $item) {
            if (isset($count[$item])) {
                $count[$item] ++;
            } else {
                $count[$item] = 1;
            };
        };
        $mode = false;
        $iter = 0;
        foreach ($count as $k => $v) {
            if ($v > $iter) {
                $mode = $k;
                $iter = $v;
            };
        };
        return $mode;
    }

    /**
     * <Range Integer>
     * Returns the range value of the supplied array.
     * @param array $values An array of integers.
     * @return int
     */
    public static function range(array $values) {
        sort($values);
        $sml = $values[0];
        rsort($values);
        $lrg = $values[0];
        return $lrg - $sml;
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    //protected methods

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    //private methods

}
