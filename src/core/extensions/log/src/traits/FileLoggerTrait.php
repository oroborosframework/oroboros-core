<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2013-2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\log\traits;

/**
 * <File Log Manager Trait>
 * This trait provides methods required to manage logging events using the Psr-3 standard.
 * This represents the logic that will typically be directly interacted with by
 * other logic that requires logging functionality, acting as a facade
 * that abstracts out any details about the process of logging the event or
 * how it is recorded.
 *
 * This trait logs to the log file defined by the constant OROBOROS_ERROR_LOG,
 * which is defined at or before system bootload. You may change the default
 * location of this logger by simply defining that constant with the desired
 * location prior to loading the initialization file.
 *
 * Log output from this logger will be plaintext, and contain the date, log level,
 * class that logged the error, followed by the message and an indented context.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 * 
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage psr3
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\patterns\behavioral\WorkerContract
 * @satisfies \oroboros\core\interfaces\contract\libraries\environment\LoggerContract
 */
trait FileLoggerTrait
{

    use LoggerTrait;


    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * Interpolates context values into the message placeholders.
     * @return string
     */
    protected function _interpolate( $message, array $context = array() )
    {
        // build a replacement array with braces around the context keys
        $replace = array();
        foreach ( $context as
            $key =>
            $val )
        {
            // check that the value can be casted to string
            if ( !is_array( $val ) && (!is_object( $val ) || method_exists( $val,
                    '__toString' )) )
            {
                $replace['{' . $key . '}'] = $val;
            }
        }

        // interpolate replacement values into the message and return
        return strtr( $message, $replace );
    }

    /**
     * <Logger Writer Getter>
     * Returns a logger writer object, which handles the
     * actual process of sending the message to the endpoint,
     * or an adapter that can be handled like a stream.
     * @return resource
     */
    protected function _loggerGetWriter()
    {
        if ( is_null( $this->_logger_writer ) )
        {
            $this->_logger_writer = $this->_loadLogWriter();
        }
        return $this->_logger_writer;
    }

    /**
     * <Logger Writer Default Setter>
     * Loads the logger writer if none is supplied through dependency injection.
     * This method should be overridden to use a new default.
     * @return \oroboros\log\interfaces\contract\LogWriterContract
     */
    protected function _loadLogWriter()
    {
        $writer = new \oroboros\log\FileLogWriter();
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */
    /**
     * Formats the date to the specifications of the current logger.
     * This method only produces a date string, and does not wrap it
     * in any contextual wrapper.
     * @param string $date
     * @return string
     */

    /**
     * Provides the header for a new log.
     * @return string|null
     */
    private function _loggerGetLogHeading()
    {
        return null;
    }

    /**
     * Provides the footer for a new log.
     * @return string|null
     */
    private function _loggerGetLogFooter()
    {
        return null;
    }

    /**
     * Provides a date formatted to the correct
     * specifications for the current logger.
     * @param string $date
     * @return string
     */
    private function _loggerGetFormattedDate( $date )
    {
        return date_format( new \DateTime( $date ), DATE_RFC3339_EXTENDED );
    }

    /**
     * Provides a contextual string wrapper for the specified context key.
     * The context is the date, log level, or the class name, method name,
     * or file and line number where the log entry was generated.
     * @param string $content
     * return string
     */
    private function _loggerGetContextualWrapper( $content )
    {
        return '[ ' . $content . ' ]';
    }

    /**
     * Provides the wrapper for the message.
     * This is separate from the contextual
     * wrapper or backtrace trace wrapper.
     * @param string $message
     * @param array $context
     * @return string
     */
    private function _loggerGetMessageWrapper( $message )
    {
        return $message;
    }

    /**
     * Provides an output friendly wrapper for the log level.
     * @param array $level
     * @return string
     */
    private function _loggerGetFormattedLevel( $level )
    {
        return $this->_loggerGetContextualWrapper( $level );
    }

    /**
     * Provides an output friendly wrapper for the backtrace.
     * @param array $trace
     * @return string
     */
    private function _loggerGetFormattedBacktrace( $trace )
    {
        return print_r( $trace, 1 );
    }

    /**
     * Fully formats the log entry to fit the current schema,
     * and returns a fully formatted log entry.
     * @param string $level
     * @param string $message
     * @param array $context
     * @return string
     */
    private function _loggerGetFormattedMessage( $level, $message,
        $context = array() )
    {

    }

}
