<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\log\traits;

/**
 * <Oroboros Static Log Api Trait>
 * This trait provides the base accessor class with logging passthrough
 * methods compatible with the StaticControlApi declaration.
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
trait LogExtensionTrait
{

    /**
     * Designates the default logger objects made available
     * out of the box by the core library.
     * @see \oroboros\log\interfaces\enumerated\LoggerDefaults
     */
    private static $_oroboros_log_modes = array(
        "default" => \oroboros\log\interfaces\enumerated\LoggerDefaults::LOGGER_DEFAULT,
        "file" => \oroboros\log\interfaces\enumerated\LoggerDefaults::LOGGER_FILE,
        "database" => \oroboros\log\interfaces\enumerated\LoggerDefaults::LOGGER_DATABASE,
        "null" => \oroboros\log\interfaces\enumerated\LoggerDefaults::LOGGER_NULL,
        "screen" => \oroboros\log\interfaces\enumerated\LoggerDefaults::LOGGER_SCREEN,
        "cli" => \oroboros\log\interfaces\enumerated\LoggerDefaults::LOGGER_CLI,
        "css" => \oroboros\log\interfaces\enumerated\LoggerDefaults::LOGGER_CSS,
        "js" => \oroboros\log\interfaces\enumerated\LoggerDefaults::LOGGER_JS,
        "ajax" => \oroboros\log\interfaces\enumerated\LoggerDefaults::LOGGER_AJAX,
    );

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * Logs an debug to all loggers that are active,
     * if debug level logging is enabled.
     * @param string $message The log message
     * @param array $context (optional) Contextual keys to pass into the loggers
     * @return void
     */
    protected static function _logDebug( $message, array $context = array() )
    {
        self::_getLogger()->debug( $message, $context );
    }

    /**
     * Logs an info to all loggers that are active,
     * if info level logging is enabled.
     * @param string $message The log message
     * @param array $context (optional) Contextual keys to pass into the loggers
     * @return void
     */
    protected static function _logInfo( $message, array $context = array() )
    {

        self::_getLogger()->info( $message, $context );
    }

    /**
     * Logs a notice to all loggers that are active,
     * if notice level logging is enabled.
     * @param string $message The log message
     * @param array $context (optional) Contextual keys to pass into the loggers
     * @return void
     */
    protected static function _logNotice( $message, array $context = array() )
    {

        self::_getLogger()->notice( $message, $context );
    }

    /**
     * Logs a warning to all loggers that are active,
     * if warning level logging is enabled.
     * @param string $message The log message
     * @param array $context (optional) Contextual keys to pass into the loggers
     * @return void
     */
    protected static function _logWarning( $message, array $context = array() )
    {

        self::_getLogger()->warning( $message, $context );
    }

    /**
     * Logs an error to all loggers that are active,
     * if error level logging is enabled.
     * @param string $message The log message
     * @param array $context (optional) Contextual keys to pass into the loggers
     * @return void
     */
    protected static function _logError( $message, array $context = array() )
    {

        self::_getLogger()->error( $message, $context );
    }

    /**
     * Logs a critical to all loggers that are active,
     * if critical level logging is enabled.
     * @param string $message The log message
     * @param array $context (optional) Contextual keys to pass into the loggers
     * @return void
     */
    protected static function _logCritical( $message, array $context = array() )
    {

        self::_getLogger()->critical( $message,
            $context );
    }

    /**
     * Logs an alert to all loggers that are active,
     * if alert level logging is enabled.
     * @param string $message The log message
     * @param array $context (optional) Contextual keys to pass into the loggers
     * @return void
     */
    protected static function _logAlert( $message, array $context = array() )
    {

        self::_getLogger()->alert( $message, $context );
    }

    /**
     * Logs an emergency to all loggers that are active,
     * if emergency level logging is enabled.
     * @param string $message The log message
     * @param array $context (optional) Contextual keys to pass into the loggers
     * @return void
     */
    protected static function _logEmergency( $message, array $context = array() )
    {

        self::_getLogger()->emergency( $message,
            $context );
    }

    /**
     * Logs with an arbitrary level.
     * @param string $level The log level [debug|info|notice|warning|error|critical|alert|emergency]
     * @param string $message The log message
     * @param array $context (optional) Contextual keys to pass into the loggers
     * @return void
     */
    protected static function _logLog( $level, $message, array $context = array() )
    {

        self::_getLogger()->log( $level, $message,
            $context );
    }

    /**
     * Adds a new logger to the LogManager pool, or replaces an existing
     * one with the same context. This method will throw an exception if an
     * invalid parameter is passed instead of triggering a fatal error, so it
     * can potentially be handled without breaking execution if desired.
     * @param \Psr\Log\LoggerInterface $logger The logger object to set
     * @param string $name (optional) A slug to refer to the logger as. This will be automatically determined if not supplied.
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if an invalid logger is passed
     */
    protected static function _logSet( $logger, $name = null )
    {

        if ( !is_object( $logger ) || !($logger instanceof \Psr\Log\LoggerInterface) )
        {
            throw new \oroboros\core\utilities\exception\InvalidArgumentException(
            sprintf( \oroboros\core\interfaces\enumerated\exception\ExceptionMessage::ERROR_LOGIC_BAD_PARAMETERS_MESSAGE,
                __METHOD__, '\\Psr\\Log\\LoggerInterface',
                is_object( $logger )
                    ? get_class( $logger )
                    : gettype( $logger )  )
            );
        }
        self::_getLogger()->setLogger( $logger, $name );
    }

    /**
     * Resets the LogManager. The LogManager will reinitialize with the
     * provided parameters. If no parameters are provided, it will use
     * its default parameters.
     * @see \oroboros\core\traits\patterns\behavioral\ManagerTrait::initialize
     * @param type $dependencies (optional) Loggers to inject into the LogManager. Must be a valid instance of \Psr\Log\LoggerInterface
     * @param type $settings (optional) Logging settings to inject into the LogManager.
     * @param type $flags (optional) Flags to inject into the LogManager.
     * @return void
     */
    protected static function _logReset( $dependencies = array(),
        $settings = array(), $flags = array() )
    {

        self::_getLogger()->initialize( $dependencies,
            $settings, $flags );
    }

    /**
     * Lists the available loggers by their context key.
     * These keys can be used in other methods to refer
     * to a specific logger.
     */
    protected static function _logList()
    {

        return self::_getLogger()->listLoggers();
    }

    /**
     * Enables logging to a specific logger, by its identifying worker scope.
     * This does not affect any other loggers, and does not have an effect if
     * the given context is not known to the LogManager. Available contexts
     * can be checked with the 'list' command.
     * @param string $context
     * @return void
     */
    protected static function _logEnable( $context )
    {

        self::_getLogger()->enableLogger( $context );
    }

    /**
     * Disables logging to a specific logger, by its identifying worker scope.
     * This does not affect any other loggers, and does not have an effect if
     * the given context is not known to the LogManager. Available contexts
     * can be checked with the 'list' command.
     * @param string $context
     * @return void
     */
    protected static function _logDisable( $context )
    {

        self::_getLogger()->disableLogger( $context );
    }

    /**
     * Flushes the log buffer from all loggers that maintain an internal buffer.
     * If $return is true (default), these buffers will be returned as an array.
     * If $return is false, they will be printed directly and no return value
     * will be received.
     * @param bool $return
     * @return array|void
     */
    protected static function _logFlush( $return = true )
    {

        self::_getLogger()->flushLogs( $return );
    }

    /**
     * Clears existing log buffers. This only affects loggers that collect
     * their messages and flush them all at once, such as screen loggers or
     * ajax loggers. Any logger that logs in realtime to its active
     * destination will remain unaffected.
     * @return void
     */
    protected static function _logClear()
    {

        self::_getLogger()->clearLogs();
    }

    /**
     * Sets the log visibility for a given level to show or hide.
     * @param string $level The log level to affect. This should be a valid Psr-3 log level [emergency|alert|critical|error|warning|notice|info|debug]
     * @param bool $show (optional) If true, the log level will render in the log. If false, it will be omitted. Default true.
     * @return void
     */
    protected static function _logVisibility( $level, $show = true )
    {

        if ( $show )
        {
            self::_getLogger()->enableLogLevel( $level );
        } else
        {
            self::_getLogger()->disableLogLevel( $level );
        }
    }

    private static function &_getLogger()
    {
        $logger = self::_staticBaselineGetDependency('log');
        if ( is_null( $logger ) )
        {
            throw new \oroboros\core\utilities\exception\RuntimeException(
            sprintf( 'Error encountered at [%s]. Logger instance of [%s] '
                . 'must be set as a static baseline dependency to use '
                . 'this trait.', __METHOD__,
                '\\oroboros\\log\\interfaces\\contract\\LogManagerContract' ),
            \oroboros\core\interfaces\enumerated\exception\ExceptionCode::ERROR_CORE_EXTENSION_FAILURE );
        }
        return $logger;
    }

}
