<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\log\interfaces\contract;

/**
 * <Logger Contract>
 * Defines the standard methods for interacting with the logger.
 * This follows the Psr-3 standards for logger objects.
 *
 * Describes a logger instance
 *
 * The message MUST be a string or object implementing __toString().
 *
 * The message MAY contain placeholders in the form: {foo} where foo
 * will be replaced by the context data in key "foo".
 *
 * The context array can contain arbitrary data, the only assumption that
 * can be made by implementors is that if an Exception instance is given
 * to produce a stack trace, it MUST be in a key named "exception".
 *
 * @see https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md
 * for the full interface specification.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface LogWriterContract
extends \oroboros\core\interfaces\contract\libraries\LibraryContract
{

    /**
     * <Log Writer Constructor>
     * Oroboros Core log writers have a standardized constructor.
     * Settings are nullable, but the log writer will generally
     * not do anything if it does not have settings injected.
     *
     * The Logger will inject the correct parameters in the normal
     * context of logger usage in this system, but will only do so for loggers
     * that honor the Oroboros Core logger contract in addition
     * to \Psr\Log\LoggerInterface.
     *
     * @param array $settings
     */
    public function __construct( $settings = null, $destination = null );

    /**
     * <Log Writer Write Method>
     * Writes the message to the endpoint.
     * @param string $message
     * @return void
     */
    public function write( $message );

    /**
     * <Log Writer Flush Method>
     * Flushes the output buffer, if it is buffered.
     * This will print directly.
     * @return void
     */
    public function flush( $return );

    /**
     * <Log Writer Clear Method>
     * Clears the output buffer, if buffering is possible.
     * This will discard all buffered messages.
     * @return voind
     */
    public function clear();
}
