<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\scope;

/**
 * <Pattern Class Scope Base Enumerated Api Interface>
 * This is the base enumerated api interface for pattern class scope definitions.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage classes
 * @version 0.2.5
 * @since 0.2.5
 */
interface PatternClassScopes
extends ClassScopeBase
{

    /**
     * <Pattern>
     *
     * ----------------
     *
     * A pattern is just a design pattern. It provides an unopinionated approach
     * to efficiently performing a specific type of task, without any regard to
     * why that task is neccessary. These are building blocks for robust class
     * functionality. they are never required for use, but speed up ongoing
     * development and scalability drastically if used intelligently. If used
     * recklessly, they overengineer your system into a nightmare.
     *
     * Design patterns are usually provided as an abstract that can be extended,
     * a trait that grants all of their methods independently, and an interface
     * that enforces an agreed upon approach for interoperability.
     *
     * All of the abstract classes for design patterns are very low level,
     * and extending those will require that you provide the logic for all
     * of the actual work manually. Generally, it is better to use the trait
     * as a drop-in solution wherever it is needed, and affix the interface if
     * it needs to be passed around the system (pattern traits ALWAYS satisfy
     * the interface requirements of the same pattern,
     * class constant declarations notwithstanding).
     */

    /**
     * <Generic Pattern>
     * This classification designates only that the class is a design pattern,
     * and makes no further assumption about it. You will need to provide your
     * own documentation if you use this scope.
     */
    const CLASS_SCOPE_PATTERN = "::pattern::";
    const CLASS_SCOPE_PATTERN_ABSTRACT = "::abstract-pattern::";
    //behavioral patterns
    const CLASS_SCOPE_BEHAVIORAL_PATTERN = "::behavioral-pattern::";
    //creational patterns
    const CLASS_SCOPE_CREATIONAL_PATTERN = "::creational-pattern::";
    const CLASS_SCOPE_CREATIONAL_ABSTRACT_FACTORY_PATTERN = "::abstract-factory-creational-pattern::";
    const CLASS_SCOPE_CREATIONAL_FACTORY_PATTERN = "::factory-creational-pattern::";
    const CLASS_SCOPE_CREATIONAL_PROTOTYPE_PATTERN = "::prototype-creational-pattern::";
    const CLASS_SCOPE_CREATIONAL_BUILDER_PATTERN = "::builder-creational-pattern::";
    const CLASS_SCOPE_CREATIONAL_OBJECT_POOL_PATTERN = "::object-pool-creational-pattern::";

    /**
     * <Singleton pattern>
     * A pattern that enforces only a single instance of an object.
     *
     * There is a lot of discussion over whether or not this is an anti-pattern.
     * I tend to agree that is is, but will not exclude it's compatibility
     * from 3rd party integration.
     *
     * We do not use this pattern internally,
     * but provide the classification
     * for people using this package who want to.
     */
    const CLASS_SCOPE_CREATIONAL_SINGLETON_PATTERN = "::singleton-creational-pattern::";
    //structural patterns
    const CLASS_SCOPE_STRUCTURAL_PATTERN = "::structural-pattern::";
    const CLASS_SCOPE_STRUCTURAL_REGISTRY_PATTERN = "::registry-structural-pattern::";
    const CLASS_SCOPE_STRUCTURAL_STATIC_REGISTRY_PATTERN = "::static-registry-structural-pattern::";
    //concurrency patterns
    const CLASS_SCOPE_CONCURRENCY_PATTERN = "::concurrency-pattern::";

    //more classifications may be added if we ever
    //decide to dig into enterprise level patterns
    //for this package.


}
