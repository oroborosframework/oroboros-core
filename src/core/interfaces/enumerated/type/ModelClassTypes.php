<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\type;

/**
 * <Oroboros Library Class Type Api>
 *
 * This Interface should be used as the authority on all class types used
 * throughout the system designated as models. All system classes designated
 * as models can be expected to have one of these class types.
 *
 * These types will be extendable with your own customized class types as well,
 * which is not currently implemented, but should be within a couple of releases.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage classes
 * @version 0.2.5
 * @since 0.2.5
 */
interface ModelClassTypes
extends ClassTypeBase
{

    /**
     * <Model Class Types>
     *
     * ----------------
     * 
     * A model is essentially a library that performs system-specific tasks.
     * Unlike a library, it is useless outside of the system that it is defined
     * to use, because it has inherent association with the logic and structure
     * of that specific system.
     *
     * Models primarily control the flow, presentation, and modification
     * of information. Often times, they are structured to be automatically
     * bound to a data layer, but this system does not really take that
     * approach to them, be cause then you would not be able to interract with
     * other sources of data effectively with a model, which degrades your
     * overall structure when it is encountered and you have to start hacking
     * in libraries that also fullfill the same responsibilities as models, or
     * doing model stuff in the controller directly to compensate.
     *
     * The approach to Models that Oroboros takes, is that a model only
     * represents the need to interract with a source of data and execute
     * business logic against it, of which a data-layer is one type (but not the only type).
     * For this reason, several implementations of abstract Model are provided,
     * which can easily be scoped to just about any type of business logic
     * application you need to do by extension. Model traits are also provided
     * to accomplish this behavior in some other class (like maybe a model from
     * another framework) in a way that is still recognizably valid through the
     * interal structure. This allows you to make interoperable Models, which is
     * typically unheard of, but can be used to maintain portability between two
     * systems.
     *
     * For example, if you had a legacy application built on CodeIgniter,
     * and you wanted to port it to Laravel (or to Oroboros :) ),
     * you can use the Oroboros Model trait as a bridge to smooth out the
     * distinction between the two, which will let you incrementally refactor
     * to conform to the desired system's interface, and then eventually move
     * it fully without really interrupting the live execution much.
     *
     * This approach was taken specifically because it is usually not possible
     * to salvage much when porting models from one framework to another, as
     * both have their own specific opinion about how it ought to be done,
     * which often is the underlying cause of a full rewrite of an existing
     * application. This is NOT a drop-in compatibility solution, it just takes
     * a task that is usually impossible, and makes it somewhat approachable.
     *
     * Oroboros models work fine independently of any other system, and do not
     * require external integration whatsoever. The primary purpose of this
     * package is to satisfy all baseline needs and also allow those needs to
     * be interchangeable for external preferred resources, so care has been
     * taken to insure that both needs are well represented, particularly in
     * how the Model class has been abstracted.
     */
    const CLASS_TYPE_MODEL = "::model::";

}
