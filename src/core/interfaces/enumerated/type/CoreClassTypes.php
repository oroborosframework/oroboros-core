<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\type;

/**
 * <Oroboros Core Class Type Api>
 *
 * This Interface should be used as the authority on all class types used
 * throughout the system designated as core classes. All system classes designated
 * as internals not meant for external use can be expected to have one of these class types.
 *
 * These types will be extendable with your own customized class types as well,
 * which is not currently implemented, but should be within a couple of releases.
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage classes
 * @version 0.2.5
 * @since 0.2.5
 */
interface CoreClassTypes
extends ClassTypeBase
{

    /**
     * <Core Class Types>
     *
     * ----------------
     *
     * Core classes represent very system-specific classes provided to access
     * functionality, that are not appropriate to override. Do not use this
     * class type for 3rd party integrations.
     */
    const CLASS_TYPE_CORE = "::core::";

    /**
     * <Core Base Accessor>
     *
     * ----------------
     *
     * Designates that the class is a accessor class for further functionality
     * that can be used globally.
     */
    const CLASS_TYPE_CORE_ACCESSOR = "::accessor-core::";

    /**
     * <Core Base>
     *
     * ----------------
     *
     * Designates that the class fulfills all baseline contractual obligation.
     */
    const CLASS_TYPE_CORE_BASE = "::base-core::";

    /**
     * <Core Context>
     *
     * ----------------
     *
     * Designates that the class is a contextual reference.
     */
    const CLASS_TYPE_CORE_CONTEXT = "::context-core::";

    /**
     * <Core Flag>
     *
     * ----------------
     *
     * Designates that the class is a contextual reference to a flag.
     */
    const CLASS_TYPE_CORE_FLAG = "::flag-core::";

    /**
     * <Core Hook>
     *
     * ----------------
     *
     * Designates that the class is a contextual reference to a hook.
     */
    const CLASS_TYPE_CORE_HOOK = "::hook-core::";

    /**
     * <Core Dictionary>
     *
     * ----------------
     *
     * Designates that the class is a container of read-only reference values.
     */
    const CLASS_TYPE_CORE_DICTIONARY = "::dictionary-core::";

    /**
     * <Core Lang>
     *
     * ----------------
     *
     * Designates that the class is a container of translation
     * references for a given context.
     */
    const CLASS_TYPE_CORE_LANG = "::lang-core::";

    /**
     * <Core Setting>
     *
     * ----------------
     *
     * Designates that the class is a contextual reference
     * to a specific setting.
     */
    const CLASS_TYPE_CORE_SETTING = "::setting-core::";

    /**
     * <Core Extension>
     *
     * ----------------
     *
     * Designates that the class is an extension to
     * core outward-facing object apis.
     */
    const CLASS_TYPE_CORE_EXTENSION = "::extension-core::";

    /**
     * <Core Module>
     *
     * ----------------
     *
     * Designates that the class is an module that extends upon
     * core internal logic.
     */
    const CLASS_TYPE_CORE_MODULE = "::module-core::";

    /**
     * <Core Component>
     *
     * ----------------
     *
     * Designates that the class enhances the normal return value
     * of core output.
     */
    const CLASS_TYPE_CORE_COMPONENT = "::component-core::";

    /**
     * <Core Library>
     *
     * ----------------
     *
     * Designates that the class is a library provided by the core.
     */
    const CLASS_TYPE_CORE_LIBRARY = "::library-core::";

    /**
     * <Core Utility>
     *
     * ----------------
     *
     * Designates that the class is a utility provided by the core.
     */
    const CLASS_TYPE_CORE_UTILITY = "::utility-core::";

    /**
     * <Core Internal>
     *
     * ----------------
     *
     * Designates that the class is used internally for a specific purpose
     * and is not meant for external consumption or extension.
     */
    const CLASS_TYPE_CORE_INTERNAL = "::internal-core::";

    /**
     * <Core Value>
     *
     * ----------------
     *
     * Designates that the class is used to represent a specific value,
     * and provide some contextual information about its point of origin.
     */
    const CLASS_TYPE_CORE_VALUE = "::value-core::";

}
