<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\exception;

/**
 * <Exception Message Api>
 * Provides default message bodies for error messages.
 *
 * These are intended to be used in conjunction with sprintf to inject values.
 *
 * These will be updated to provide a default that
 * corresponds to each system error code.
 * @see \oroboros\core\interfaces\enumerated\exception\ExceptionCode
 * @author Brian Dayhoff <mopsyd@me.com>
 */
interface ExceptionMessage extends ExceptionCode {

    /**
     * <Default Exception Message Suffix>
     * If suffix is enabled, this will be appended to the end of all messages.
     * Thrown at [__LINE__, __FUNCTION__, __METHOD__] of [__FILE__, __CLASS__, __TRAIT__]
     */
    const DEFAULT_EXCEPTION_MESSAGE_SUFFIX = ' Thrown at [%s] of [%s].';

    /**
     * <Default Exception Message Prefix>
     * If prefix is enabled, this will be prepended to the exception message.
     * 1) [$code] -Error code passed with the exception
     * 2) [$severity] -error, warning, info, debug, alert, critical, etc (See Psr-3 spec)
     * 3) [$class|$trait|$file] -\some\namespace\classOrTrait::method OR /fully/qualified/filepath.php
     * 4) [$request_uri|$cli_command] -console command OR http://example.com/path/to/page/
     * 5) [$request_type] -cgi, get, post, put, delete, etc.
     */
    const DEFAULT_EXCEPTION_MESSAGE_PREFIX = ' [%s][%s][%s][%s][%s] ';

    /**
     * This is the default \Exception code,
     * so it maps to unknown error. The system will treat all
     * uncaught exceptions with status code 0 as FATAL ERRORS.
     */
    const ERROR_UNKNOWN_MESSAGE = 'An unknown error has occurred.';

    /**
     * <Note about extending this api>
     * Extending this functionality with your own error messages requires two steps.
     *
     * First, create a status code api interface as per below (it should extend this)
     * @see \oroboros\core\api\ExceptionCodeApi
     *
     * Second, create a message api interface that extends this class as well as your status code api interface
     * @see \oroboros\core\api\
     */

    /**
     * Most of the following will get chunked out into their own api's.
     * @todo migrate individual error apis
     */

    /**
     * Generic Oroboros core errors.
     *
     * ----------------
     *
     * These represent errors in the oroboros core class structure.
     */
    const ERROR_CORE_MESSAGE = 'An unknown error has occured with an oroboros class.'; //Generic Oroboros core errors.
    const ERROR_CORE_LIBRARY_FAILURE_MESSAGE = 'Library [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_CONTROLLER_FAILURE_MESSAGE = 'Controller [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_MODEL_FAILURE_MESSAGE = 'Model [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_VIEW_FAILURE_MESSAGE = 'View [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_ADAPTER_FAILURE_MESSAGE = 'Adapter [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_AUTH_FAILURE_MESSAGE = 'Authorization [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_MODULE_FAILURE_MESSAGE = 'Module [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_EXTENSION_FAILURE_MESSAGE = 'Extension [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_PARSER_FAILURE_MESSAGE = 'Parser [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_RECORD_FAILURE_MESSAGE = 'Record [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_STREAM_FAILURE_MESSAGE = 'Stream [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_SERVICE_FAILURE_MESSAGE = 'Service [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_RESPONSE_FAILURE_MESSAGE = 'Response object [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_REQUEST_FAILURE_MESSAGE = 'Request object [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_ROUTER_FAILURE_MESSAGE = 'Router object [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_UTILITY_FAILURE_MESSAGE = 'Utility [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_JOB_FAILURE_MESSAGE = 'Job [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_ENTITY_FAILURE_MESSAGE = 'Entity object [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_PATTERN_FAILURE_MESSAGE = 'Pattern [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_ROUTINE_FAILURE_MESSAGE = 'Routine [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_EVENT_FAILURE_MESSAGE = 'Event object [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_FUNCTION_FAILURE_MESSAGE = 'Function [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_ERROR_FAILURE_MESSAGE = 'Error object [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_FLAG_FAILURE_MESSAGE = 'Flag object [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_ENUM_FAILURE_MESSAGE = 'Enum object [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_DATA_OBJECT_FAILURE_MESSAGE = 'Data object [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.
    const ERROR_CORE_BOOTSTRAP_FAILURE_MESSAGE = 'Bootstrap object [%s] has failed, due to an irrecoverable condition: [%s].'; //Generic Oroboros core errors.

    /**
     * PHP Errors
     *
     * ----------------
     *
     * Generic or expected errors that can't be handled,
     */
    const ERROR_PHP_MESSAGE = 'A generic PHP exception has occurred: [%s].'; //PHP Errors, vanilla exceptions that can't be resolved, etc.
    const ERROR_PHP_METHOD_FAILURE_MESSAGE = 'Method [%s] has failed in class [%s].'; //Method Failure.
    const ERROR_PHP_INSTANTIATION_FAILURE_MESSAGE = 'Could not complete instantiation due to an unexpected condition [%s].'; //Instantiation Failure.
    const ERROR_PHP_DESTRUCTOR_FAILURE_MESSAGE = 'Destructor failed to fire in [%s].'; //Destruct Failure.
    const ERROR_PHP_BAD_PARAMETERS_MESSAGE = 'Invalid parameters passed. Expected [%s], but received [%s].'; //Bad parameters.
    const ERROR_PHP_BAD_METHOD_CALL_MESSAGE = 'Method [%s] does not exist in [%s].'; //Bad method call.
    const ERROR_PHP_BAD_FUNCTION_CALL_MESSAGE = 'Function [%s] does not exist.'; //Bad function call.
    const ERROR_PHP_WRONG_INSTANCE_SUPPLIED_MESSAGE = 'Invalid instance passed. Expected instanceof [%s], but received [%s].'; //Wrong instance passed.
    const ERROR_PHP_WRONG_SCHEMA_PROVIDED_MESSAGE = 'Invalid argument schema provided. Expected keys [%s], received [%s].'; //Wrong schema provided (Missing expected keys in array, etc).
    const ERROR_PHP_NOT_INITIALIZED_MESSAGE = 'Could not complete operation because [%s] is not initialized. Please call the initialization method [%s] before referencing this method.'; //Not initialized.
    const ERROR_PHP_INVOCATION_FAILURE_MESSAGE = 'Magic method [__invoke] failed to resolve in [%s].'; //Invocation failed.
    const ERROR_PHP_CALL_FAILURE_MESSAGE = 'Magic method [__call] failed to resolve in [%s].'; //Call failed.
    const ERROR_PHP_CLONE_FAILURE_MESSAGE = 'Magic method [__clone] failed to resolve in [%s].'; //Clone failed.
    const ERROR_PHP_SERIALIZATION_FAILURE_MESSAGE = 'Magic method [__serialize] failed to resolve in [%s].'; //Serialization failed.
    const ERROR_PHP_UNSERIALIZATION_FAILURE_MESSAGE = 'Magic method [__unserialize] failed to resolve in [%s].'; //Serialization failed.
    const ERROR_PHP_SLEEP_FAILURE_MESSAGE = 'Magic method [__sleep] failed to resolve in [%s].'; //Sleep failed.
    const ERROR_PHP_WAKEUP_FAILURE_MESSAGE = 'Magic method [__wakeup] failed to resolve in [%s].'; //Wakeup failed.
    const ERROR_PHP_GETTER_FAILURE_MESSAGE = 'Magic method [__get] failed to resolve in [%s].'; //Get failed.
    const ERROR_PHP_SETTER_FAILURE_MESSAGE = 'Magic method [__set] failed to resolve in [%s].'; //Set failed.
    const ERROR_PHP_TO_ARRAY_FAILURE_MESSAGE = 'Magic method [__toArray] failed to resolve in [%s].'; //To Array failed. Placeholder for if the language ever supports this, and may be used through internal abstraction.
    const ERROR_PHP_CALLSTATIC_FAILURE_MESSAGE = 'Magic method [__callStatic] failed to resolve in [%s].'; //CallStatic failed.
    const ERROR_PHP_IS_SET_FAILURE_MESSAGE = 'Magic method [__toArray] failed to resolve in [%s].'; //IsSet failed.
    const ERROR_PHP_UN_SET_FAILURE_MESSAGE = 'Magic method [__unSet] failed to resolve in [%s].'; //UnSet failed.
    const ERROR_PHP_DEBUG_INFO_FAILURE_MESSAGE = 'Magic method [__debugInfo] failed to resolve in [%s].'; //DebugInfo failed.
    const ERROR_PHP_KEY_NOT_FOUND_MESSAGE = 'Requested key [%s] does not exist in [%s].'; //Requested key does not exist.
    const ERROR_FILESYSTEM_MESSAGE = 'Filesystem operation failed for [%s], because [%s].'; //Filesystem Error - Any issues arising from file access, missing resources, etc.
    const ERROR_DATABASE_MESSAGE = 'A database error occurred in [%s]. Error message: [%s].'; //Database Error - A database error. These can range from trivial to very, very bad. Check the log if you see these.
    const ERROR_SESSION_MESSAGE = 'A session error occurred in [%s]. Error message: [%s].'; //Session Error - A session mismatch. May occur either due to the server or the client. Usually aggravating, but not serious.
    const ERROR_SECURITY_MESSAGE = 'A security error occurred in [%s]. Error message: [%s].'; //Security Error - An error with a security protocol. Usually thrown because of an intentional, malicious attempt to break things by a 3rd party.
    const ERROR_SECURITY_LOCKED_RESOURCE_MESSAGE = 'Your request could not be resolved because resource [%s] is locked.'; //Security Error - An attempt was made to access a locked resource in the codebase.
    const ERROR_SECURITY_LOCKED_FILE_MESSAGE = 'Your request could not be resolved because file [%s] is locked.'; //Security Error - An attempt was made to access a locked resource in the filebase.
    const ERROR_SECURITY_LOCKED_TABLE_MESSAGE = 'Your request could not be resolved because table [%s] is locked.'; //Security Error - An attempt was made to access a locked table in the database.
    const ERROR_SECURITY_LOCKED_COLUMN_MESSAGE = 'Your request could not be resolved because column [%s] is locked.'; //Security Error - An attempt was made to access a locked column in the database.
    const ERROR_SECURITY_LOCKED_OBJECT_MESSAGE = 'Your request could not be resolved because object [%s] is locked.'; //Security Error - An attempt was made to access a locked object.
    const ERROR_SECURITY_LOCKED_EVENT_MESSAGE = 'Your request could not be resolved because event [%s] is locked.'; //Security Error - An attempt was made to access a locked event.
    const ERROR_SECURITY_LOCKED_ENTITY_MESSAGE = 'Your request could not be resolved because entity [%s] is locked.'; //Security Error - An attempt was made to access a locked entity.
    const ERROR_SECURITY_LOCKED_JOB_MESSAGE = 'Your request could not be resolved because job [%s] is locked.'; //Security Error - An attempt was made to access a locked job.
    const ERROR_SECURITY_LOCKED_POLICY_MESSAGE = 'Your request could not be resolved because policy [%s] is locked.'; //Security Error - An attempt was made to access a locked policy.
    const ERROR_SECURITY_LOCKED_ROUTE_MESSAGE = 'Your request could not be resolved because route [%s] is locked.'; //Security Error - An attempt was made to access a locked route.
    const ERROR_SECURITY_LOCKED_COMMAND_MESSAGE = 'Your request could not be resolved because command [%s] is locked.'; //Security Error - An attempt was made to access a locked command.
    const ERROR_ROUTING_MESSAGE = 'An error occurred with routing at [%s]. Message: [%s].'; //Routing Error - An error with DNS resolution. This means a request was made for something that doesn't exist, or that resource pointers are wrong.
    const ERROR_LOGIC_MESSAGE = 'A logic error occurred at [%s]. Message: [%s].'; //Logic Error - An error generated from sloppy programming logic, like infinite recursion or dividing by zero. These should usually be anticipated and handled before they happen, and indicate that a patch is needed.
    const ERROR_LOGIC_BAD_PARAMETERS_MESSAGE = 'A logic error occurred at [%s] due to bad parameters. Expected [%s] but received [%s].'; //Logic Error - A parameter was supplied, but is malformed.
    const ERROR_LOGIC_MISSING_PARAMETERS_MESSAGE = 'A logic error occurred at [%s] due to missing required parameters [%s].'; //Logic Error - A required parameter was not supplied.
    const ERROR_INITIALIZATION_MESSAGE = 'A generic initialization error occurred in [%s]. Message [%s].'; //Initialization Error - An error with initialization/bootstrap.
    const ERROR_INSTALLATION_MESSAGE = 'A generic installation error occurred in [%s]. Message [%s].'; //Installation Error - An error with initial setup. These are very bad.
    const ERROR_MODEL_MESSAGE = 'A generic model error occurred in [%s]. Message [%s].'; //Model Error - Any error that cannot be handled by a model. These are bad.
    const ERROR_VIEW_MESSAGE = 'A generic view error occurred in [%s]. Message [%s].'; //View Error - Any error in output logic.
    const ERROR_LIBRARY_MESSAGE = 'A generic library error occurred in [%s]. Message [%s].'; //Controller Error - An error in a library.
    const ERROR_CONTROLLER_MESSAGE = 'A generic controller error occurred in [%s]. Message [%s].'; //Controller Error - An error in the controller.
    const ERROR_ADAPTER_MESSAGE = 'A generic adapter error occurred in [%s]. Message [%s].'; //Adapter Error - Any error with an adapter used for interfacing with other resources.
    const ERROR_MODULE_MESSAGE = 'A generic module error occurred in [%s]. Message [%s].'; //Module Error - Any error from a 3rd party module
    const ERROR_ROUTINE_MESSAGE = 'A generic initialization error occurred in [%s]. Message [%s].'; //Routine Error - An error generated while running a procedural routine.
    const ERROR_SDK_MESSAGE = 'A generic SDK error occurred in [%s]. Message [%s].'; //SDK Error - Any error generated from a remote 3rd party service returning bad results.
    const ERROR_NETWORK_MESSAGE = 'A generic network error occurred in [%s]. Message [%s].'; //RESERVED FOR EXPANSION
    const ERROR_CLUSTER_MESSAGE = 'A generic cluster error occurred in [%s]. Message [%s].'; //RESERVED FOR EXPANSION
    const ERROR_NODE_MESSAGE = 'A generic error occurred in node [%s]. Message [%s].'; //RESERVED FOR EXPANSION
}
