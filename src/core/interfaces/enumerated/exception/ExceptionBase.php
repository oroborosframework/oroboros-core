<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\exception;

/**
 * <Exception Base Enumerated Api Interface>
 * This is the base interface for core exception api interfaces.
 * All other core exception enumerated api interfaces extend from this interface.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage exception
 * @version 0.2.5
 * @since 0.2.5
 */
interface ExceptionBase
extends \oroboros\core\interfaces\enumerated\core\CoreBase
{

    /**
     * This is the default \Exception code,
     * so it maps to unknown error. The system will treat all
     * uncaught exceptions with status code 0 as FATAL ERRORS.
     */
    const ERROR_UNKNOWN = 0;

    /**
     * <Note about extending this api>
     * Range 0-49999 is reserved for HTTP Status codes.
     * They don't go that high, but the spec may change
     * in the future, so there is room for error
     * when integrating new class types.
     * @see \oroboros\core\api\ExceptionCodeApi
     */

    /**
     * <Safe 3rd Party Code Range>
     * Oroboros core (and authorized extensions)
     * will never claim an error code above this number.
     *
     * If you want to extend this API with your own codes, they should be
     * higher than this number to avoid system collisions, which might produce
     * really strange bugs or inaccurate error reporting if you are also using
     * the system provided exceptions.
     *
     * Package validation will fail your package build if this occurs.
     * (meaning you have provided custom error codes within the reserved range)
     *
     * Collisions with other 3rd party extensions cannot be guaranteed, so try to
     * pick an error range that you know isn't used elsewhere.
     *
     * Anything above the below value will not collide with the core system.
     */
    const SAFE_MINIMUM_ERROR_RANGE = 50000;

    /**
     * Most of the following will get chunked out into their own api's.
     * @todo migrate individual error apis
     */
}
