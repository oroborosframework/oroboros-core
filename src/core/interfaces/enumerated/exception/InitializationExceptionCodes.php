<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\exception;

/**
 * <Oroboros Logic Exception Code API>
 * This interface declares all of the exception codes used to designate an error
 * that prevents initialization from resolving correctly, leaving the expected
 * functionality unusable on account of initialization failure. These codes
 * can grant some contextual information about what manner of logic failure
 * occurred in the event that an exception is raised.
 *
 * -------------------------------
 *
 * If you throwing an exception from a trait,
 * you can reference these definitions like so:
 *
 * throw new \oroboros\core\utilities\exception\Exception("Some error", \oroboros\core\api::EXCEPTION_CODE_GENERAL);
 *
 * this will insure that all trait exceptions are handled seamlessly,
 * even if changes to the exception codes themselves happen in the future.
 * If you absolutely must not type that much, put a use/as statement at
 * the head of your file like so:
 *
 */
interface InitializationExceptionCodes
extends ExceptionBase
{

    const RANGE_ERROR_INITIALIZATION_MIN = 20000;
    const RANGE_ERROR_INITIALIZATION_MAX = 20999;

    /**
     * Logic Errors
     *
     * ----------------
     *
     * Collision or misuse of otherwise stable functionality.
     */
    const ERROR_INITIALIZATION = 20000; //Initialization Error - An error with initialization/bootstrap.
    const ERROR_INITIALIZATION_ALREADY_INITIALIZED = 20001; //Initialization Error - The object is already initialized.
    const ERROR_INITIALIZATION_DEPENDENCY_INVALID = 20002; //Initialization Error - A provided dependency is not valid.
    const ERROR_INITIALIZATION_SETTING_INVALID = 20003; //Initialization Error - A provided setting is not valid.
    const ERROR_INITIALIZATION_FLAG_INVALID = 20004; //Initialization Error - A provided flag is not valid.
    const ERROR_INITIALIZATION_MISSING_SETTING = 20005; //Initialization Error - A required setting was not provided.
    const ERROR_INITIALIZATION_MISSING_FLAG = 20006; //Initialization Error - A required flag was not provided.
    const ERROR_INITIALIZATION_MISSING_DEPENDENCY = 20007; //Initialization Error - A required dependency was not provided.

}
