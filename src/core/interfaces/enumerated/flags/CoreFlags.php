<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\flags;

/**
 * <Core Flag Enumerated Api Interface>
 * This is the master interface for core flag sets.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage flags
 * @version 0.2.4-alpha
 * @since 0.2.4-alpha
 */
interface CoreFlags
extends FlagBase
{

    /**
     * Designates that the application is in recovery,
     * and only whitelisted users should have access whatsoever.
     * Also that static resources should be used to bootstrap
     * the application instead of the standard data sources.
     * This flag typically gets tripped when the database is down,
     * or some mission critical resource is not responding.
     * This allows the whitelisted admins to log in and diagnose
     * at least enough information to get the problem corrected
     * with minimal downtime, instead of hunting through logs
     * for nebulous error messages.
     */
    const FLAG_RECOVERY = '::recovery::';

    /**
     * Designates that the application is in maintenance mode,
     * and only authorized users should have access.
     */
    const FLAG_MAINTENANCE = '::maintenance::';

    /**
     * A feature in development,
     * may be unsafe in production.
     */
    const FLAG_BETA = '::beta::';

    /**
     * An experimental feature,
     * should not be used in production.
     */
    const FLAG_ALPHA = '::alpha::';

    /**
     * Output Mode Flag.
     * ajax request. Check the headers for the request data type.

     */
    const FLAG_MODE_AJAX = '::mode-ajax::';

    /**
     * Output Mode Flag.
     * command line interface, do not use server variables
     * or set headers, output should be plaintext.
     */
    const FLAG_MODE_CLI = '::mode-cli::';

    /**
     * HTTP Response Flag.
     * GET HTTP or GET REST API request.
     */
    const FLAG_TYPE_GET = '::type-get::';

    /**
     * HTTP Response Flag.
     * POST HTTP or POST REST API request.
     */
    const FLAG_TYPE_POST = '::type-post::';

    /**
     * HTTP Response Flag.
     * PUT REST API request.
     */
    const FLAG_TYPE_PUT = '::type-put::';

    /**
     * HTTP Response Flag.
     * DELETE REST API request.
     */
    const FLAG_TYPE_DELETE = '::type-delete::';

    /**
     * HTTP Response Flag.
     * Respond with options headers.
     */
    const FLAG_TYPE_OPTIONS = '::type-options::';

    /**
     * HTTP Response Flag.
     * Respond with only http headers.
     */
    const FLAG_TYPE_HEAD = '::type-head::';

    /**
     * Designates that an object is read-only, and may only
     * provide already defined data to other sources.
     */
    const FLAG_READONLY = '::read-only::';

    /**
     * Designates that an object should not be writeable,
     * but may be readable or executeable.
     */
    const FLAG_LOCK_WRITE = '::lock-write::';

    /**
     * Designates that an object should not be readable,
     * but may still be writeable or executeable.
     */
    const FLAG_LOCK_READ = '::lock-read::';

    /**
     * Designates that an object should be locked,
     * and prevent method execution.
     */
    const FLAG_LOCK_EXECUTE = '::lock-execute::';

}
