<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\enumerated\index;

/**
 * <Index Index Enumerated Api Interface>
 * Provides an enumerated index of all index enumerated interfaces
 * that ship with Oroboros Core.
 *
 * --------
 *
 * Enumerated Api Interfaces
 * Enumerated Api Interfaces provide sets of fixed,
 * enumerated values that must maintain consistency,
 * and should not be overridden at compile time.
 *
 * In some cases, these values are considered absolute,
 * and the program will always reference them as defined.
 * In other cases, they provide baseline definitions,
 * which the individual api implementing them may allow
 * for extension upon. In these cases, the fixed values
 * MAY NOT be removed or altered, but additions and aliases
 * will be allowed for. Check the api of the individual
 * contract being considered for extension for information
 * how to accomplish this as it applies.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/enumerated_api_interface.md
 * @category enumerated-interfaces
 * @package oroboros/core
 * @subpackage core
 * @version 0.2.5
 * @since 0.2.5
 */
interface IndexIndex
extends IndexBase
{
    const INDEX_BASE_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\IndexBase';
    const INDEX_ABSTRACT_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\AbstractIndex';
    const INDEX_API_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\ApiIndex';
    const INDEX_CLASS_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\ClassIndex';
    const INDEX_COMPONENT_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\ComponentIndex';
    const INDEX_CONCRETE_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\ConcreteIndex';
    const INDEX_CONFIG_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\ConfigIndex';
    const INDEX_CONTEXT_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\ContextIndex';
    const INDEX_CONTRACT_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\ContractIndex';
    const INDEX_CORE_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\CoreIndex';
    const INDEX_DIRECTORY_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\DirectoryIndex';
    const INDEX_ENUM_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\EnumIndex';
    const INDEX_EXCEPTION_CODE_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\ExceptionCodeIndex';
    const INDEX_EXTENSION_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\ExtensionIndex';
    const INDEX_FLAG_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\FlagIndex';
    const INDEX_INDEX_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\IndexIndex';
    const INDEX_MODULE_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\ModuleIndex';
    const INDEX_SCOPE_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\ScopeIndex';
    const INDEX_TRAIT_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\TraitIndex';
    const INDEX_TYPE_INDEX = '\\oroboros\\core\\interfaces\\enumerated\\index\\TypeIndex';
}
