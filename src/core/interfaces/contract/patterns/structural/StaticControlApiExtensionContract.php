<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\contract\patterns\structural;

/**
 * <Static Control Api Extension Contract Interface>
 * The Static Control Api Extension designates an object that is intended to open up
 * the possibility of recursive api extension of control apis, like an inverted
 * Decorator.
 *
 * Classes must implement this interface if they are intended to extend
 * recursively on a StaticControlApi,and provide their own Api to the pool of
 * api methods. This allows for a tree of logic to be represented as either
 * pseudo-methods invoked by the __callStatic magic method or actual methods,
 * while still allowing the base level construct to represent one single
 * expected instance.
 *
 * It should be noted that this functionality cannot be mixed with the
 * ControlApi. Extension trees must either be fully static or
 * fully non-static.
 *
 * @see \oroboros\core\interfaces\contract\patterns\structural\StaticControlApiContract
 * @see \oroboros\core\traits\patterns\structural\StaticControlApiTrait
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage patterns
 * @version 0.2.5
 * @since 0.2.5
 */
interface StaticControlApiExtensionContract
extends StaticControlApiContract
{

    /**
     * <Static Control Api Initialization>
     * This method will be called during the
     * extension process in place of a constructor.
     *
     * This method must always fire clean, and the full
     * api must be available when it has resolved.
     *
     * Calling this method again should not produce a different result,
     * and should not be blocked by internal logic or bypassed if called
     * a second time.
     *
     * This method MUST NOT be made into a Singleton.
     *
     * @return void
     */
    public static function staticInitialize( $params = null,
        $dependencies = null, $flags = null );
}
