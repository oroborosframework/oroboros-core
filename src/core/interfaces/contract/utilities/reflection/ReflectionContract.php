<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\contract\utilities\reflection;

/**
 * <Reflection Utility Contract Interface>
 * This contract interface designates general Oroboros core reflectors.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 * 
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage utilities
 * @version 0.2.5
 * @since 0.2.5
 * @see \oroboros\core\traits\utilities\reflection\ReflectionTrait
 */
interface ReflectionContract
extends \oroboros\core\interfaces\contract\utilities\UtilityContract, \Reflector
{

    /**
     * <Oroboros Reflection Constructor>
     * Provides the default reflection constructor.
     *
     * The [$type] parameter should denote the type of property
     * that is being passed. If this is not passed, the program
     * should attempt to determine it automatically.
     *
     * Note that class constant reflection is only available in PHP 7.1+,
     * while generators and types are available in PHP 7.0+. These reflectors
     * will need to be emulated if they are not available for
     * backwards compatibility.
     *
     * @param string $property The property to evaluate.
     * @param string $property_of The property's containing construct. If it is a parameter, this should be the function/fully qualified method (eg: \namespace\ClassName::methodName). If it is a method, class constant, or class property, this should be the fully qualified class name it belongs to.
     * @param string $type (optional) the type of property
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException if the provided subject is not reflectable
     */
    public function __construct( $subject, $property_of = null,
        $namespace = null, $type = null );

    /**
     * <Reflection Call Magic Method>
     * Provides a passthrough to the reflector's internal methods.
     * @param string $name
     * @param array $args
     * @return mixed
     * @throws \oroboros\core\utilities\exception\LogicException if the object is not initialized. This means the constructor was overridden and initialization never got called. Someone overrode something they should not have, or did it improperly.
     * @throws \oroboros\core\utilities\exception\BadMethodCallException if an invalid reflection method is called
     */
    public function __call( $name, $args = array() );

    /**
     * <Reflection String Magic Method>
     * Provides a passthrough to the reflectors internal __toString method.
     * @return string
     */
    public function __toString();

    /**
     * <Reflection Instance Getter Method>
     * Returns the reflection instance to work with directly.
     * @return \ReflectionAbstract
     */
    public function getReflector();

    /**
     * <Reflection Type Getter>
     * Returns a canonicalized string indicating the type of reflection.
     * @return string
     */
    public function getType();

    /**
     * <Reflection Valid Types Getter>
     * Returns an array of the valid canonicalized strings returned by getType
     * @return array
     */
    public function getValidTypes();

    /**
     * <Reflection Type Check Method>
     * Checks if the supplied type corresponds to the current reflector type.
     * This method will not check inheritance, and will return false
     * on subclasses of reflectors. For this reason you
     * should work only with native PHP reflectors.
     * @param string $type
     * @return bool
     */
    public function isType( $type );
}
