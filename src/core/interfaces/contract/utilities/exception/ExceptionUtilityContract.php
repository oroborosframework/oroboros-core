<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\contract\utilities\exception;

/**
 * <Exception Utility Contract Interface>
 * This contract interface designates methods for standardization
 * of exception codes and messages based on enumeration.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage utilities
 * @version 0.2.5
 * @since 0.2.5
 * @see \oroboros\core\traits\utilities\exception\ExceptionTrait
 */
interface ExceptionUtilityContract
extends \oroboros\core\interfaces\contract\utilities\UtilityContract
{
    /**
     * <Exception Code Getter>
     * Returns the designated code for a canonicalized string.
     *
     * @param string $slug The canonicalized exception code identifier
     * @return int Returns 0 if code not found, otherwise returns the specified code.
     */
    public static function getCode( $slug = 'unknown' );

    /**
     * <Exception Message Getter>
     * Returns the designated message for a canonicalized string.
     *
     * @param string $slug The canonicalized exception message identifier
     * @return string Returns "An unknown error has occurred." if code not found,
     * otherwise returns the designated code for the specified message.
     */
    public static function getMessage( $slug = 'unknown' );

    /**
     * <Exception Identifier Getter>
     * Returns the designated canonicalized identifier for an exception code.
     *
     * @param int $code The exception code to reverse parse.
     * @return bool|string Returns false if code not found, otherwise returns
     * the canonicalized identifier for the code. The same identifier can be
     * used to return the corresponding default message.
     */
    public static function getCodeIdentifier( $code = 0 );

    /**
     * <Exception Code Identifier Set Getter>
     * Returns an array of all acceptable canonicalized code/message identifiers.
     *
     * @return array
     */
    public static function getCodeIdentifiers();

    /**
     * <Exception Identifier Set Getter>
     * Returns an array of all acceptable canonicalized exception identifiers.
     *
     * @return array
     */
    public static function getExceptionIdentifiers();

    /**
     * <Exception Generator Method>
     * Throws a generated exception using standardized Oroboros canonicalization.
     * This method uses standardized exception messages found in
     * the ExceptionMessage enumerated interface, and exception codes
     * found in the ExceptionCode enumerated interface.
     *
     * This method returns the exception but does not throw it.
     *
     * @example self::throwException( 'invalid-argument', 'logic-bad-parameters', array( 'string', 'boolean' );
     *
     * @param type $type
     * @param type $slug
     * @param type $params
     * @param \Exception $previous
     * @see \oroboros\core\interfaces\enumerated\exception\ExceptionCode
     * @see \oroboros\core\interfaces\enumerated\exception\ExceptionMessage
     * @return \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     */
    public static function getException( $type = 'exception', $slug = 'unknown',
        $params = array(), \Exception $previous = null );

    /**
     * <Exception Throw Method>
     * Throws an Oroboros exception by canonicalized keyword.
     * @param type $type (optional) The canonicalized keyword selector for the exception. Default is the Oroboros standard exception.
     * @param type $message (optional) Standard exception message. Default is no message.
     * @param type $code (optional) Standard exception code. May also be a canonicalized string. Default is 0
     * @param \Exception $previous (optional) An optional previous exception to pass into the exception stack. Default is null.
     * @return void
     * @throws \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     */
    public static function throwException( $type = 'exception', $message = '',
        $code = 0, \Exception $previous = null );

    /**
     * <Exception Recast Method>
     * Recasts a given exception as an equivalent that honors its inheritance
     * and is recast as an Oroboros Exception that honors the internal exception
     * contract interface.
     *
     * This method returns the exception but does not throw it.
     *
     * @param \Exception $exception
     * @param string $type (optional) If provided, will recast based on a canonicalized name
     * @return \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     */
    public static function recastException( \Exception $exception, $type = null );
}
