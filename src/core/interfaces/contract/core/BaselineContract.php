<?php

/*
 * The MIT License
 *
 * Copyright 2016 Brian Dayhoff <brian@mopsyd.me>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\interfaces\contract\core;

/**
 * <Baseline Contract>
 * This interface designates the baseline methods provided as
 * part of the baseline object api. Every single class that maintains
 * object state and does not extend a PHP base class or 3rd party class
 * that would cause a conflict extends from this contract, with the exception of
 * a very small handful of utility classes that are used in baseline
 * initialization.
 *
 * --------
 *
 * Contract interfaces enforce expected behavior in a non-colliding way.
 * They are tasked with enforcing methods, and extending interfaces
 * provided by standards and other packages for compatibility.
 *
 * All valid oroboros classes MUST extend at
 * least one Contract Interface to be considered valid.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/contract_interface.md
 * @category contract-interfaces
 * @package oroboros/core
 * @subpackage internal
 * @version 0.2.5
 * @since 0.2.5
 */
interface BaselineContract
extends CoreContract
{

    /**
     * <Standard Destructor>
     * This presents a standardized destructor that fires cleanup operations.
     */
    public function __destruct();

    /**
     * <Baseline Initialization Method>
     * Fires the baseline initialization.
     * @param mixed $params Any parameters that should be passed into initialization.
     * @param array $dependencies Any dependencies to inject into initialization.
     * @param array $flags
     * @return $this
     *
     * @throws \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     * This interface is attached to all exceptions that arise from this trait. If you want
     * to handle everything in one catch block, use this for your catch statement.
     *
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     * If provided parameters are not valid
     *
     * @throws \oroboros\core\utilities\exception\RuntimeException
     * If initialization has already occurred and reinitialization is disabled.
     *
     * @throws \oroboros\core\utilities\exception\LogicException
     * If the class is misconfigured. This is a serious error that
     * requires developer intervention on the part of whoever wrote
     * the override that caused the error, because it means the class
     * is entirely unusable.
     */
    public function initialize( $params = null, $dependencies = null,
        $flags = null );

    /**
     * <Baseline Runtime Type Context Getter Method>
     * Returns the default declared runtime class type context.
     * If the CLASS_TYPE class constant is not set on this object,
     * it will instead fall back to the static compiled class type,
     * which will in turn return either a compiled class type or false.
     *
     * @return string|bool
     */
    public function getType();

    /**
     * <Baseline Runtime Scope Context Getter Method>
     * Returns the default declared runtime class scope context.
     * If the CLASS_SCOPE class constant is not set on this object,
     * it will instead fall back to the static compiled class scope,
     * which will in turn return either a compiled class scope or false.
     *
     * @return string|bool
     */
    public function getScope();

    /**
     * <Baseline Runtime Api Context Getter Method>
     * Returns the default declared runtime class api context.
     * If the API class constant is not set on this object,
     * it will instead fall back to the static compiled class api,
     * which will in turn return either a compiled class api or false.
     *
     * @return string|bool
     */
    public function getApi();

    /**
     * <Baseline Public Flag Setter Method>
     * Sets a given flag, if flag setting is allowed,
     * if it passes the whitelist if one exists, and
     * if it is a scalar value.
     *
     * @param scalar $flag
     * @return void
     * @throws \oroboros\core\utilities\exception\RuntimeException
     * If setting flags is not allowed.
     *
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     * If the given flag is not scalar, or if a whitelist is defined
     * and the flag is not in it.
     */
    public function setFlag( $flag );

    /**
     * <Baseline Public Flag Unsetter Method>
     * Removes a given flag, if it exists and unsetting flags is allowed.
     * @param scalar $flag
     * @return void
     * @throws \oroboros\core\utilities\exception\RuntimeException If unsetting flags is not allowed.
     */
    public function unsetFlag( $flag );

    /**
     * <Baseline Flag Check Method>
     * Returns a boolean determination as to whether a given flag exists
     * in the current objects flag set.
     * @param scalar $flag
     * @return bool
     */
    public function checkFlag( $flag );

    /**
     * <Baseline Flag Getter Method>
     * Returns all flags currently set in the current object flag set.
     * @return array
     */
    public function getFlags();

    /**
     * <Baseline Required Parameters Getter Method>
     * Returns a list of all required parameters.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the parameter requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public function getRequiredParameters();

    /**
     * <Baseline Required Dependency Getter Method>
     * Returns a list of all required dependencies.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the dependency injection requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public function getRequiredDependencies();

    /**
     * <Baseline Required Parameters Getter Method>
     * Returns a list of all required parameters.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the flag requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public function getRequiredFlags();

    /**
     * <Baseline Valid Parameters Getter Method>
     * Returns a list of all valid parameters.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the parameter requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public function getValidParameters();

    /**
     * <Baseline Valid Dependencies Getter Method>
     * Returns a list of all valid dependencies.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the dependency injection requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public function getValidDependencies();

    /**
     * <Baseline Valid Flags Getter Method>
     * Returns a list of all required flags.
     * This may be called at any point after instantiation,
     * so that other objects may evaluate if they can meet
     * all of the flag requirements prior to calling
     * initialization without raising an exception.
     *
     * @note if auto-initialization is enabled, other objects will not have
     * a chance to call this method before initialization occurs. In that case,
     * initialization should be put in a try/catch block to control error
     * flow safely.
     *
     * @return array
     */
    public function getValidFlags();

    /**
     * <Standard Object Fingerprint Getter>
     * Returns the object fingerprint for the given object instance.
     * The static fingerprint is tracked globally across all instances
     * of a given object, whereas the object fingerprint is tracked
     * on a per-object designation. This method will create a fingerprint
     * if none has already been created, so there is no need to explicitly
     * call the setter from your constructor in most cases.
     * @return string
     * @final
     */
    public function getFingerprint();

    /**
     * <Standard Initialization Check>
     * Returns a boolean determination as to whether the object has completed
     * its baseline initialization.
     * @return bool
     */
    public function isInitialized();
}
