<?php

/*
 * The MIT License
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @copyright (c) 2017, Brian Dayhoff <mopsyd@me.com> all rights reserved.
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace oroboros\core\traits\extensions;

/**
 * <Extension Trait>
 *
 * --------
 *
 * Traits provide extended method support to classes without requiring a direct,
 * linear chain of inheritance. This allows functions to inherit subsets of
 * related methods without declaring a parent class.
 *
 * In Oroboros core, ALL methods are granted to classes via traits,
 * and the classes themselves are just containers that correlate their methods
 * to an interface they are expected to honor. This approach maximizes
 * interoperability, by entirely removing class inheritance as a requirement
 * for extension of any class in this system.
 *
 * 3rd parties using this package are not expected to follow this approach,
 * but ALL of our internal class and logic structure does.
 *
 * --------
 *
 * @author Brian Dayhoff <mopsyd@me.com>
 * @license http://opensource.org/licenses/MIT The MIT License (MIT)
 * @link bitbucket.org/oroborosframework/oroboros-core/wiki/development/api/traits.md
 * @category traits
 * @category extension
 * @package oroboros/core
 * @subpackage extension
 * @version 0.2.5
 * @since 0.2.5
 * @satisfies \oroboros\core\interfaces\contract\extensions\ExtensionContract
 */
trait ExtensionTrait
{

    use \oroboros\core\traits\core\BaselineTrait
    {
        \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait::_validate insteadof \oroboros\core\traits\core\BaselineTrait;
        \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait::_validationModes insteadof \oroboros\core\traits\core\BaselineTrait;
        \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait::_throwException insteadof \oroboros\core\traits\core\BaselineTrait;
        \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait::_getException insteadof \oroboros\core\traits\core\BaselineTrait;
        \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait::_recastException insteadof \oroboros\core\traits\core\BaselineTrait;
        \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait::_getExceptionCode insteadof \oroboros\core\traits\core\BaselineTrait;
        \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait::_getExceptionMessage insteadof \oroboros\core\traits\core\BaselineTrait;
        \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait::_getExceptionIdentifier insteadof \oroboros\core\traits\core\BaselineTrait;
        \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait::_getExceptionIdentifiers insteadof \oroboros\core\traits\core\BaselineTrait;
        \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait::_getExceptionCodeIdentifiers insteadof \oroboros\core\traits\core\BaselineTrait;
        \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait::_baselineInternalsMergeArrayDefaults insteadof \oroboros\core\traits\core\BaselineTrait;
        \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait::_baselineInternalsValidateValues insteadof \oroboros\core\traits\core\BaselineTrait;
        \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait::_baselineInternalsCheckValid insteadof \oroboros\core\traits\core\BaselineTrait;
        \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait::_baselineInternalsThrowInvalidException insteadof \oroboros\core\traits\core\BaselineTrait;
        \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait::_baselineInternalsCheckConfiguration insteadof \oroboros\core\traits\core\BaselineTrait;
    }
    use \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait
    {
        \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait::staticInitialize as private scae_staticInitialize;
        \oroboros\core\traits\patterns\structural\StaticControlApiExtensionTrait::registerIndex as private scae_registerIndex;
    }

    /**
     * Represents the context of the extension, which should
     * correspond to the class intended to be extended by
     * the extension.
     * @var string
     */
    private static $_extension_context;

    /**
     * Represents the identifier of the extension, which
     * becomes the selection key for it within its
     * parent extendable class.
     * @var string
     */
    private static $_extension_id;

    /**
     * -------------------------------------------------------------------------
     * Contract Methods
     *
     * These methods satisfy the public api defined in the bootstrap contract
     *
     * @satisfies \oroboros\core\interfaces\contract\extensions\ExtensionContract
     *
     * @execution Default Execution Plan (minimal)
     *
     * @execution Default Execution Plan (commented)
     *
     * -------------------------------------------------------------------------
     */

    /**
     * <Static Baseline Internal Initialization Method>
     * Initializes a class in the static scope, so that related configurations,
     * dependencies, flags, and settings are accessible across a range of child
     * classes. This also allows for extension logic to be immediately accessible
     * to all child classes if extensions are used.
     *
     * Static initialization should be stateless, only providing base level
     * dependencies and configurations that are required across a family of
     * related classes. Any class extending upon this logic SHOULD NOT mutate
     * the static state in any way that would interrupt other objects operating
     * from the same parent elsewhere.
     *
     * Most Oroboros internals that extend upon this and work in the active
     * object scope will lock static initialization so it remains immutable
     * after its first run.
     *
     * Stateful static initialization should only ever occur in a
     * dedicated base object that does not have a deep inheritance chain,
     * and only in very specialized cases. The functionality is not outright
     * prevented, however it does require several overrides to enable, because
     * its use in such a manner is highly discouraged to avoid introducing
     * instability in your code. Internal Oroboros logic always disables this
     * functionality, but you may roll your own classes using this trait that
     * implement it pretty easily if need be.
     *
     * @param type $params (optional) Any parameters that need
     *     to be accessible across all child objects.
     * @param type $dependencies (optional) Any dependencies that need
     *     to be accessible across all child objects.
     * @param type $flags (optional) Any flags that need
     *     to be accessible across all child objects.
     * @return string The extension class name
     *     for static method chaining
     * @throws \oroboros\core\interfaces\contract\utilities\exception\ExceptionContract
     * This interface is attached to all exceptions that arise from this trait. If you want
     * to handle everything in one catch block, use this for your catch statement.
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     * If provided parameters are not valid
     * @throws \oroboros\core\utilities\exception\RuntimeException
     * If initialization has already occurred and reinitialization is disabled.
     * @throws \oroboros\core\utilities\exception\LogicException
     * If the class is misconfigured. This is a serious error that
     * requires developer intervention on the part of whoever wrote
     * the override that caused the error, because it means the class
     * is entirely unusable.
     */
    public static function staticInitialize( $params = null,
        $dependencies = null, $flags = null )
    {
        self::_extensionPreInitialization();
        self::_staticBaselineEnableFlagValidation();
        self::_staticBaselineRegisterInitializationTask( '_extensionPostInitialization' );
        self::_staticBaselineRegisterInitializationTask( '_extensionSetup' );
        self::scae_staticInitialize( $params, $dependencies, $flags );
        return get_called_class();
    }

    /**
     * <Extension Context Getter>
     * This method is used to determine the context of the extension by
     * the ExtensionManager, and which class it is intended to extend.
     * @return string|bool Returns false if no context is set,
     *     otherwise returns the context.
     */
    public static function getContext()
    {
        return self::_extensionGetContext();
    }

    /**
     * <Extension Identifier Getter>
     * Returns the Id associated with the extension.
     * @return string
     */
    public static function getExtensionId()
    {
        return self::_extensionGetId();
    }

    /**
     * <Extension Index Check Method>
     * Returns a boolean determination as to whether the given index
     * is registered for the extension.
     * @param string $index
     * @return bool
     */
    public static function hasIndex( $index )
    {
        return self::_extensionHasIndex( $index );
    }

    /**
     * <Extension Method Index Getter>
     * Returns a list of the commands within the given index.
     * If the index is not registered, returns false.
     * @param string $index The index to check within
     * @return array|bool Returns false if the index is not registered,
     *     otherwise returns an array of the valid commands
     *     within the index.
     */
    public static function getIndex( $index )
    {
        return self::_extensionGetIndex( $index );
    }

    /**
     * <Extension Index List Getter>
     * Returns an array of all indexes the extension has declared.
     * @return array
     */
    public static function getIndexes()
    {
        return self::_extensionGetIndexes();
    }

    /**
     * <Extension Command Check Method>
     * Returns a boolean determination as to whether the given index
     * contains the specified method. If no method is supplied, will return
     * whether the index has a default method.
     * @param string $index The index to check for the method within
     * @param string $method (optional) If not supplied, will return whether
     *     the index has a default. Default not supplied.
     * @return bool
     */
    public static function hasMethod( $index, $method = null )
    {
        return self::_extensionHasMethod( $index, $method );
    }

    /**
     * -------------------------------------------------------------------------
     * Extension Methods (protected)
     *
     * These methods may be extended by inheriting constructs as needed.
     * They represent the interal api.
     * -------------------------------------------------------------------------
     */

    /**
     * <Extension Internal Pre-Initialization Setup Method>
     * This method will be called immediately prior to calling the baseline
     * initialization, which allows the extension to set any of the baseline
     * parameters required for valid initialization.
     * @return void
     */
    protected static function _extensionPreInitialization()
    {
        //no-op
    }

    /**
     * <Extension Internal Setup Method>
     * This method will be called immediately after initialization fires,
     * allowing the extension to perform any post-initialization setup tasks
     * required for valid setup.
     * @return void
     */
    protected static function _extensionSetup()
    {
        //no-op
    }

    /**
     * <Extension Context Setter>
     * Sets the context of the extension. This method should be overridden
     * in your extension to define the context it is meant to extend.
     * @return string
     */
    abstract protected static function _declareExtensionContext();

    /**
     * <Extension Id Setter>
     * Sets the identifying slug that the extension is known by.
     * This method should be overridden in your extension to define
     * the identifying key that the extension is known by within the
     * class it extends.
     * @return string
     */
    abstract protected static function _declareExtensionId();

    /**
     * <Extension Api Setter>
     * Sets the Api declarations for the extension.
     * This method should return an array of api prefixes to
     * scrape from the class methods. The StaticControlApi
     * will do the rest of the heavy lifting.
     * @return array
     */
    abstract protected static function _declareExtensionApi();

    /**
     * Sets the static baseline valid flags
     * @return array
     */
    protected static function _staticBaselineSetFlagsValid()
    {
        return \oroboros\Oroboros::getCompiledRequiredFlags();
    }

    /**
     * Sets the static default flags
     * @return array
     */
    protected static function _staticBaselineSetFlagsDefault()
    {
        return \oroboros\Oroboros::getCompiledFlags();
    }

    /**
     * -------------------------------------------------------------------------
     * Logic Methods (private)
     *
     * These methods are not externally exposed.
     * They represent the actual work.
     * -------------------------------------------------------------------------
     */

    /**
     * Sets up the extension.
     * @return void
     * @internal
     */
    private static function _extensionPostInitialization()
    {
        $class = get_called_class();
        self::_extensionSetIdentifier( $class::_declareExtensionId() );
        self::_extensionSetContext( $class::_declareExtensionContext() );
        if ( !self::isStaticInitialized() ) //throws an exception if the indexes have already been declared. Reinitialization must be non-blocking unless explicitly set to be blocking via the baseline logic switch.
        {
            self::_extensionSetApiIndexes( $class::_declareExtensionApi() );
        }
        //Blacklist the extension methods
        $ref = new \oroboros\codex\LexiconEntry( __TRAIT__ );
        $blacklist = array_merge( get_class_methods( __TRAIT__ ),
            get_class_methods( '\\oroboros\\core\\traits\\core\\StaticBaselineTrait' ),
            get_class_methods( '\\oroboros\\core\\traits\\core\\BaselineTrait' ) );
        foreach ( $blacklist as
            $method )
        {
            self::_staticControlApiBlacklistMethod( $method );
        }
    }

    /**
     * Returns false if no context has been defined,
     * otherwise returns the context.
     * @return string|bool
     * @internal
     */
    private static function _extensionGetContext()
    {
        if ( is_null( self::$_extension_context ) )
        {
            $class = get_called_class();
            self::_extensionSetContext( $class::_declareExtensionContext() );
        }
        return self::$_extension_context;
    }

    /**
     * Returns false if no identifier has been defined,
     * otherwise returns the identifier.
     * @return string|bool
     * @internal
     */
    private static function _extensionGetId()
    {
        if ( is_null( self::$_extension_id ) )
        {
            $class = get_called_class();
            self::_extensionSetIdentifier( $class::_declareExtensionId() );
        }
        return self::$_extension_id;
    }

    /**
     * Sets the context of the extension
     * @param string $context
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException If an invalid context is passed
     * @throws \oroboros\core\utilities\exception\RuntimeException If the context is already set
     * @internal
     */
    private static function _extensionSetContext( $context )
    {
        self::_validate( 'stringable', $context, true );
        self::$_extension_context = (string) $context;
    }

    /**
     * Declares the extension api to the StaticControlApi logic.
     * @param array $api
     * @return void
     * @internal
     */
    private static function _extensionSetApiIndexes( $api )
    {
        self::_validate( 'type-of', $api, 'array', true );
        foreach ( $api as
            $index )
        {
            self::scae_registerIndex( $index );
        }
    }

    /**
     * Sets the internal identifier for the extension.
     * @param string $id The identifier to set for the extension
     * @return void
     * @throws \oroboros\core\utilities\exception\InvalidArgumentException
     *     If the given identifier is not valid
     * @throws \oroboros\core\utilities\exception\RuntimeException
     *     If the identifier is already declared
     * @internal
     */
    private static function _extensionSetIdentifier( $id )
    {
        self::_validate( 'stringable', $id, null, true );
        self::$_extension_id = (string) $id;
    }

    /**
     * Performs a check against the registered indexes,
     * and returns a determination as to whether the
     * given index is registered.
     * @param type $index
     * @return bool
     * @internal
     */
    private static function _extensionHasIndex( $index )
    {
        return is_string( $index ) && in_array( $index,
                self::_extensionGetIndexes() );
    }

    /**
     * Returns a set of the valid commands for the given index,
     * or false if the index is not registered.
     * @param type $index
     * @return array
     * @internal
     */
    private static function _extensionGetIndex( $index )
    {
        $api = self::api( $index );
        if ( !$api )
        {
            return false;
        }
        return json_decode( json_encode( $api ), 1 );
    }

    /**
     * Returns an array of all indexes the extension has declared.
     * @return array
     * @internal
     */
    private static function _extensionGetIndexes()
    {
        $class = get_called_class();
        return $class::_declareExtensionApi();
    }

    /**
     * Performs a check against the registered indexes, and returns
     * a determination as to whether a given command exists within
     * the given index, or whether the index has a default command
     * if no command is supplied.
     * @param type $index
     * @param type $method
     * @return bool
     * @internal
     */
    private static function _extensionHasMethod( $index, $method = null )
    {
        $class = get_called_class();
        return $class::api( $index, $method ) !== false;
    }

}
